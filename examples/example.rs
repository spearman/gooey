#![warn(unused_extern_crates)]

use std;
use serde_yaml;
use serde::{Deserialize, Serialize};
use gooey::*;

#[allow(unused_macros)]
macro_rules! display {
  ($e:expr) => { println!("{}: {}", stringify!($e), $e); }
}
#[allow(unused_macros)]
macro_rules! show {
  ($e:expr) => { println!("{}: {:?}", stringify!($e), $e); }
}
#[allow(unused_macros)]
macro_rules! pretty {
  ($e:expr) => { println!("{}: {:#?}", stringify!($e), $e); }
}

const CONFIG_PATH_YAML : &'static str = "Example.yaml";

#[derive(Deserialize, Serialize)]
pub struct Config {
  bindings : interface::controller::Bindings
}

fn main() {
  println!("gooey example: main...");
  // load example config
  let cfg = {
    let config_file = std::fs::File::open (CONFIG_PATH_YAML).unwrap();
    serde_yaml::from_reader::<std::fs::File, Config> (config_file).unwrap()
  };
  display!(serde_yaml::to_string (&cfg).unwrap());
  // print some size information
  report_sizes();
  // create a default interface
  let mut interface : Interface = Interface::with_root (interface::Element {
    name: "Root".to_string(), .. Default::default()
  });
  interface.print_elements_tree();
  // manually construct an element subtree and attach to root
  let subtree = {
    let node = |text : &str| tree::Node::new (interface::Element {
      name:  text.to_uppercase().into(),
      model: interface::model::Component::Text (text.into()).into(),
      .. interface::Element::default()
    });
    let insert_under = |node_id| tree::InsertBehavior::UnderNode (node_id);
    let mut subtree  = tree::TreeBuilder::new().with_root (node ("a")).build();
    let root_id      = subtree.root_node_id().unwrap().clone();
    let child_id     = subtree.insert (node ("b"), insert_under (&root_id))
      .unwrap();
    let _            = subtree.insert (node ("c"), insert_under (&child_id))
      .unwrap();
    subtree
  };
  let root_id = interface.root_id().clone();
  let events  = interface.action (&root_id,
    interface::Action::Create (subtree, interface::CreateOrder::Append));
  for (_, event) in events {
    show!(event);
  }
  interface.print_elements_tree_names();
  println!("gooey example: ...main");
}
