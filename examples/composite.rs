#![warn(unused_extern_crates)]

use env_logger;
use lazy_static::lazy_static;
use log;
use serde::{Deserialize, Serialize};
use vec_map::VecMap;
use gooey::*;

const CONFIG_PATH_YAML  : &'static str = "Composite.yaml";
const LOG_FILENAME      : &'static str = "composite.log";
const FRAME_MS          : u64          = 30;
const fn log_level_default() -> log::LevelFilter {
  log::LevelFilter::Info
}
const SOUND_TICK  : interface::view::Sound = interface::view::Sound (0);
const SOUND_BITE  : interface::view::Sound = interface::view::Sound (1);
const SOUND_GLOP  : interface::view::Sound = interface::view::Sound (2);
const SOUND_METAL : interface::view::Sound = interface::view::Sound (3);
const SOUND_DOOR  : interface::view::Sound = interface::view::Sound (4);

lazy_static!{
  pub static ref CONFIG : Config = {
    let config_file = std::fs::File::open (CONFIG_PATH_YAML).unwrap();
    serde_yaml::from_reader::<std::fs::File, Config> (config_file).unwrap()
  };
  pub static ref SCREEN_CONTROLS : interface::controller::Controls =
    interface::controller::controls::Builder::new()
      .buttons (vec![ Mybuttoncontrols::Myquit.into() ].into())
      .build();
}

#[derive(Deserialize, Serialize)]
pub struct Config {
  #[serde(default = "log_level_default")]
  pub log_level       : log::LevelFilter,
  pub screen_bindings : interface::controller::Bindings <Myapp>,
  pub menu_bindings   : interface::controller::Bindings <Myapp>,
  pub button_bindings : interface::controller::Bindings <Myapp>
}

////////////////////////////////////////////////////////////////////////////////
//  application                                                               //
////////////////////////////////////////////////////////////////////////////////

#[derive(Debug)]
pub struct Myapp {
  interface : Interface <Self,
    presentation::Composite <presentation::Curses, presentation::Fmod>>,
  running   : bool
}

impl Application for Myapp {
  type CallbackIds     = Mycallbackids;
  type AxisControls    = interface::controller::controls::Nil;
  type ButtonControls  = Mybuttoncontrols;
  type MotionControls  = interface::controller::controls::Nil;
  type PointerControls = interface::controller::controls::Nil;
  type SystemControls  = interface::controller::controls::Nil;
  type TextControls    = interface::controller::controls::Nil;
  type WheelControls   = interface::controller::controls::Nil;
}

//  controls
extend_control!{
  Mybuttoncontrols <interface::controller::controls::button::Release>
    (interface::controller::controls::button, Button)
  {
    Myquit => myquit
  }
}
//  callbacks
def_callback_ids! {
  Mycallbackids <Myapp> {
    Quit => Myapp::quit
    Play => Myapp::play
  }
}

impl Myapp {
  fn new () -> Self {
    // init interface
    let interface = {
      use gooey::prelude::*;
      let mut interface = presentation::Composite::<
        presentation::Curses, presentation::Fmod
      >::make_interface();
      // debug: log curses info
      interface.presentation.graphics.inner().log_info();
      // load audio data
      interface.presentation.audio.audition.load_samples (vec![
        (SOUND_TICK.0  as usize, "tick.wav".into()),
        (SOUND_BITE.0  as usize, "bite.wav".into()),
        (SOUND_GLOP.0  as usize, "glop.wav".into()),
        (SOUND_METAL.0 as usize, "metal.wav".into()),
        (SOUND_DOOR.0  as usize, "door.wav".into())
      ].into_iter().collect::<VecMap <String>>());
      interface.presentation.audio.audition.update();
      // root screen frame
      let screen_id = {
        let root_id = interface.focused_element_id().clone();
        let Element { mut controller, view, .. }  = {
          let mut screen = frame::screen::TileBuilder::new()
            .bindings (&CONFIG.screen_bindings)
            .clear_color (Some (Color::white()).into())
            .dimensions (interface.presentation.graphics.dimensions())
            .build_element();
          screen.focus();
          screen
        };
        controller.add_bindings (
          &CONFIG.screen_bindings.get_bindings (&SCREEN_CONTROLS));
        let screen_view       = Box::new (|v : &mut View| *v = view);
        let screen_controller = Box::new (|c : &mut Controller| *c = controller);
        let _ = interface.action (&root_id, Action::ModifyView (screen_view));
        let _ = interface.action (&root_id,
          Action::ModifyController (screen_controller));
        root_id
      };
      // menu
      let menu_id = {
        // frame: magenta (20x40)
        let (rows, columns) = (20, 40);
        let appearances = AppearancesBuilder::default()
          .style_fg (State::Enabled, Color::white())
          .style_bg (State::Enabled, Color::magenta())
          .style_fg (State::Focused, Color::white())
          .style_bg (State::Focused, Color::magenta())
          .build();
        let layout = {
          let anchor = Alignment::middle_center();
          let size   = Size { width: columns.into(), height: rows.into() };
          let offset = Offset { horizontal: 0.into(), vertical: 0.into() };
          layout::Free { anchor, offset, size }
        };
        let border = Border {
          top:              b'~' as u32,
          bottom:           b'~' as u32,
          left:             b'(' as u32,
          right:            b')' as u32,
          top_left:         b'~' as u32,
          top_right:        b'~' as u32,
          bottom_left:      b'~' as u32,
          bottom_right:     b'~' as u32,
          thickness_top:    1,
          thickness_bottom: 1,
          thickness_left:   1,
          thickness_right:  1
        };
        let frame = frame::free::Builder::<Myapp>::new (
          interface.elements(), &screen_id
        ) .appearances (appearances)
          .border (border)
          .layout (layout)
          .build_element();
        interface.create_singleton (&screen_id, frame, CreateOrder::Append)
      };
      // switch controller
      let switch_builder = {
        let trigger = switch::Trigger::new::<Myapp> (
          controls::button::Builtin::FormSubmitCallback.into(), None);
        switch::Builder::default()
          .style_fg ((switch::State::On,  State::Enabled), Color::cyan())
          .style_bg ((switch::State::On,  State::Enabled), Color::white())
          .style_fg ((switch::State::Off, State::Enabled), Color::white())
          .style_bg ((switch::State::Off, State::Enabled), Color::cyan())
          .sound    ((switch::State::On,  State::Focused), SOUND_TICK)
          .style_fg ((switch::State::On,  State::Focused), Color::red())
          .style_bg ((switch::State::On,  State::Focused), Color::white())
          .sound    ((switch::State::Off, State::Focused), SOUND_TICK)
          .style_fg ((switch::State::Off, State::Focused), Color::white())
          .style_bg ((switch::State::Off, State::Focused), Color::red())
          .enter    (switch::State::On, trigger)
      };
      {  // button0: bite.wav
        let label  = "bite.wav".to_string();
        let layout = {
          let anchor  = Alignment::middle_center();
          let offset  = Offset::default_absolute();
          let size    = Size {
            width:  (label.len() as u32 + 2).into(),
            height: 3.into()
          };
          layout::Variant::from (layout::Free { anchor, offset, size })
        };
        let switch = switch_builder.clone().label (label).build();
        let builder = button::Builder::new (interface.elements(), &menu_id)
          .bindings (&CONFIG.button_bindings)
          .callback_id (Mycallbackids::Play.into())
          .frame_border (Border::default())
          .frame_layout (layout.into())
          .switch (switch);
        let switch_id = build_and_return_node_id!(interface, builder, 1);
        let sfx = playback::new (Some (SOUND_BITE));
        let _ = interface.action (&switch_id,
          Action::create_singleton (sfx, CreateOrder::Append));
      }
      {  // button1: glop.wav
        let label  = "glop.wav".to_string();
        let layout = {
          let anchor  = Alignment::middle_center();
          let offset  = Offset::default_absolute();
          let size    = Size {
            width:  (label.len() as u32 + 2).into(),
            height: 3.into()
          };
          layout::Variant::from (layout::Free { anchor, offset, size })
        };
        let switch = switch_builder.clone().label (label).build();
        let builder = button::Builder::new (interface.elements(), &menu_id)
          .bindings (&CONFIG.button_bindings)
          .callback_id (Mycallbackids::Play.into())
          .frame_border (Border::default())
          .frame_layout (layout.into())
          .switch (switch);
        let switch_id = build_and_return_node_id!(interface, builder, 1);
        let sfx = playback::new (Some (SOUND_GLOP));
        let _ = interface.action (&switch_id,
          Action::create_singleton (sfx, CreateOrder::Append));
      }
      {  // button2: metal.wav
        let label  = "metal.wav".to_string();
        let layout = {
          let anchor = Alignment::middle_center();
          let offset = Offset::default_absolute();
          let size   = Size {
            width:  (label.len() as u32 + 2).into(),
            height: 3.into()
          };
          layout::Variant::from (layout::Free { anchor, offset, size })
        };
        let switch = switch_builder.clone().label (label).build();
        let builder = button::Builder::new (interface.elements(), &menu_id)
          .bindings (&CONFIG.button_bindings)
          .callback_id (Mycallbackids::Play.into())
          .frame_border (Border::default())
          .frame_layout (layout.into())
          .switch (switch);
        let switch_id = build_and_return_node_id!(interface, builder, 1);
        let sfx = playback::new (Some (SOUND_METAL));
        let _ = interface.action (&switch_id,
          Action::create_singleton (sfx, CreateOrder::Append));
      }
      {  // button3: door.wav
        let label  = "door.wav".to_string();
        let layout = {
          let anchor = Alignment::middle_center();
          let offset = Offset::default_absolute();
          let size   = Size {
            width:  (label.len() as u32 + 2).into(),
            height: 3.into()
          };
          layout::Variant::from (layout::Free { anchor, offset, size })
        };
        let switch = switch_builder.clone().label (label).build();
        let builder = button::Builder::new (
          interface.elements(), &menu_id
        ) .bindings (&CONFIG.button_bindings)
          .callback_id (Mycallbackids::Play.into())
          .frame_border (Border::default())
          .frame_layout (layout.into())
          .switch (switch);
        let switch_id = build_and_return_node_id!(interface, builder, 1);
        let sfx = playback::new (Some (SOUND_DOOR));
        let _ = interface.action (&switch_id,
          Action::create_singleton (sfx, CreateOrder::Append));
      }
      { // arrange buttons
        let anchor = Alignment {
          horizontal: alignment::Horizontal::Left,
          vertical:   alignment::Vertical::Top
        };
        let actions = frame::free::rearrange_absolute (
          interface.elements(), &menu_id, anchor, (3, 1),
          Orientation::Vertical, 1, false
        );
        let _ = interface.actions (actions);
      }
      { // menu widget
        let selection = Selection { loop_: true, .. Selection::default() };
        let actions   = menu::Builder::new (interface.elements(), &menu_id)
          .bindings (&CONFIG.menu_bindings)
          .selection (selection)
          .build_actions();
        let _ = interface.actions (actions);
      }
      // focus menu
      let _ = interface.action (&menu_id, Action::Focus);
      interface
    };
    //log::debug!("interface: {:#?}", interface);

    Myapp { interface, running: true }
  }

  #[inline]
  fn is_running (&self) -> bool {
    self.running
  }

  #[inline]
  fn interface (&self) -> &Interface <Self,
    presentation::Composite <presentation::Curses, presentation::Fmod>>
  {
    &self.interface
  }

  fn update (&mut self) {
    log::trace!("update...");
    // update interface and handle events
    let events = self.interface.update().filter (|(_, event)| match event {
      interface::model::Event::Submit (_) => true,
      _ => false
    }).collect::<Vec <(tree::NodeId, interface::model::Event)>>();
    for (node_id, event) in events {
      match event {
        interface::model::Event::Submit (model) => {
          let callback =
            application::callback::<Myapp> (model.callback_id.unwrap());
          callback (self, model.component, node_id);
        }
        _ => {}
      }
    }
    self.interface.display();
    log::trace!("...update");
  }

  //
  //  callbacks
  //
  fn quit (&mut self, _ : interface::model::Component, _ : tree::NodeId) {
    self.running = false;
  }
  /// Play the sfx child node
  fn play (&mut self, _ : interface::model::Component, switch_id : tree::NodeId) {
    log::trace!("play...");
    let sfx_id = self.interface.elements().children_ids (&switch_id).unwrap()
      .next().unwrap().clone();
    let mut actions = vec![];
    interface::widget::playback::play (&None, self.interface.elements(),
      &sfx_id, &mut actions);
    let _ = self.interface.actions (actions);
    log::trace!("...play");
  }
}

//
//  control funs
//
/// Submit the `Quit` callback
fn myquit (
  _             : &interface::controller::controls::button::Release,
  _elements     : &Tree <interface::Element>,
  node_id       : &tree::NodeId,
  action_buffer : &mut Vec <(tree::NodeId, interface::Action)>
) {
  log::trace!("myquit...");
  action_buffer.push (
    ( node_id.clone(),
      interface::Action::SubmitCallback (Mycallbackids::Quit.into()) ));
  log::trace!("...myquit");
}

////////////////////////////////////////////////////////////////////////////////
//  main                                                                      //
////////////////////////////////////////////////////////////////////////////////

fn main() {
  println!("gooey composite example: main...");

  report_sizes();

  println!("config:\n{}", serde_yaml::to_string (&*CONFIG).unwrap());

  // log init
  let log_file = Box::new (std::fs::File::create (LOG_FILENAME).unwrap());
  env_logger::Builder::new()
    .filter_level (CONFIG.log_level)
    .parse_default_env()
    .target (env_logger::Target::Pipe (log_file))
    .init();
  println!("logging output to {:?}", LOG_FILENAME);
  log::info!("log begin...");

  // create application
  let mut myapp = Myapp::new();
  myapp.interface().log_elements_tree();
  myapp.interface().log_elements_tree_names();

  // main loop
  while myapp.is_running() {
    myapp.update();
    std::thread::sleep (std::time::Duration::from_millis (FRAME_MS));
  }

  log::info!("...log end");
  println!("gooey composite example: ...main");
}
