#![feature(decl_macro)]
#![warn(unused_extern_crates)]

use std::fs;
use env_logger;
use lazy_static::lazy_static;
use log;
use serde::{Deserialize, Serialize};
use nsys::curses;
use gooey::*;

const CONFIG_PATH_YAML  : &'static str = "CursesDefaults.yaml";
const LOG_FILENAME      : &'static str = "curses-defaults.log";
const FRAME_MS          : u64          = 30;
// function for serde default
const fn log_level_default() -> log::LevelFilter {
  log::LevelFilter::Info
}

lazy_static!{
  pub static ref CONFIG : Config = {
    let config_file = fs::File::open (CONFIG_PATH_YAML).unwrap();
    serde_yaml::from_reader::<fs::File, Config> (config_file).unwrap()
  };
  pub static ref SCREEN_CONTROLS : interface::controller::Controls =
    interface::controller::controls::Builder::new()
      .buttons (vec![ Mybuttoncontrols::Myquit.into() ].into())
      .build();
}

#[derive(Deserialize, Serialize)]
pub struct Config {
  #[serde(default = "log_level_default")]
  pub log_level       : log::LevelFilter,
  pub screen_bindings : interface::controller::Bindings <Myapp>,
  pub frame_bindings  : interface::controller::Bindings <Myapp>,
  pub field_bindings  : interface::controller::Bindings <Myapp>,
  pub numbox_bindings : interface::controller::Bindings <Myapp>
}

#[allow(unused_macros)]
pub (crate) macro debug {
  ($e:expr) => {
    log::debug!("{}: {:?}", stringify!($e), $e);
  }
}

////////////////////////////////////////////////////////////////////////////////
//  application                                                               //
////////////////////////////////////////////////////////////////////////////////

#[derive(Debug)]
pub struct Myapp {
  interface : Option <Interface <Self, presentation::Curses>>,
  running   : bool
}

impl Default for Myapp {
  fn default() -> Self {
    Self::new()
  }
}

impl Application for Myapp {
  type CallbackIds     = MycallbackIds;
  type AxisControls    = interface::controller::controls::Nil;
  type ButtonControls  = Mybuttoncontrols;
  type MotionControls  = interface::controller::controls::Nil;
  type PointerControls = interface::controller::controls::Nil;
  type SystemControls  = interface::controller::controls::Nil;
  type TextControls    = interface::controller::controls::Nil;
  type WheelControls   = interface::controller::controls::Nil;
}

//  controls
extend_control!{
  Mybuttoncontrols <interface::controller::controls::button::Release>
    (interface::controller::controls::button, Button)
  {
    Myquit => myquit
  }
}
//  callbacks
def_callback_ids! {
  MycallbackIds <Myapp> {
    Quit => Myapp::quit
  }
}

impl Myapp {

  fn new () -> Self {
    // curses backend supports overriding default colors
    if cfg!(windows) {
      let mut init_colors = presentation::curses::INIT_COLORS.lock().unwrap();
      *init_colors = Some (*curses::INIT_COLORS_DEFAULT);
    }
    // init interface
    let interface = {
      use interface::controller::component::layout;
      use interface::*;
      use widget::{BuildActions, BuildElement};

      // create curses interface with root screen frame
      let mut interface = presentation::Curses::make_interface();
      // debug log
      //interface.log_elements_tree();
      interface.presentation.inner().log_info();

      // set root controller quit button
      debug_assert_eq!(interface.root_id(), interface.focused_element_id());
      let screen_id         = interface.root_id().clone();
      let screen_bindings   = CONFIG.screen_bindings
        .get_bindings (&SCREEN_CONTROLS);
      let update_controller = Box::new (move |controller : &mut Controller|
        controller.add_bindings (&screen_bindings));
      let _ = interface
        .action (&screen_id, Action::ModifyController (update_controller));

      // tiled frame with white clear color; the default screen does not set a
      // clear color, so this tiled frame will effectively clear the screen
      let actions = widget::frame::tiled::Builder::<Myapp>::new (
        interface.elements(), &screen_id
      ) .clear_color  (Some (view::Color::white()).into())
        .build_actions();
      let _ = interface.actions (actions);

      let mut layout = layout::Free::default();

      // default free frame
      let frame = widget::frame::free::Builder::<Myapp>::new (
        interface.elements(), &screen_id
      ) .layout (layout)
        .build_element();
      let _ = interface.action (
        &screen_id, Action::create_singleton (frame, CreateOrder::Append));

      layout.offset.vertical = 13.into();

      // default button
      let actions = widget::button::Builder::<Myapp>::new (
        interface.elements(), &screen_id
      ) .frame_layout (layout::Variant::from (layout).into())
        .build_actions();
      let _ = interface.actions (actions);

      layout.offset.vertical = 26.into();

      // default field
      let actions = widget::field::Builder::<Myapp>::new (
        interface.elements(), &screen_id
      ) .frame_offset (layout.offset)
        .build_actions();
      let _ = interface.actions (actions);

      layout.offset.vertical = 28.into();

      // default numbox
      let actions = widget::numbox::Builder::<Myapp>::new (
        interface.elements(), &screen_id
      ) .frame_offset (layout.offset)
        .build_actions();
      let _ = interface.actions (actions);

      layout.offset.vertical = 30.into();

      // default picture
      let actions = widget::picture::Builder::<Myapp>::new (
        interface.elements(), &screen_id
      ) .frame_layout (layout::Variant::from (layout).into())
        .build_actions();
      let _ = interface.actions (actions);

      layout.offset.vertical   = 0.into();
      layout.offset.horizontal = 26.into();

      // default textbox
      let actions = widget::textbox::Builder::<Myapp>::new (
        interface.elements(), &screen_id
      ) .frame_layout (layout::Variant::from (layout).into())
        .build_actions();
      let _ = interface.actions (actions);

      // pop-up textbox with some text
      let actions = {
        let text     = "hello world".to_string();
        let bindings = controller::bindings::Builder::new()
          .any_button (controller::controls::button::Builtin::FrameClose.into())
          .build();
        widget::textbox::FitBuilder::<Myapp>::new (
          interface.elements(), &screen_id
        ) .bindings (&bindings)
          .frame_anchor (controller::Alignment::middle_center())
          .text (text)
          .build_actions()
      };
      let (hello_id, _) = interface.actions (actions).skip (1).next().unwrap();
      let _ = interface.action (&hello_id, Action::Focus);

      interface
    };

    //log::debug!("interface: {:#?}", interface);
    Myapp {
      interface: Some (interface),
      running:   true
    }
  }

  #[inline]
  fn is_running (&self) -> bool {
    self.running
  }

  #[inline]
  fn interface (&self) -> &Interface <Self, presentation::Curses> {
    self.interface.as_ref().unwrap()
  }

  fn update (&mut self) {
    log::trace!("update...");
    // update interface and handle events
    let mut interface = self.interface.take().unwrap();
    let events = interface.update();
    for (node_id, event) in events {
      match event {
        interface::model::Event::Submit (model) => {
          let callback =
            application::callback::<Myapp> (model.callback_id.unwrap());
          callback (self, model.component, node_id);
        }
        _ => {}
      }
    }
    interface.display();
    self.interface = Some (interface);
    log::trace!("...update");
  }

  //
  //  callbacks
  //
  fn quit (&mut self, _ : interface::model::Component, _ : tree::NodeId) {
    self.running = false;
  }
}

//
//  control funs
//
/// Submit the `Quit` callback
fn myquit (
  _             : &interface::controller::controls::button::Release,
  _elements     : &Tree <interface::Element>,
  node_id       : &tree::NodeId,
  action_buffer : &mut Vec <(tree::NodeId, interface::Action)>
) {
  log::trace!("myquit...");
  action_buffer.push (
    ( node_id.clone(),
      interface::Action::SubmitCallback (MycallbackIds::Quit.into())
    )
  );
  log::trace!("...myquit");
}

////////////////////////////////////////////////////////////////////////////////
//  main                                                                      //
////////////////////////////////////////////////////////////////////////////////

fn main() {
  println!("gooey curses defaults example: main...");

  report_sizes();

  println!("config:\n{}", serde_yaml::to_string (&*CONFIG).unwrap());

  // log init
  let log_file = Box::new (std::fs::File::create (LOG_FILENAME).unwrap());
  env_logger::Builder::new()
    .filter_level (CONFIG.log_level)
    .parse_default_env()
    .target (env_logger::Target::Pipe (log_file))
    .init();
  println!("logging output to {:?}", LOG_FILENAME);
  log::info!("log begin...");

  // create application
  let mut myapp = Myapp::new();
  myapp.interface().log_elements_tree();
  myapp.interface().log_elements_tree_names();

  // main loop
  while myapp.is_running() {
    myapp.update();
    std::thread::sleep (std::time::Duration::from_millis (FRAME_MS));
  }

  log::info!("...log end");
  println!("gooey curses defaults example: ...main");
}
