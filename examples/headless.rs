#![feature(pattern)]
#![warn(unused_extern_crates)]

use serde::{Deserialize, Serialize};
use env_logger;
use vec_map::VecMap;
use gooey::*;

#[allow(unused_macros)]
macro_rules! display {
  ($e:expr) => { println!("{}: {}", stringify!($e), $e); }
}
#[allow(unused_macros)]
macro_rules! show {
  ($e:expr) => { println!("{}: {:?}", stringify!($e), $e); }
}
#[allow(unused_macros)]
macro_rules! pretty {
  ($e:expr) => { println!("{}: {:#?}", stringify!($e), $e); }
}

const CONFIG_PATH_YAML : &'static str  = "Example.yaml";
const SOUND_GLOP : interface::view::Sound = interface::view::Sound (0);

#[derive(Deserialize, Serialize)]
pub struct Config {
  log_level : log::LevelFilter,
  bindings  : interface::controller::Bindings <Myapp>
}

pub struct Myapp {
  pub interface : Option <Interface <Self,
    presentation::Composite <presentation::Headless, presentation::Fmod>
  >>,
  running : bool
}

impl Application for Myapp {
  type CallbackIds     = Mycallbackids;
  type AxisControls    = interface::controller::controls::Nil;
  type ButtonControls  = interface::controller::controls::Nil;
  type MotionControls  = interface::controller::controls::Nil;
  type PointerControls = interface::controller::controls::Nil;
  type SystemControls  = interface::controller::controls::Nil;
  type TextControls    = Mytextcontrols;
  type WheelControls   = interface::controller::controls::Nil;
}

def_callback_ids!{
  Mycallbackids <Myapp> {
    Quit => Myapp::quit
  }
}

impl Myapp {
  pub fn new() -> Self {
    // init interface
    let interface = {
      // manually construct root node containing text control bindings and model
      // quit callback
      let root = {
        let controller = {
          let bindings = interface::controller::bindings::Builder::<Myapp>::new()
            .text (Mytextcontrols::Mytext.into())
            .build();
          interface::Controller::with_bindings (&bindings)
        };
        let model = interface::Model {
          callback_id: Some (Mycallbackids::Quit.into()),
          .. interface::Model::default()
        };
        interface::Element {
          controller, model, name: "Myroot".to_string(),
          .. interface::Element::default()
        }
      };
      // composite presentation has headless graphics and Fmod audio
      let mut interface = Interface::<Self,
        presentation::Composite <presentation::Headless, presentation::Fmod>
      >::with_root (root);

      // load presentation audio data
      interface.presentation.audio.audition.load_samples (vec![
        (SOUND_GLOP.0 as usize, "glop.wav".into())
      ].into_iter().collect::<VecMap <String>>());
      interface.presentation.audio.audition.update();

      // set root view component to loaded sfx resource; must take place after
      // sfx resource has been loaded above
      let root_id   = interface.focused_element_id().clone();
      let root_view = Box::new (|view : &mut interface::View| {
        use interface::view::component::{sfx, Sfx};
        let playback = Some ((SOUND_GLOP, sfx::State::Ready));
        view.component = Sfx { playback }.into();
      });
      let _ = interface
        .action (&root_id, interface::Action::ModifyView (root_view));
      Some (interface)
    };
    let running = true;

    Myapp { interface, running }
  }

  #[inline]
  pub fn is_running (&self) -> bool {
    self.running
  }

  /// Display prompt, update interface, handle interface events, and display
  /// interface
  pub fn update (&mut self) {
    use std::io::Write;
    // prompt
    print!("> ");
    let _  = std::io::stdout().flush();
    // interface update events
    let mut interface = self.interface.take().unwrap();
    let events = interface.update();
    for (node_id, event) in events {
      use interface::model;
      match event {
        model::Event::Submit (model) => {
          let callback =
            application::callback::<Myapp> (model.callback_id.unwrap());
          callback (self, model.component, node_id);
        }
        model::Event::Update (model) => {
          use std::convert::TryFrom;
          let text = String::try_from (model.component).unwrap();
          println!("announcement: {:?}", text);
        }
        _ => unreachable!()
      }
    }
    interface.display();
    self.interface = Some (interface);
  }

  //
  //  callbacks
  //
  /// Modifies application running flag to false, causing main loop to terminate
  pub fn quit (&mut self, _ : interface::model::Component, _ : tree::NodeId) {
    self.running = false;
  }
}

//
//  controls
//

extend_control!{
  Mytextcontrols <interface::view::input::Text> (
    interface::controller::controls::text, Text
  ) {
    Mytext => mytext
  }
}

/// Maps text commands to control functions on the target node:
///
/// - quit -- submit the quit callback
/// - play, loop, stop -- control the playback widget
///
/// Unrecognized commands are written to the model text component for the
/// application to process.
fn mytext (
  text          : &interface::view::input::Text,
  elements      : &Tree <interface::Element>,
  node_id       : &tree::NodeId,
  action_buffer : &mut Vec <(tree::NodeId, interface::Action)>
) {
  log::trace!("mytext...");
  let mut text = match text.clone() {
    interface::view::input::Text::String (text) => text,
    _ => unreachable!()
  };
  text.pop();
  let _             = text.trim_end();
  let announce_text = text.clone();
  let node_id       = node_id.clone();
  let announce = || {
    let announce_text = Box::new (|model : &mut interface::Model|
      model.component = interface::model::Component::Text (announce_text));
    action_buffer.push ((
      node_id.clone(), interface::Action::ModifyModel (announce_text)
    ));
  };
  if !text.is_empty() {
    let word_ct   = text.as_str().split_whitespace().count();
    let mut words = text.as_str().split_whitespace();
    match word_ct {
      0 => unreachable!("zero words in input readline parse"),
      1 => {
        use std::str::pattern::Pattern;
        let command = words.next().unwrap();
        if command.is_prefix_of("quit") {
          interface::widget::form::submit_callback (
            &None, elements, &node_id, action_buffer);
        } else if command.is_prefix_of("play") {
          interface::widget::playback::play (
            &None, elements, &node_id, action_buffer);
        } else if command.is_prefix_of("loop") {
          interface::widget::playback::loop_ (
            &None, elements, &node_id, action_buffer);
        } else if command.is_prefix_of("stop") {
          interface::widget::playback::ready (
            &None, elements, &node_id, action_buffer);
        } else {
          announce()
        }
      }
      _ => announce()
    }
  }
  log::trace!("...mytext");
}

fn main() {
  println!("gooey headless example: main...");

  // load config
  let cfg = {
    let config_file = std::fs::File::open (CONFIG_PATH_YAML).unwrap();
    serde_yaml::from_reader::<std::fs::File, Config> (config_file).unwrap()
  };
  display!(serde_yaml::to_string (&cfg).unwrap());
  // log init
  env_logger::Builder::new()
    .filter_level (cfg.log_level)
    .parse_default_env()
    .init();
  log::info!("terminal logger initialized");

  report_sizes();

  // init application
  let mut myapp = Myapp::new();
  //pretty!(myapp.interface);
  myapp.interface.as_ref().unwrap().print_elements_tree();
  myapp.interface.as_ref().unwrap().print_elements_tree_names();

  println!("\nWelcome\n");
  println!("commands:\n    \
    'play' -- play the sample ping wav\n    \
    'loop' -- loop the sample ping wav\n    \
    'stop' -- stop the sample ping wav\n    \
    'quit' -- quit");
  // main loop
  let mut iter = 0;
  while myapp.is_running() {
    log::trace!("main loop iter: {}", iter);
    myapp.update();
    iter += 1;
  }

  println!("gooey headless example: ...main");
}
