#![warn(unused_extern_crates)]

use std::fs;
use std::convert::TryFrom;
use env_logger;
use lazy_static::lazy_static;
use log;
use serde::{Deserialize, Serialize};
use nsys::gl;
use gooey::*;

const CONFIG_PATH_YAML  : &'static str = "Opengl.yaml";
const LOG_FILENAME      : &'static str = "opengl.log";
const LIPSUM_PATH       : &'static str = "lipsum.txt";
const SPLAT_PATH        : &'static str = "splat64.png";
const TEXTURE_SPLAT     : interface::view::Texture = interface::view::Texture {
  resource: presentation::opengl::RESOURCE_TEXTURES_64X64,
  index:    0
};
const _FPS_MAX           : u64          = 360;
const fn log_level_default() -> log::LevelFilter {
 log::LevelFilter::Info
}

lazy_static!{
  pub static ref CONFIG : Config = {
    let config_file = std::fs::File::open (CONFIG_PATH_YAML).unwrap();
    serde_yaml::from_reader::<std::fs::File, Config> (config_file).unwrap()
  };
  pub static ref SCREEN_CONTROLS : interface::controller::Controls =
    interface::controller::controls::Builder::new()
      .buttons (vec![
        interface::controller::controls::button::Builtin::FrameToggleOrientation
          .into(),
        Mybuttoncontrols::Myquit.into(),
        Mybuttoncontrols::Mysplit.into(),
        Mybuttoncontrols::Myjoin.into()
      ].into())
      .build();
  pub static ref FRAME_CONTROLS : interface::controller::Controls =
    interface::controller::controls::Builder::new()
      .buttons (vec![ Mybuttoncontrols::Myfocus.into() ].into())
      .build();
  pub static ref FOCUS_BINDINGS : interface::controller::Bindings <Myapp> =
    CONFIG.frame_bindings.get_bindings (&FRAME_CONTROLS);
}

#[derive(Deserialize, Serialize)]
pub struct Config {
  #[serde(default = "log_level_default")]
  pub log_level       : log::LevelFilter,
  pub screen_bindings : interface::controller::Bindings <Myapp>,
  pub frame_bindings  : interface::controller::Bindings <Myapp>,
  pub field_bindings  : interface::controller::Bindings <Myapp>,
  pub menu_bindings   : interface::controller::Bindings <Myapp>,
  pub numbox_bindings : interface::controller::Bindings <Myapp>
}

////////////////////////////////////////////////////////////////////////////////
//  application                                                               //
////////////////////////////////////////////////////////////////////////////////

#[derive(Debug)]
pub struct Myapp {
  interface : Option <Interface <Self, presentation::Opengl>>,
  running   : bool
}

impl Default for Myapp {
  fn default() -> Self {
    Self::new()
  }
}

impl Application for Myapp {
  type CallbackIds     = MycallbackIds;
  type AxisControls    = interface::controller::controls::Nil;
  type ButtonControls  = Mybuttoncontrols;
  type MotionControls  = interface::controller::controls::Nil;
  type PointerControls = interface::controller::controls::Nil;
  type SystemControls  = interface::controller::controls::Nil;
  type TextControls    = interface::controller::controls::Nil;
  type WheelControls   = interface::controller::controls::Nil;
}

//  controls
extend_control!{
  Mybuttoncontrols <interface::controller::controls::button::Release>
    (interface::controller::controls::button, Button)
  {
    Myquit  => myquit,
    Myclose => myclose,
    Myfocus => myfocus,
    Mysplit => mysplit,
    Myjoin  => myjoin
  }
}
//  callbacks
def_callback_ids! {
  MycallbackIds <Myapp> {
    Quit => Myapp::quit
  }
}

impl Myapp {
  fn new () -> Self {
    // init interface
    let interface = {
      use strum::EnumCount;
      use gooey::prelude::*;
      let mut interface = presentation::Opengl::make_interface();
      // debug: log gl info
      presentation::opengl::log_glium_info (
        &interface.presentation.inner().1.glium_display);
      // load mouse pointer
      const POINTER    : &'static [u8; 112] = include_bytes!("../pointer.png");
      const POINTER_ID : u16 =
        gl::render::resource::default::DefaultTexturePointerId::COUNT as u16;
      interface.presentation.load_pointer (POINTER_ID, POINTER, [0,0].into());
      interface.presentation.set_pointer_sensitivity (1.33);
      // root screen frame
      // TODO: modify existing screen?
      let (_screen_id, screen_tiles_id) = {
        let root_id = interface.focused_element_id().clone();
        let appearances = AppearancesBuilder::default()
          .pointer (State::Focused, POINTER_ID.into())
          .build();
        let Element { view, .. } = {
          let mut screen = frame::screen::PixelBuilder::default()
            .anchor      (Alignment::pixel())
            .appearances (appearances)
            .bindings    (&CONFIG.screen_bindings)
            .clear_color (Some (Color::white()).into())
            .dimensions  (interface.presentation.dimensions())
            .build_element();
          screen.focus();
          screen
        };
        let screen_view = Box::new (|v : &mut View| *v = view);
        let _ = interface.action (&root_id, Action::ModifyView (screen_view));
        let tiles_id = interface.presentation.screen_tiles_id().clone();
        { // add binding for screen tiles toggle orientation
          let tiles_controller = Box::new (|c : &mut Controller|
            c.add_bindings (
              &CONFIG.screen_bindings.get_bindings (&SCREEN_CONTROLS))
          );
          let _ = interface.action (&tiles_id,
            Action::ModifyController (tiles_controller));
        }
        (root_id, tiles_id)
      };
      let mut frame_number = 0u64;

      // callbacks to set frame numbers and focus bindings
      let set_frame_number   = |frame_number : u64|
        Box::new (move |m : &mut Model| m.component = frame_number.into());
      let add_frame_bindings = ||
        Box::new (|c : &mut Controller| c.add_bindings (&FOCUS_BINDINGS));

      // create child frames with focus bindings
      let focus_bindings = CONFIG.frame_bindings.get_bindings (&FRAME_CONTROLS);
      let appearances_text_frame = AppearancesBuilder::default()
        .style_fg (State::Enabled, Color::white())
        .style_bg (State::Enabled, Color::magenta())
        .style_fg (State::Focused, Color::white())
        .style_bg (State::Focused, Color::magenta())
        .build();
      let appearances_text = AppearancesBuilder::default()
        .style_fg (State::Enabled, Color::white())
        .style_bg (State::Enabled, Color::magenta())
        .style_fg (State::Focused, Color::magenta())
        .style_bg (State::Focused, Color::white())
        .build();

      let _frame0_id = {
        // frame0: red (100%, 5)
        let frame = {
          let layout = {
            let anchor = Alignment {
              horizontal: alignment::Horizontal::Left,
              vertical:   alignment::Vertical::Middle
            };
            let offset = Offset {
              horizontal: math::NormalSigned::<f32>::noisy (0.1).into(),
              vertical:   math::NormalSigned::<f32>::zero().into()
            };
            let size   = {
              use math::num_traits::One;
              let height = math::Normalized::<f32>::one().into();
              let width  = 5.into();
              Size { width, height }
            };
            layout::Free { anchor, offset, size }
          };
          let appearances = AppearancesBuilder::default()
            .style_fg (State::Enabled, Color::red())
            .style_bg (State::Enabled, Color::white())
            .style_fg (State::Focused, Color::white())
            .style_bg (State::Focused, Color::red())
            .build();
          let mut frame = frame::free::Builder::new (
            interface.elements(), &screen_tiles_id
          ) .appearances (appearances)
            .bindings (&CONFIG.frame_bindings)
            .border (Border::default())
            .layout (layout)
            .build_element();
          frame.model.component = frame_number.into();
          frame.controller.add_bindings (&focus_bindings);
          frame
        };
        interface.create_singleton (&screen_tiles_id, frame, CreateOrder::Append)
      };

      frame_number += 1;
      let _frame1_id = {
        // frame1: text, left aligned
        let (rows, columns)  = (10, 100);
        let text   = fs::read_to_string (LIPSUM_PATH).unwrap();
        let scroll = Scroll {
          offset:    Offset::default_absolute(),
          alignment: Alignment {
            horizontal: alignment::Horizontal::Left,
            vertical:   alignment::Vertical::Middle
          }
        };
        let layout = {
          let anchor = Alignment {
            horizontal: alignment::Horizontal::Center,
            vertical:   alignment::Vertical::Top
          };
          let offset = Offset { horizontal: 0.into(), vertical: 1.into() };
          let size   = Size { width: columns.into(), height: rows.into() };
          layout::Variant::from (layout::Free { anchor, offset, size })
        };
        let builder = textbox::Builder::new (
          interface.elements(), &screen_tiles_id
        ) .appearances (appearances_text.clone())
          .bindings (&CONFIG.frame_bindings)
          .frame_appearances (appearances_text_frame.clone())
          .frame_border (Border::default())
          .frame_layout (layout.into())
          .scroll (scroll)
          .text (text);
        let frame_id = build_and_return_node_id!(interface, builder);
        let _ = interface.action (&frame_id,
          Action::ModifyModel (set_frame_number (frame_number)));
        let _ = interface.action (&frame_id,
          Action::ModifyController (add_frame_bindings()));
        frame_id
      };

      frame_number += 1;
      let _frame2_id = {
        // frame2: text, centered
        let (rows, columns)  = (10, 100);
        let text   = fs::read_to_string (LIPSUM_PATH).unwrap();
        let scroll = Scroll {
          offset:    Offset::default_absolute(),
          alignment: Alignment::middle_center()
        };
        let layout = {
          let anchor = Alignment::middle_center();
          let size   = Size { width: columns.into(), height: rows.into() };
          let offset = Offset { horizontal: 0.into(), vertical: 0.into() };
          layout::Variant::from (layout::Free { anchor, offset, size })
        };
        let builder = textbox::Builder::new (
          interface.elements(), &screen_tiles_id
        ) .appearances (appearances_text.clone())
          .bindings (&CONFIG.frame_bindings)
          .frame_appearances (appearances_text_frame.clone())
          .frame_border (Border::default())
          .frame_layout (layout.into())
          .scroll (scroll)
          .text (text);
        let frame_id = build_and_return_node_id!(interface, builder);
        let _ = interface.action (&frame_id,
          Action::ModifyModel (set_frame_number (frame_number)));
        let _ = interface.action (&frame_id,
          Action::ModifyController (add_frame_bindings()));
        frame_id
      };

      frame_number += 1;
      let _frame3_id = {
        // frame3: text, right aligned
        let (rows, columns)  = (10, 100);
        let text   = fs::read_to_string (LIPSUM_PATH).unwrap();
        let scroll = Scroll {
          offset:    Offset::default_absolute(),
          alignment: Alignment {
            horizontal: alignment::Horizontal::Right,
            vertical:   alignment::Vertical::Middle
          }
        };
        let layout = {
          let anchor = Alignment {
            horizontal: alignment::Horizontal::Center,
            vertical:   alignment::Vertical::Bottom
          };
          let size   = Size { width: columns.into(), height: rows.into() };
          let offset = Offset { horizontal: 0.into(), vertical: 1.into() };
          layout::Variant::from (layout::Free { anchor, offset, size })
        };
        let builder = textbox::Builder::new (
          interface.elements(), &screen_tiles_id
        ) .appearances (appearances_text.clone())
          .bindings (&CONFIG.frame_bindings)
          .frame_appearances (appearances_text_frame.clone())
          .frame_border (Border::default())
          .frame_layout (layout.into())
          .scroll (scroll)
          .text (text);
        let frame_id = build_and_return_node_id!(interface, builder);
        let _ = interface.action (&frame_id,
          Action::ModifyModel (set_frame_number (frame_number)));
        let _ = interface.action (&frame_id,
          Action::ModifyController (add_frame_bindings()));
        frame_id
      };

      frame_number += 1;
      let frame4_id = {
        // frame4: cyan (30x40)
        let (rows, columns) = (30, 40);
        let layout = {
          let anchor = Alignment {
            horizontal: alignment::Horizontal::Right,
            vertical:   alignment::Vertical::Top
          };
          let size   = Size { width: columns.into(), height: rows.into() };
          let offset = Offset { horizontal: 15.into(), vertical: 2.into() };
          layout::Free { anchor, offset, size }
        };
        let appearances = AppearancesBuilder::default()
          .style_fg (State::Enabled, Color::cyan())
          .style_bg (State::Enabled, Color::white())
          .style_fg (State::Focused, Color::white())
          .style_bg (State::Focused, Color::cyan())
          .build();
        let mut frame = frame::free::Builder::new (
          interface.elements(), &screen_tiles_id
        ) .appearances (appearances)
          .bindings (&CONFIG.frame_bindings)
          .border (Border::default())
          .layout (layout)
          .build_element();
        frame.model.component = frame_number.into();
        frame.controller.add_bindings (&focus_bindings);
        interface.create_singleton (&screen_tiles_id, frame, CreateOrder::Append)
      };

      frame_number += 1;
      let _frame5_id = { // frame5: yellow text entry field (3x24)
        let width  = size::Unsigned::Absolute (22);
        let anchor = Alignment::tile();
        let offset = Offset { horizontal: 2.into(), vertical: 1.into() };
        let appearances_text = AppearancesBuilder::default()
          .style_fg (State::Enabled, Color::white())
          .style_bg (State::Enabled, Color::yellow())
          .style_fg (State::Focused, Color::yellow())
          .style_bg (State::Focused, Color::white())
          .build();
        let appearances_frame = AppearancesBuilder::default()
          .style_fg (State::Enabled, Color::white())
          .style_bg (State::Enabled, Color::yellow())
          .style_fg (State::Focused, Color::white())
          .style_bg (State::Focused, Color::yellow())
          .build();
        let builder = field::Builder::new (interface.elements(), &frame4_id)
          .appearances (appearances_text)
          .bindings (&CONFIG.field_bindings)
          .cursor (Cursor {
            ignore: [input::button::Keycode::Grave].into(),
            .. Default::default()
          })
          .frame_anchor (anchor)
          .frame_appearances (appearances_frame)
          .frame_bindings (&CONFIG.frame_bindings)
          .frame_border (Border::default())
          .frame_offset (offset)
          .frame_width (width);
        let frame_id = build_and_return_node_id!(interface, builder);
        let _ = interface.action (&frame_id,
          Action::ModifyModel (set_frame_number (frame_number)));
        let _ = interface.action (&frame_id,
          Action::ModifyController (add_frame_bindings()));
        frame_id
      };

      frame_number += 1;
      let _frame6_id = { // frame6: yellow number box
        let length = 4;
        let anchor = Alignment::tile();
        let offset = Offset { horizontal: 2.into(), vertical: 3.into() };
        let appearances_number = AppearancesBuilder::default()
          .style_fg (State::Enabled, Color::white())
          .style_bg (State::Enabled, Color::yellow())
          .style_fg (State::Focused, Color::yellow())
          .style_bg (State::Focused, Color::white())
          .build();
        let appearances_frame = AppearancesBuilder::default()
          .style_fg (State::Enabled, Color::white())
          .style_bg (State::Enabled, Color::yellow())
          .style_fg (State::Focused, Color::white())
          .style_bg (State::Focused, Color::yellow())
          .build();
        let numeral = Numeral {
          min: Some (-999.0),
          max: Some (999.0),
          .. Numeral::default()
        };
        let builder = numbox::Builder::new (interface.elements(), &frame4_id)
          .bindings (&CONFIG.numbox_bindings)
          .appearances (appearances_number)
          .frame_anchor (anchor)
          .frame_appearances (appearances_frame)
          .frame_bindings (&CONFIG.frame_bindings)
          .frame_border (Border::default())
          .frame_offset (offset)
          .length (length)
          .numeral (numeral);
        let frame_id = build_and_return_node_id!(interface, builder);
        let _ = interface.action (&frame_id,
          Action::ModifyModel (set_frame_number (frame_number)));
        let _ = interface.action (&frame_id,
          Action::ModifyController (add_frame_bindings()));
        frame_id
      };

      frame_number += 1;
      { // frame7: image "splat64.png" (64x64)
        // load image
        presentation::opengl::render::load_textures (
          &mut interface.presentation.inner_mut().1,
          &[],
          &[SPLAT_PATH],
          &[]
        );
        let pixmap   = Pixmap::Texture {
          resource_id:  TEXTURE_SPLAT.resource,
          index:        TEXTURE_SPLAT.index,
          width:        64,
          height:       64
        };
        let anchor   = Alignment {
          horizontal: alignment::Horizontal::Right,
          vertical:   alignment::Vertical::Bottom
        };
        let offset   = Offset { horizontal: 4.into(), vertical: 2.into() };
        let appearances = AppearancesBuilder::default()
          .style_fg (State::Enabled, Color::black())
          .style_bg (State::Enabled, Color::white())
          .style_fg (State::Focused, Color::white())
          .style_bg (State::Focused, Color::black())
          .build();
        let builder = picture::FitBuilder::new (interface.elements(), &frame4_id)
          .anchor (anchor)
          .appearances (appearances)
          .bindings (&CONFIG.frame_bindings)
          .border (Border::default())
          .coord_kind_override (coordinates::Kind::Pixel)
          .offset (offset)
          .pixmap (pixmap);
        let frame_id = build_and_return_node_id!(interface, builder);
        let _ = interface.action (&frame_id,
          Action::ModifyModel (set_frame_number (frame_number)));
        let _ = interface.action (&frame_id,
          Action::ModifyController (add_frame_bindings()));
      }

      let hello_id = {
        // textbox: "hello world"
        let text = "hello world".to_string();
        let anchor = Alignment::middle_center();
        let offset = Offset {
          horizontal: 0.into(), vertical: 0.into()
        };
        let appearances = AppearancesBuilder::default()
          .style_fg (State::Focused, Color::white())
          .style_bg (State::Focused, Color::green())
          .build();
        let builder = textbox::FitBuilder::<Myapp>::new (
          interface.elements(), &screen_tiles_id
        ) .frame_anchor (anchor)
          .frame_appearances (appearances)
          .frame_border (Border::default())
          .frame_offset (offset)
          .text (text);
        let hello_id = build_and_return_node_id!(interface, builder);
        let add_close_binding = Box::new (|c : &mut Controller|
          c.add_bindings (&Bindings::<Myapp> {
            any_button: Some (Mybuttoncontrols::Myclose.into()),
            .. Bindings::empty()
          })
        );
        let _ = interface.action (&hello_id,
          Action::ModifyController (add_close_binding));
        hello_id
      };

      { // menu
        let selection = Selection { loop_: true, .. Selection::default() };
        let actions = menu::Builder::new (interface.elements(), &screen_tiles_id)
          .bindings (&CONFIG.menu_bindings)
          .selection (selection)
          .build_actions();
        let _ = interface.actions (actions);
      }

      // focus hello
      let _ = interface.action (&hello_id, Action::Focus);

      interface
    };
    //log::debug!("interface: {:#?}", interface);

    Myapp {
      interface:  Some (interface),
      running:    true
    }
  }

  #[inline]
  fn is_running (&self) -> bool {
    self.running
  }

  #[inline]
  fn interface (&self) -> &Interface <Self, presentation::Opengl> {
    self.interface.as_ref().unwrap()
  }

  fn update (&mut self) {
    log::trace!("update...");
    // update interface and handle events
    let mut interface = self.interface.take().unwrap();
    let events = interface.update();
    for (node_id, event) in events {
      match event {
        interface::model::Event::Submit (model) => {
          let callback =
            application::callback::<Myapp> (model.callback_id.unwrap());
          callback (self, model.component, node_id);
        }
        _ => {}
      }
    }
    interface.display();
    self.interface = Some (interface);
    log::trace!("...update");
  }

  /// Utility function to return the current max frame number
  fn frame_max (elements : &Tree <interface::Element>) -> u64 {
    use interface::{model::component::Kind, widget, Widget};
    let root_id = elements.root_node_id().unwrap();
    elements.traverse_pre_order (root_id).unwrap().skip (2)
      .filter_map (|node| {
        let Widget (_, model, _) = widget::Frame::try_from (node.data()).ok()?;
        u64::try_ref (model).cloned()
      }).max().unwrap()
  }

  //
  //  callbacks
  //
  fn quit (&mut self, _ : interface::model::Component, _ : tree::NodeId) {
    self.running = false;
  }
}

//
//  control funs
//
/// Submit the `Quit` callback
fn myquit (
  _             : &interface::controller::controls::button::Release,
  _elements     : &Tree <interface::Element>,
  node_id       : &tree::NodeId,
  action_buffer : &mut Vec <(tree::NodeId, interface::Action)>
) {
  log::trace!("myquit...");
  action_buffer.push (
    ( node_id.clone(),
      interface::Action::SubmitCallback (MycallbackIds::Quit.into()) ));
  log::trace!("...myquit");
}

/// Close the "hello world" frame and focus on frame1
fn myclose (
  _             : &interface::controller::controls::button::Release,
  elements      : &Tree <interface::Element>,
  node_id       : &tree::NodeId,
  action_buffer : &mut Vec <(tree::NodeId, interface::Action)>
) {
  log::trace!("myclose...");
  interface::widget::frame::close (&None, elements, node_id, action_buffer);
  let mut focused = false;
  let root_id = elements.root_node_id().unwrap();
  for element_id in elements.traverse_pre_order_ids (root_id).unwrap() {
    match interface::widget::Frame::try_get (elements, &element_id) {
      Ok (
        interface::Widget (_, interface::model::Component::Unsigned (1), _)
      ) => {
        let focus_id = match elements.children_ids (&element_id).unwrap().next() {
          Some (child_id) => child_id.clone(),
          None            => element_id.clone()
        };
        action_buffer.push ((focus_id, interface::Action::Focus));
        focused = true;
        break
      }
      _ => {}
    }
  }
  assert!(focused);
  log::trace!("...myclose");
}

/// Switch focus between nodes
fn myfocus (
  _             : &interface::controller::controls::button::Release,
  elements      : &Tree <interface::Element>,
  node_id       : &tree::NodeId,
  action_buffer : &mut Vec <(tree::NodeId, interface::Action)>
) {
  use interface::model::component::Kind;
  log::trace!("myfocus...");
  let root_id     = elements.root_node_id().unwrap();
  let frame_count = Myapp::frame_max (elements) + 1;
  let node_number =
    u64::try_ref (&elements.get_element (node_id).model.component).unwrap();
  // frame nodes are numbered 0-frame_count-1
  let next_number = (node_number+1) % frame_count;
  let next_id = elements.traverse_pre_order_ids (root_id).unwrap().skip (1)
    .find (|node_id|
      if let Some (frame_number) =
        u64::try_ref (&elements.get_element (node_id).model.component)
      {
        next_number == *frame_number
      } else {
        false
      }
    ).unwrap();
  let focus_id = match elements.children_ids (&next_id).unwrap().next() {
    Some (child_id) => {
      if interface::widget::Frame::try_get (elements, &child_id).is_err() {
        child_id.clone()
      } else {
        next_id
      }
    }
    None => next_id
  };
  action_buffer.push ((focus_id, interface::Action::Focus));
  log::trace!("...myfocus");
}

/// Prepend a new tiled child frame to the root screen frame
fn mysplit (
  _             : &interface::controller::controls::button::Release,
  elements      : &Tree <interface::Element>,
  node_id       : &tree::NodeId,
  action_buffer : &mut Vec <(tree::NodeId, interface::Action)>
) {
  use gooey::prelude::*;
  log::trace!("mysplit...");
  // alternate blue/green color, starting with blue
  let bg_color = {
    let second_child = elements.children (node_id).unwrap().skip (1).next()
      .unwrap().data();
    let Widget (layout, _, _) = Frame::try_from (second_child).unwrap();
    match layout.variant {
      layout::Variant::Tiled (_) => {
        let bg = second_child.controller.appearances.get (State::Enabled).style
          .clone().unwrap().bg;
        if bg == Color::blue() {
           Color::green()
        } else if bg == Color::green() {
           Color::blue()
        } else {
          unreachable!()
        }
      },
      _ => Color::blue()
    }
  };
  let appearances = AppearancesBuilder::default()
    .style_bg (State::Enabled, bg_color)
    .style_bg (State::Focused, Color::yellow())
    .build();
  let frame_number = Myapp::frame_max (elements) + 1;
  let layout = layout::Tiled::default();
  let actions = {
    let mut actions = frame::tiled::Builder::new (elements, node_id)
      .appearances (appearances)
      .bindings (&CONFIG.frame_bindings)
      .create_order (CreateOrder::NthSibling (1))
      .layout (layout)
      .build_actions();
    match actions[0].1 {
      Action::Create (ref mut subtree, _) => {
        let focus_bindings = CONFIG.frame_bindings
          .get_bindings (&FRAME_CONTROLS);
        let root_id = subtree.root_node_id().unwrap().clone();
        let frame   = subtree.get_mut (&root_id).unwrap().data_mut();
        frame.model.component = frame_number.into();
        frame.controller.add_bindings (&focus_bindings);
      }
      _ => unreachable!()
    }
    actions
  };
  action_buffer.extend (actions);

  log::trace!("...mysplit");
}

/// Pop the first tiled child frame of the root screen frame
fn myjoin (
  _             : &interface::controller::controls::button::Release,
  elements      : &Tree <interface::Element>,
  node_id       : &tree::NodeId,
  action_buffer : &mut Vec <(tree::NodeId, interface::Action)>
) {
  use interface::{widget, Widget};
  use interface::controller::component::layout;
  log::trace!("myjoin...");
  let mut child_ids   = elements.children_ids (node_id).unwrap().skip (1);
  let second_child_id = child_ids.next().unwrap().clone();
  let second_child    = elements.get_element (&second_child_id);
  let Widget (layout, _, _) = widget::Frame::try_from (second_child).unwrap();
  match layout.variant {
    layout::Variant::Tiled (_) =>
      widget::frame::close (&None, elements, &second_child_id, action_buffer),
    _ => {}
  }
  log::trace!("...myjoin");
}

////////////////////////////////////////////////////////////////////////////////
//  main                                                                      //
////////////////////////////////////////////////////////////////////////////////

fn main() {
  println!("gooey opengl example: main...");

  report_sizes();
  //println!("config:\n{}", serde_yaml::to_string (&CONFIG).unwrap());

  // log init
  let log_file = Box::new (std::fs::File::create (LOG_FILENAME).unwrap());
  env_logger::Builder::new()
    .filter_level (CONFIG.log_level)
    .parse_default_env()
    .target (env_logger::Target::Pipe (log_file))
    .init();
  println!("logging output to {:?}", LOG_FILENAME);
  log::info!("log begin...");

  // create application
  let mut myapp = Myapp::new();
  myapp.interface().log_elements_tree();
  myapp.interface().log_elements_tree_names();

  // main loop
  while myapp.is_running() {
    myapp.update();
    // TODO: fps limit
  }

  log::info!("...log end");
  println!("gooey opengl example: ...main");
}
