#![warn(unused_extern_crates)]

use env_logger;
use lazy_static::lazy_static;
use log;
use nsys::gl;
use gooey::*;

const LOG_FILENAME      : &'static str = "toggle-test.log";
const _FPS_MAX           : u64          = 360;
const fn log_level_default() -> log::LevelFilter {
  log::LevelFilter::Info
}

lazy_static!{
  static ref MENU_BINDINGS : interface::controller::Bindings <Myapp> = {
    use gooey::prelude::*;
    bindings::Builder::<Myapp>::new()
      .buttons (vec![
        ( input::button::Keycode::Right.into(),
          controls::button::Builtin::MenuNextItem.into()
        ).into(),
        ( input::button::Keycode::Left.into(),
          controls::button::Builtin::MenuPreviousItem.into()
        ).into(),
        ( input::button::Keycode::Tab.into(),
          controls::button::Builtin::MenuNextItem.into()
        ).into(),
        ( input::Button {
           variant:   input::button::Keycode::Tab.into(),
           modifiers: input::Modifiers::SHIFT
          },
          controls::button::Builtin::MenuPreviousItem.into()
        ).into()
      ])
      .build()
  };
}

////////////////////////////////////////////////////////////////////////////////
//  application                                                               //
////////////////////////////////////////////////////////////////////////////////

#[derive(Debug)]
pub struct Myapp {
  interface : Option <Interface <Self, presentation::Opengl>>,
  running   : bool
}

impl Default for Myapp {
  fn default() -> Self {
    Self::new()
  }
}

impl Application for Myapp {
  type CallbackIds     = MycallbackIds;
  type AxisControls    = interface::controller::controls::Nil;
  type ButtonControls  = Mybuttoncontrols;
  type MotionControls  = interface::controller::controls::Nil;
  type PointerControls = interface::controller::controls::Nil;
  type SystemControls  = interface::controller::controls::Nil;
  type TextControls    = interface::controller::controls::Nil;
  type WheelControls   = interface::controller::controls::Nil;
}

//  controls
extend_control!{
  Mybuttoncontrols <interface::controller::controls::button::Release>
    (interface::controller::controls::button, Button)
  {
    Myquit => myquit
  }
}
//  callbacks
def_callback_ids! {
  MycallbackIds <Myapp> {
    Quit => Myapp::quit
  }
}

impl Myapp {
  fn new () -> Self {
    // init interface
    let interface = {
      use strum::EnumCount;
      use gooey::prelude::*;
      let mut interface = presentation::Opengl::make_interface();
      // debug: log gl info
      presentation::opengl::log_glium_info (
        &interface.presentation.inner().1.glium_display);
      // load mouse pointer
      const POINTER    : &'static [u8; 112] = include_bytes!("../pointer.png");
      const POINTER_ID : u16 =
        gl::render::resource::default::DefaultTexturePointerId::COUNT as u16;
      interface.presentation.load_pointer (POINTER_ID, POINTER, [0,0].into());
      // root screen frame
      // TODO: modify existing screen?
      let (_screen_id, screen_tiles_id) = {
        let root_id = interface.focused_element_id().clone();
        let appearances = AppearancesBuilder::default()
          .pointer (State::Focused, POINTER_ID.into())
          .build();
        let Element { view, .. } = {
          let mut screen = frame::screen::PixelBuilder::<Myapp>::default()
            .anchor      (Alignment::pixel())
            .appearances (appearances)
            .clear_color (Some (Color::white()).into())
            .dimensions  (interface.presentation.dimensions())
            .build_element();
          screen.focus();
          screen
        };
        let screen_view = Box::new (|v : &mut View| *v = view);
        let _ = interface.action (&root_id, Action::ModifyView (screen_view));
        let tiles_id = interface.presentation.screen_tiles_id().clone();
        {
          let bindings = bindings::Builder::<Myapp>::new()
            .buttons (vec![
              ( input::button::Keycode::Q.into(),
                Mybuttoncontrols::Myquit.into()
              ).into(),
              ( input::button::Mouse::Mouse1.into(),
                controls::button::Builtin::ActivatePointer.into()
              ).into()
            ])
            .build();
          let tiles_controller = Box::new (move |c : &mut Controller|
            c.add_bindings (&bindings)
          );
          let _ = interface.action (&tiles_id,
            Action::ModifyController (tiles_controller));
        }
        (root_id, tiles_id)
      };

      { // menu widget
        let selection = Selection { loop_: true, .. Selection::default() };
        let actions   = menu::Builder::new (
          interface.elements(), &screen_tiles_id
        ) .bindings (&*MENU_BINDINGS)
          .selection (selection)
          .build_actions();
        let _ = interface.actions (actions);
      }

      // switch controller
      let switch_builder = switch::Builder::default()
        .style_fg ((switch::State::On,  State::Enabled), Color::cyan())
        .style_bg ((switch::State::On,  State::Enabled), Color::white())
        .style_fg ((switch::State::Off, State::Enabled), Color::white())
        .style_bg ((switch::State::Off, State::Enabled), Color::cyan())
        .style_fg ((switch::State::On,  State::Focused), Color::red())
        .style_bg ((switch::State::On,  State::Focused), Color::white())
        .style_fg ((switch::State::Off, State::Focused), Color::white())
        .style_bg ((switch::State::Off, State::Focused), Color::red())
        .toggle (true);
      let bindings = bindings::Builder::<Myapp>::new()
        .buttons (vec![
          ( input::button::Keycode::Enter.into(),
            controls::button::Builtin::ButtonPush.into()
          ).into()
        ])
        .build();

      {  // button0: checkbox
        let layout = {
          let anchor  = Alignment::middle_center();
          let offset  = Offset::default_absolute();
          let size    = Size {
            width:  3.into(),
            height: 3.into()
          };
          layout::Variant::from (layout::Free { anchor, offset, size })
        };
        let switch = switch_builder.clone().label_on ("X".to_string()).build();
        let builder = button::Builder::new (
          interface.elements(), &screen_tiles_id
        ) .bindings (&bindings)
          .frame_border (Border::default())
          .frame_layout (layout.into())
          .switch (switch);
        let _ = build_and_return_node_id!(interface, builder);
      }

      {  // button1: toggle
        let layout = {
          let anchor  = Alignment::middle_center();
          let offset  = Offset::default_absolute();
          let size    = Size {
            width:  4.into(),
            height: 3.into()
          };
          layout::Variant::from (layout::Free { anchor, offset, size })
        };
        let switch = switch_builder.clone()
          .label_off ("\u{FF} ".to_string())
          .label_on (" \u{FF}".to_string())
          .build();
        let builder = button::Builder::new (
          interface.elements(), &screen_tiles_id
        ) .bindings (&bindings)
          .frame_border (Border::default())
          .frame_layout (layout.into())
          .switch (switch);
        let _ = build_and_return_node_id!(interface, builder);
      }

      {  // button2: vertical toggle
        let layout = {
          let anchor  = Alignment::middle_center();
          let offset  = Offset::default_absolute();
          let size    = Size {
            width:  3.into(),
            height: 4.into()
          };
          layout::Variant::from (layout::Free { anchor, offset, size })
        };
        let switch = switch_builder.clone()
          .label_off (" \n\u{FF}".to_string())
          .label_on ("\u{FF}\n ".to_string())
          .build();
        let builder = button::Builder::new (
          interface.elements(), &screen_tiles_id
        ) .bindings (&bindings)
          .frame_border (Border::default())
          .frame_layout (layout.into())
          .switch (switch);
        let _ = build_and_return_node_id!(interface, builder);
      }

      { // arrange buttons
        let anchor = Alignment::middle_center();
        let actions = frame::free::rearrange_absolute (
          interface.elements(), &screen_tiles_id, anchor, (3, 1),
          Orientation::Vertical, 1, false
        );
        let _ = interface.actions (actions);
      }

      let _ = interface.action (&screen_tiles_id, Action::Focus);

      interface
    };
    //log::debug!("interface: {:#?}", interface);

    Myapp {
      interface:  Some (interface),
      running:    true
    }
  }

  #[inline]
  fn is_running (&self) -> bool {
    self.running
  }

  #[inline]
  fn interface (&self) -> &Interface <Self, presentation::Opengl> {
    self.interface.as_ref().unwrap()
  }

  fn update (&mut self) {
    log::trace!("update...");
    // update interface and handle events
    let mut interface = self.interface.take().unwrap();
    let events = interface.update();
    for (node_id, event) in events {
      match event {
        interface::model::Event::Submit (model) => {
          let callback =
            application::callback::<Myapp> (model.callback_id.unwrap());
          callback (self, model.component, node_id);
        }
        _ => {}
      }
    }
    interface.display();
    self.interface = Some (interface);
    log::trace!("...update");
  }

  //
  //  callbacks
  //
  fn quit (&mut self, _ : interface::model::Component, _ : tree::NodeId) {
    self.running = false;
  }
}

//
//  control funs
//
/// Submit the `Quit` callback
fn myquit (
  _             : &interface::controller::controls::button::Release,
  _elements     : &Tree <interface::Element>,
  node_id       : &tree::NodeId,
  action_buffer : &mut Vec <(tree::NodeId, interface::Action)>
) {
  log::trace!("myquit...");
  action_buffer.push (
    ( node_id.clone(),
      interface::Action::SubmitCallback (MycallbackIds::Quit.into()) ));
  log::trace!("...myquit");
}

////////////////////////////////////////////////////////////////////////////////
//  main                                                                      //
////////////////////////////////////////////////////////////////////////////////

fn main() {
  println!("gooey size test example: main...");

  report_sizes();
  //println!("config:\n{}", serde_yaml::to_string (&CONFIG).unwrap());

  // log init
  let log_file = Box::new (std::fs::File::create (LOG_FILENAME).unwrap());
  env_logger::Builder::new()
    .filter_level (log_level_default())
    .parse_default_env()
    .target (env_logger::Target::Pipe (log_file))
    .init();
  println!("logging output to {:?}", LOG_FILENAME);
  log::info!("log begin...");

  // create application
  let mut myapp = Myapp::new();
  myapp.interface().log_elements_tree();
  myapp.interface().log_elements_tree_names();

  // main loop
  while myapp.is_running() {
    myapp.update();
    // TODO: fps limit
  }

  log::info!("...log end");
  println!("gooey size test example: ...main");
}
