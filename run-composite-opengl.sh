#!/usr/bin/env bash

set -x

RUST_BACKTRACE=1 cargo run --example composite-opengl \
  --features="opengl-x11 fmod-audio" $@ && tail -n 20 composite-opengl.log

exit
