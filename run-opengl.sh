#!/usr/bin/env bash

set -x

#export RUST_LOG="gooey::presentation::opengl=trace"
RUST_BACKTRACE=1 cargo run --example opengl --features="opengl-x11" $@ &&
  tail -n 20 opengl.log

exit
