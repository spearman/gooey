#!/usr/bin/env bash

set -x

RUST_BACKTRACE=1 cargo run --example composite-remote \
  --features="opengl-x11 fmod-audio" $@ && tail -n 20 composite-remote.log

exit
