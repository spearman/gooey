#!/usr/bin/env bash

set -x

export RUSTFLAGS="-Z linker-features=-lld"
RUST_BACKTRACE=1 cargo run --example headless --features="fmod-audio" $@

exit
