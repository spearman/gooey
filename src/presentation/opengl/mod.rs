use std;
use std::convert::TryFrom;
use std::time;
use log;

use nsys::{self, gl, math::Vector2};
use nsys::gl::glium::{self, glutin};
use nsys::gl::winit;

use crate::{Application, Interface, Tree};
use crate::tree::NodeId;
use crate::interface::{self, view, View};
use super::{Graphics, Presentation};

pub mod remote;
pub mod render;

pub use self::remote::Remote;

pub const RESOURCE_TEXTURES_16X16   : u16 = 0;
pub const RESOURCE_TEXTURES_64X64   : u16 = 1;
pub const RESOURCE_TEXTURES_ANYSIZE : u16 = 2;

pub const POINTER_TEXTURE_INDEX : u16 = 0;

/// An OpenGL presentation backend
pub struct Opengl {
  pub draw_crosshair : bool,
  pub inner     : (winit::event_loop::EventLoop <()>, gl::Render),
  input_state   : InputState,
  tileset_id    : Option <gl::render::resource::default::DefaultTilesetId>,
  glium_frame   : Option <glium::Frame>,
  frame         : u64,
  screen_tiles  : Option <NodeId>
}

pub (crate) struct InputState {
  pub dimensions         : view::dimensions::Pixel,
  // winit doesn't include modifiers state in key or mouse events so we have to
  // track it
  pub modifiers          : winit::keyboard::ModifiersState,
  // due to the implementation of winit, CursorMoved events are not generated on
  // windows when the mouse is hidden and grabbed so we need to create a virtual
  // pointer position and update it based on device MouseMotion events
  pub pointer_hv         : (f32, f32),
  pub pointer_sensitivity : f32
}

pub (crate) struct InputHandler <'a> {
  pub input_buffer : &'a mut Vec <view::Input>,
  pub input_state  : &'a mut InputState,
}

/// Log glium info
pub fn log_glium_info (
  glium_display : &glium::Display <glutin::surface::WindowSurface>
) {
  let glium_info = gl::info::Glium::new (glium_display);
  log::info!("{:#?}", glium_info);
}

impl Opengl {
  #[inline]
  pub fn inner (&self) -> &(winit::event_loop::EventLoop <()>, gl::Render) {
    &self.inner
  }
  #[inline]
  pub fn inner_mut (&mut self)
    -> &mut (winit::event_loop::EventLoop <()>, gl::Render)
  {
    &mut self.inner
  }
  #[inline]
  pub fn screen_tiles_id (&self) -> &NodeId {
    self.screen_tiles.as_ref().unwrap()
  }
  #[inline]
  pub fn dimensions (&self) -> view::dimensions::Pixel {
    let inner_size : (u32, u32) =
      self.inner.1.window.inner_size().into();
    Vector2::from (inner_size).into()
  }
  pub fn pointer_sentivity (&self) -> f32 {
    self.input_state.pointer_sensitivity
  }
  pub fn set_pointer_sensitivity (&mut self, pointer_sensitivity : f32) {
    self.input_state.pointer_sensitivity = pointer_sensitivity
  }
  pub fn load_pointer (&mut self,
    key    : gl::render::resource::default::PointerTextureIndexRepr,
    bytes  : &[u8],
    offset : Vector2 <i16>
  ) {
    let render  = &mut self.inner_mut().1;
    render::load_pointer (render, key, bytes, offset);
  }
}

impl std::fmt::Debug for Opengl {
  fn fmt (&self, f : &mut std::fmt::Formatter) -> Result <(), std::fmt::Error> {
    write!(f, "Opengl {{ inner: ({:?}, Render), frame: {} }}",
      self.inner.0, self.frame)
  }
}

impl Default for Opengl {
  fn default() -> Self {
    // TODO: tileset parameter ?
    let tileset_id =
      Some (gl::render::resource::default::DefaultTilesetId::EasciiAcorn128);
    let input_state = InputState::new();
    let inner = {
      let (event_loop, window, _config, display) =
        gl::init::glium_init_gl33core ("Gooey Opengl Window");
      // hide & grab cursor
      window.set_cursor_visible (false);
      window.set_cursor_grab (winit::window::CursorGrabMode::Confined).unwrap();
      let render =
        gl::Render::<gl::render::resource::Default>::new (display, window);
      // set the tile dimensions
      let [tile_width, tile_height] =
        render.resource.tile_dimensions (tileset_id.unwrap_or_default());
      std::env::set_var ("GOOEY_TILE_WIDTH", tile_width.to_string());
      std::env::set_var ("GOOEY_TILE_HEIGHT", tile_height.to_string());
      (event_loop, render)
    };
    Opengl {
      inner, input_state, tileset_id,
      glium_frame:    None,
      frame:          0,
      screen_tiles:   None,
      draw_crosshair: false
    }
  }
}

impl Graphics     for Opengl { }
impl Presentation for Opengl {
  /// Expects a root Canvas component
  fn with_root (root : View, _id : NodeId) -> Self {
    use gl::render::resource::default::{draw2d, MAIN_VIEWPORT};
    let mut opengl = Self::default();
    {
      let draw_crosshair = opengl.draw_crosshair;
      let render = &mut opengl.inner_mut().1;
      render::set_screen_view (render, &root);
      let resources = draw2d::ViewportResources {
        draw_crosshair, .. Default::default()
      };
      render.resource.draw2d.viewport_resources_set (MAIN_VIEWPORT, resources);
    }
    log::debug!("opengl with root: {:?}", opengl);
    opengl
  }

  /// Create a new interface with a root screen pixel frame element and child
  /// screen tile frame element
  fn make_interface <A : Application> () -> Interface <A, Self> {
    use interface::{controller, model};
    use interface::view::component::{Canvas, Kind};
    use interface::widget::{self, BuildElement};
    let screen = {
      // create a root screen frame and rely on the resize control function to
      // update the dimensions
      let mut screen = widget::frame::screen::PixelBuilder::<A>::new()
        .anchor (controller::Alignment::pixel())
        .build_element();
      // NOTE: viewport width/height must be non-zero
      let canvas = Canvas::try_ref_mut (&mut screen.view.component).unwrap();
      canvas.coordinates.modify_dimensions_horizontal (1);
      canvas.coordinates.modify_dimensions_vertical (1);
      screen
    };
    // create interface with screen root
    let mut interface = Interface::<A, Opengl>::with_root (screen);
    // create child tile frame
    let screen_id     = interface.root_id().clone();
    let screen_tiles  = widget::frame::free::Builder::<A>::new (
      interface.elements(), &screen_id
    ) .layout (controller::component::layout::Free {
        size: controller::Size::fill(),
        .. controller::component::layout::Free::default_tile()
      }.into())
      .coord_kind_override (view::coordinates::Kind::Tile)
      .build_element();
    let tiles_id =
      match interface.action (&screen_id, interface::Action::create_singleton (
        screen_tiles, interface::CreateOrder::Append)).next().unwrap()
      {
        (_, model::Event::Create (_, id, _)) => id,
        _ => unreachable!()
      };
    interface.presentation.screen_tiles = Some (tiles_id);
    interface
  }

  fn get_input (&mut self, input_buffer : &mut Vec <view::Input>) {
    use winit::platform::pump_events::EventLoopExtPumpEvents;
    log::trace!("get input...");
    // TODO: we may want to ignore some extra winit events, e.g. window axis
    // motion events or keyboard device events on linux
    self.inner.0.pump_app_events (
      Some (time::Duration::ZERO),
      &mut InputHandler { input_buffer, input_state: &mut self.input_state });
    log::trace!("...get input");
  }

  fn display_view <V : AsRef <View>> (&mut self,
    view_tree      : &Tree <V>,
    _display_values : std::vec::Drain <(NodeId, view::Display)>
  ) {
    // update the renderer with the current view
    let tileset_id     = self.tileset_id;
    let draw_crosshair = self.draw_crosshair;
    render::update (&mut self.inner_mut().1, tileset_id, view_tree,
      draw_crosshair);
    // render the frame
    self.inner.1.do_frame (self.glium_frame.as_mut());
    self.frame += 1;
    log::trace!("...display view");
  }
}

impl <'a> InputHandler <'a> {
  fn handle_event (&mut self, event : winit::event::Event <()>) {
    match self.input_state.try_convert_winit_event (event) {
      Ok (input) => {
        log::debug!("input: {:?}", input);
        self.input_buffer.extend (input)
      }
      Err (event) => log::trace!("ignored winit event: {:?}", event)
    }
  }
}

impl InputState {
  fn new() -> Self {
    InputState {
      dimensions:          Default::default(),
      modifiers:           Default::default(),
      // pointer is initialized to (-1,-1) until the first resize event is
      // received at which point it will be moved to the center of the screen
      pointer_hv:          (-1.0, -1.0),
      pointer_sensitivity: 1.0
    }
  }
  // NOTE: we possibly return multiple input events because winit no longer
  // emits separate "text" events, so a key event can generate both a button
  // event and a text event
  fn try_convert_winit_event <T> (&mut self, event : winit::event::Event <T>)
    -> Result <Vec <view::Input>, winit::event::Event <T>>
    where T : std::fmt::Debug
  {
    use winit::event::{self, Event};
    use view::input;
    log::trace!("winit event: {:?}", event);
    let winit_event = event; // rename to prevent shadowing
    let input = match &winit_event {
      Event::NewEvents (_) => return Err (winit_event),
      Event::WindowEvent { event, .. } => match event {
        event::WindowEvent::ModifiersChanged (modifiers) => {
          self.modifiers = modifiers.state();
          return Err (winit_event)
        }
        event::WindowEvent::AxisMotion {..}    => return Err (winit_event),
        event::WindowEvent::CloseRequested     => input::System::Close.into(),
        event::WindowEvent::CursorEntered {..} => input::System::CursorEntered
          .into(),
        event::WindowEvent::CursorLeft {..}    => input::System::CursorLeft
          .into(),
        // NOTE: due to the implementation of winit, CursorMoved events are not
        // generated on windows when the mouse is hidden and grabbed so we need
        // to create a virtual pointer position and update it based on device
        // MouseMotion events
        event::WindowEvent::CursorMoved {..}   => return Err (winit_event),
        event::WindowEvent::RedrawRequested => input::System::Redraw.into(),
        event::WindowEvent::Destroyed => input::System::Destroyed.into(),
        event::WindowEvent::Focused (focused) =>
          input::System::Focused (*focused).into(),
        event::WindowEvent::KeyboardInput { event, is_synthetic: false, .. } => {
          let input = (event, self.modifiers).into();
          if event.state == event::ElementState::Pressed {
            if let Some (mut text) = event.text.clone()
              .map (|text| text.to_string())
            {
              let text_input = if text.len() > 1 {
                input::Text::String (text)
              } else {
                debug_assert_eq!(text.len(), 1);
                input::Text::Char (text.pop().unwrap())
              }.into();
              return Ok (vec![input, text_input])
            }
          }
          input
        }
        event::WindowEvent::KeyboardInput { is_synthetic: true, .. } =>
          return Err (winit_event),
        // NOTE: allowing use of deprecated modifiers until winit issue #1824 is
        // resolved
        #[allow(deprecated)]
        event::WindowEvent::MouseInput { button, state, .. } =>
          mouse_button (*button, self.modifiers, *state),
        // NOTE: allowing use of deprecated modifiers until winit issue #1824 is
        // resolved
        #[allow(deprecated)]
        event::WindowEvent::MouseWheel { delta, .. } =>
          mouse_wheel (*delta, self.modifiers),
        event::WindowEvent::Resized (logical_size) => {
          // TODO: convert logical coordinates to physical coordinates ?
          let (width, height) = (*logical_size).into();
          let mut out = vec![input::System::Resized { width, height }.into()];
          self.dimensions =
            view::coordinates::dimensions::Pixel::new_wh (width, height);
          if self.pointer_hv == (-1.0, -1.0) {
            self.pointer_hv.0 = width  as f32 / 2.0;
            self.pointer_hv.1 = height as f32 / 2.0;
            out.push (self.pointer().into());
          } else {
            self.clip_pointer().map (|pointer| out.push (pointer.into()));
          }
          return Ok (out)
        }
        // "input method event" see winit documentation
        event::WindowEvent::Ime (_) =>
          input::System::InputMethod (/*TODO*/).into(),
        event::WindowEvent::Occluded (occluded) =>
          input::System::Occluded (*occluded).into(),
        event::WindowEvent::Moved (_) => return Err (winit_event),
        _ => {
          log::error!("TODO: unhandled glutin window event: {:?}", event);
          unimplemented!("TODO: unhandled glutin window event: {:?}", event)
        }
      }
      Event::DeviceEvent { event, .. } => match event {
        // TODO: joystick/gamepad buttons? note this also returns mouse buttons
        event::DeviceEvent::Button {..} => return Err (winit_event),
        event::DeviceEvent::Key (_)     => return Err (winit_event),
        // TODO: always round away from zero ?
        event::DeviceEvent::Motion { axis, value } =>
          input::Axis { axis: *axis, value: value.round() as i32 }.into(),
        // TODO: always round away from zero ?
        event::DeviceEvent::MouseMotion { delta: (x, y) } => {
          let motion = input::Motion (x.round() as i32, y.round() as i32).into();
          let pointer = {
            self.pointer_hv.0 += (x.abs().powf (self.pointer_sensitivity as f64)
              * x.signum()) as f32;
            self.pointer_hv.1 -= (y.abs().powf (self.pointer_sensitivity as f64)
              * y.signum()) as f32;
            self.clip_pointer().unwrap_or_else (|| self.pointer()).into()
          };
          return Ok (vec![motion, pointer])
        }
        event::DeviceEvent::MouseWheel {..} => return Err (winit_event),
        event::DeviceEvent::Added {..} => return Err (winit_event),
        _ => {
          log::error!("TODO: unhandled glutin device event: {:?}", event);
          unimplemented!("TODO: unhandled glutin device event: {:?}", event)
        }
      }
      // NOTE: resume is spammed so we are not handling it for now
      Event::Resumed     => return Err (winit_event),
      Event::AboutToWait => return Err (winit_event),
      _ => {
        log::error!("TODO: unhandled winit event: {:?}", winit_event);
        unimplemented!("TODO: unhandld winit event: {:?}", winit_event)
      }
    };
    Ok (vec![input])
  }

  fn pointer (&self) -> view::input::Pointer {
    view::input::Pointer {
      position_horizontal: self.pointer_hv.0,
      position_vertical:   self.pointer_hv.1,
      modifiers:           self.modifiers.into()
    }
  }

  /// Return a pointer input event if position was clipped
  fn clip_pointer (&mut self) -> Option <view::input::Pointer> {
    let hv_before = self.pointer_hv;
    self.pointer_hv.0 = f32::min (self.pointer_hv.0, self.dimensions.width()
      as f32);
    self.pointer_hv.0 = f32::max (self.pointer_hv.0, 0.0);
    self.pointer_hv.1 = f32::min (self.pointer_hv.1, self.dimensions.height()
      as f32);
    self.pointer_hv.1 = f32::max (self.pointer_hv.1, 0.0);
    if hv_before != self.pointer_hv {
      Some (self.pointer())
    } else {
      None
    }
  }
}

impl <'a> winit::application::ApplicationHandler for InputHandler <'a> {
  // required
  fn resumed (&mut self, _event_loop : &winit::event_loop::ActiveEventLoop) {
    self.handle_event (winit::event::Event::Resumed)
  }
  fn window_event (&mut self,
    _event_loop : &winit::event_loop::ActiveEventLoop,
    window_id   : winit::window::WindowId,
    event       : winit::event::WindowEvent
  ) {
    self.handle_event (winit::event::Event::WindowEvent { window_id, event })
  }
  // optional
  fn new_events (&mut self,
    _event_loop : &winit::event_loop::ActiveEventLoop,
    cause       : winit::event::StartCause
  ) {
    self.handle_event (winit::event::Event::NewEvents (cause))
  }
  fn user_event (&mut self,
    _event_loop : &winit::event_loop::ActiveEventLoop,
    event       : ()
  ) {
    self.handle_event (winit::event::Event::UserEvent (event))
  }
  fn device_event (&mut self,
    _event_loop : &winit::event_loop::ActiveEventLoop,
    device_id   : winit::event::DeviceId,
    event       : winit::event::DeviceEvent
  ) {
    self.handle_event (winit::event::Event::DeviceEvent { device_id, event })
  }
  fn about_to_wait (&mut self,
    _event_loop : &winit::event_loop::ActiveEventLoop
  ) {
    self.handle_event (winit::event::Event::AboutToWait)
  }
  fn suspended (&mut self,
    _event_loop : &winit::event_loop::ActiveEventLoop
  ) {
    self.handle_event (winit::event::Event::Suspended)
  }
  fn exiting (&mut self, _event_loop : &winit::event_loop::ActiveEventLoop) {
    self.handle_event (winit::event::Event::LoopExiting)
  }
  fn memory_warning (&mut self,
    _event_loop : &winit::event_loop::ActiveEventLoop
  ) {
    self.handle_event (winit::event::Event::MemoryWarning)
  }
}

impl From <winit::event::MouseButton> for view::input::button::Mouse {
  fn from (button : winit::event::MouseButton) -> view::input::button::Mouse {
    use winit::event::MouseButton;
    use view::input::button::Mouse;
    match button {
      MouseButton::Left      => Mouse::Mouse1,
      MouseButton::Right     => Mouse::Mouse2,
      MouseButton::Middle    => Mouse::Mouse3,
      MouseButton::Other (4) => Mouse::Mouse4,
      MouseButton::Other (5) => Mouse::Mouse5,
      button => {
        log::error!("unhandled glutin mouse button: {:?}", button);
        unimplemented!()
      }
    }
  }
}

impl From <(&winit::event::KeyEvent, winit::keyboard::ModifiersState)>
  for view::Input
{
  fn from (
    (input, modifiers) :
    (&winit::event::KeyEvent, winit::keyboard::ModifiersState)
  ) -> view::Input {
    use std::str::FromStr;
    use view::{input, Input};
    use winit::keyboard;
    let state  = input.state.into();
    let button = {
      let variant = match &input.logical_key {
        keyboard::Key::Named (named_key) =>
          input::button::Keycode::from ((*named_key, input.location)),
        keyboard::Key::Character (ch) => input::button::Keycode::try_from (
          char::from_str (ch.as_str()).unwrap()
        ).unwrap(),
        //  input::button::Keycode::from_scancode (input.scancode),
        keyboard::Key::Unidentified (k) =>
          unimplemented!("TODO: unidentified key: {:?}", k),
        keyboard::Key::Dead (k) =>
          unimplemented!("TODO: dead key: {:?}", k)
      }.into();
      input::Button { variant, modifiers: modifiers.into() }
    };
    Input::Button (button, state)
  }
}

impl From <winit::event::ElementState> for view::input::button::State {
  fn from (state : winit::event::ElementState) -> Self {
    use winit::event::ElementState;
    use view::input::button::State;
    match state {
      ElementState::Pressed  => State::Pressed,
      ElementState::Released => State::Released
    }
  }
}

impl From <winit::keyboard::ModifiersState> for view::input::Modifiers {
  fn from (modifiers : winit::keyboard::ModifiersState) -> Self {
    use view::input::Modifiers;
    let mut out = Modifiers::empty();
    out.set (Modifiers::SHIFT, modifiers.shift_key());
    out.set (Modifiers::CTRL,  modifiers.control_key());
    out.set (Modifiers::ALT,   modifiers.alt_key());
    out.set (Modifiers::SUPER, modifiers.super_key());
    out
  }
}

/// Convert a back-end mouse+modifier+element state event into an interface
/// button input
fn mouse_button (
  button    : winit::event::MouseButton,
  modifiers : winit::keyboard::ModifiersState,
  state     : winit::event::ElementState
) -> view::Input {
  let state  = state.into();
  let button = {
    let variant   = view::input::button::Mouse::from (button).into();
    let modifiers = modifiers.into();
    view::input::Button { variant, modifiers }
  };
  view::Input::Button (button, state)
}

/// Convert a back-end mouse wheel delta+modifier event into an interface wheel
/// input
fn mouse_wheel (
  delta     : winit::event::MouseScrollDelta,
  modifiers : winit::keyboard::ModifiersState
) -> view::Input {
  let delta = match delta {
    winit::event::MouseScrollDelta::LineDelta (x, y)
      => (x.round() as i32, y.round() as i32),
    winit::event::MouseScrollDelta::PixelDelta (
      winit::dpi::PhysicalPosition { x, y }
    ) => (x.round() as i32, y.round() as i32)
  };
  let modifiers = modifiers.into();
  view::input::Wheel { delta, modifiers }.into()
}
