//! Remote Opengl backend: events are collected locally and rendering is done on
//! a remote thread.
//!
//! `Opengl::init` should be called first. It will wait until the
//! `Remote::make_interface` initialization process has placed the windowed
//! context and display receiver into a shared mutex so the `Render`
//! initialization can proceed. The `Remote::make_interface` initialization
//! process will then in turn wait for the `Opengl::init` process to finish.

use std::{sync, time};
use lazy_static::lazy_static;
use nsys::{self, gl, math};
use nsys::gl::glium::{self, glutin};
use nsys::gl::winit;
use key_vec::KeyVec;
use unbounded_spsc;

use crate::{Application, Interface, Tree};
use crate::interface::{self, view, View};
use crate::presentation::HasGraphics;
use crate::tree::{self, NodeId};
use super::{render, Graphics, InputHandler, InputState, Presentation};

lazy_static!{
  /// Used to share window with render thread
  static ref GLUTIN_WINDOW
    : sync::Mutex <Option <(winit::window::Window, glutin::config::Config)>>
    = sync::Mutex::new (None);
  /// Used to transfer view display events to the rendering backend
  static ref DISPLAY_RECEIVER
    : sync::Mutex <Option <
      unbounded_spsc::Receiver <Vec <(NodeId, view::Display)>>>>
    = sync::Mutex::new (None);
}

/// Remote rendering handle and glutin event loop
pub struct Remote {
  pub event_loop : winit::event_loop::EventLoop <()>,
  display        : unbounded_spsc::Sender <Vec <(NodeId, view::Display)>>,
  input_state    : InputState,
  screen_tiles   : Option <NodeId>
}

/// Rendering backend that can receive and handle display updates
pub struct Opengl {
  pub draw_crosshair : bool,
  pub inner       : gl::Render,
  tileset_id      : Option <gl::render::resource::default::DefaultTilesetId>,
  updates         : unbounded_spsc::Receiver <Vec <(NodeId, view::Display)>>,
  id_map          : KeyVec <NodeId, NodeId>,
  view            : Tree <interface::View>,
  glium_frame     : Option <glium::Frame>,
  /// Counts number of batches of display events have been processed
  display_counter : u64,
  /// Counts number of frames that have been rendered
  frame           : u64
}

/// Create a tile child frame of the root screen frame.
///
/// Must be called after `Opengl::init()`.
pub fn create_screen_tiles <A, P> (interface : &mut Interface <A, P>) where
  A : Application,
  P : Presentation + HasGraphics <Remote>
{
  use interface::{controller, model};
  use interface::widget::{self, BuildElement};
  let screen_id     = interface.root_id().clone();
  let screen_tiles  = widget::frame::free::Builder::<A>::new (
    interface.elements(), &screen_id
  ) .layout (controller::component::layout::Free {
      size: controller::Size::fill(),
      .. controller::component::layout::Free::default_tile()
    }.into())
    .coord_kind_override (view::coordinates::Kind::Tile)
    .build_element();
  let tiles_id =
    match interface.action (&screen_id, interface::Action::create_singleton (
      screen_tiles, interface::CreateOrder::Append)).next().unwrap()
    {
      (_, model::Event::Create (_, id, _)) => id,
      _ => unreachable!()
    };
  interface.presentation.graphics().screen_tiles = Some (tiles_id);
}

impl Default for Remote {
  fn default() -> Self {
    let event_loop = {
      let (event_loop, window, gl_config) =
        gl::init::glutin_window ("Gooey Opengl Remote Window");
      {
        let mut glutin_window_lock = GLUTIN_WINDOW.lock().unwrap();
        debug_assert!(glutin_window_lock.is_none());
        // hide & grab cursor
        // NOTE: as of winit 0.30.5 setting cursor visibility on other threads
        // seems to block indefinitely on windows, so we set it here before
        // sending to remote thread
        window.set_cursor_visible (false);
        // NOTE: grabbing cursor here on x11 results in error:
        // Misc("Cursor could not be confined: confine location not viewable")
        //window.set_cursor_grab (winit::window::CursorGrabMode::Confined)
        //  .unwrap();
        *glutin_window_lock = Some ((window, gl_config));
      }
      event_loop
    };
    let display = {
      let (sender, receiver) =
        unbounded_spsc::channel::<Vec <(NodeId, view::Display)>>();
      {
        let mut display_receiver_lock = DISPLAY_RECEIVER.lock().unwrap();
        debug_assert!(display_receiver_lock.is_none());
        *display_receiver_lock = Some (receiver);
      }
      sender
    };
    Remote {
      event_loop,
      display,
      input_state:  InputState::new(),
      screen_tiles: None
    }
  }
}

impl Remote {
  #[inline]
  pub fn screen_tiles_id (&self) -> &NodeId {
    self.screen_tiles.as_ref().unwrap()
  }
  #[inline]
  pub fn dimensions (&self) -> view::dimensions::Pixel {
    self.input_state.dimensions.clone()
  }
  pub fn pointer_sensitivity (&self) -> f32 {
    self.input_state.pointer_sensitivity
  }
  pub fn set_pointer_sensitivity (&mut self, pointer_sensitivity : f32) {
    self.input_state.pointer_sensitivity = pointer_sensitivity
  }
}

impl Graphics     for Remote { }
impl Presentation for Remote {
  /// &#9888; Must be called after Opengl::init.
  ///
  /// Expects a root Canvas component
  fn with_root (root : View, id : NodeId) -> Self {
    let remote = Self::default();
    log::debug!("opengl remote with root: {:?}", root);
    remote.display.send (vec![(
      id.clone(),
      view::Display::Create (root, id, interface::CreateOrder::Append)
    )]).unwrap();
    // wait for Opengl::init to finish: to do so we check if the
    // DISPLAY_RECEIVER has been taken
    loop {
      if (*DISPLAY_RECEIVER).lock().unwrap().is_none() {
        break
      }
      std::thread::sleep (std::time::Duration::from_millis (100));
    }
    remote
  }

  /// &#9888; Must be called after Opengl::init
  ///
  /// Creates a root screen canvas with default dimensions (1x1).
  fn make_interface <A : Application> () -> Interface <A, Self> {
    use interface::controller;
    use interface::view::component::{Canvas, Kind};
    use interface::widget::{self, BuildElement};
    let screen = {
      // create a root screen frame and rely on the resize control function to
      // update the dimensions
      let mut screen = widget::frame::screen::PixelBuilder::<A>::new()
        .anchor (controller::Alignment::pixel())
        .build_element();
      // NOTE: viewport width/height must be non-zero
      let canvas = Canvas::try_ref_mut (&mut screen.view.component).unwrap();
      canvas.coordinates.modify_dimensions_horizontal (1);
      canvas.coordinates.modify_dimensions_vertical (1);
      screen
    };
    Interface::<A, Remote>::with_root (screen)
  }

  fn get_input (&mut self, input_buffer : &mut Vec <view::Input>) {
    use winit::platform::pump_events::EventLoopExtPumpEvents;
    log::trace!("get input...");
    // TODO: we may want to ignore some extra glutin events, e.g. window axis
    // motion events or keyboard device events on linux
    self.event_loop.pump_app_events (
      Some (time::Duration::ZERO),
      &mut InputHandler { input_buffer, input_state: &mut self.input_state });
    log::trace!("...get input");
  }

  fn display_view <V : AsRef <View>> (&mut self,
    _view_tree      : &Tree <V>,
    display_values : std::vec::Drain <(NodeId, view::Display)>
  ) {
    self.display.send (display_values.collect()).unwrap();
  }
}

impl Opengl {
  /// Get the windowed context and display receiver and create the Opengl
  /// backend. This function will not return until the Remote struct has been
  /// created.
  pub fn init (
    tileset_id : Option <gl::render::resource::default::DefaultTilesetId>
  ) -> Self {
    let inner = {
      use gl::render::resource::default::MAIN_VIEWPORT;
      // wait for glutin window to be set
      let (window, gl_config) = loop {
        if let Some (glutin_window) = (*GLUTIN_WINDOW).lock().unwrap().take() {
          break glutin_window
        }
        std::thread::sleep (std::time::Duration::from_millis (100));
      };
      // NOTE: as of winit 0.30.5 setting cursor visibility/grab on other
      // threads seems to block indefinitely on windows, so we set it on main
      // before receiving here
      //window.set_cursor_visible (false);
      //window.set_cursor_grab (winit::window::CursorGrabMode::Confined).unwrap();
      let display = gl::init::glium_display_gl33core (&window, &gl_config);
      let mut render =
        gl::Render::<gl::render::resource::Default>::new (display, window);
      // set the tile dimensions
      let [tile_width, tile_height] =
        render.resource.tile_dimensions (tileset_id.unwrap_or_default());
      std::env::set_var ("GOOEY_TILE_WIDTH", tile_width.to_string());
      std::env::set_var ("GOOEY_TILE_HEIGHT", tile_height.to_string());
      // set the main viewport
      render.resource.draw2d
        .viewport_resources_set (MAIN_VIEWPORT, Default::default());
      render
    };
    let updates = loop {
      if let Some (receiver) = (*DISPLAY_RECEIVER).lock().unwrap().take() {
        break receiver
      }
      std::thread::sleep (std::time::Duration::from_millis (100));
    };
    Opengl {
      inner,
      tileset_id,
      updates,
      id_map:          KeyVec::new(),
      view:            Tree::new(),
      glium_frame:     None,
      display_counter: 0,
      frame:           0,
      draw_crosshair:  false
    }
  }

  #[inline]
  pub fn inner (&self) -> &gl::Render {
    &self.inner
  }
  #[inline]
  pub fn inner_mut (&mut self) -> &mut gl::Render {
    &mut self.inner
  }
  #[inline]
  pub fn get_display_counter (&self) -> u64 {
    self.display_counter
  }
  #[inline]
  pub fn reset_display_counter (&mut self) {
    self.display_counter = 0;
  }
  pub fn load_pointer (&mut self,
    key    : gl::render::resource::default::PointerTextureIndexRepr,
    bytes  : &[u8],
    offset : math::Vector2 <i16>
  ) {
    let render = &mut self.inner;
    render::load_pointer (render, key, bytes, offset);
  }
  pub fn process_display_events (&mut self) {
    while let Ok (updates) = self.updates.try_recv() {
      for (node_id, display) in updates {
        match display {
          view::Display::Create (view, child_id, order) => {
            let new_id = if self.id_map.is_empty() {
              // special case for initial root node: parent_id == child_id
              self.view.insert (tree::Node::new (view),
                tree::InsertBehavior::AsRoot).unwrap()
            } else {
              let parent_id = self.id_map.get (&node_id).unwrap();
              let new_id    = self.view.insert (tree::Node::new (view),
                tree::InsertBehavior::UnderNode (&parent_id)).unwrap();
              match order {
                interface::CreateOrder::Prepend => {
                  let _ = self.view.make_first_sibling (&new_id).unwrap();
                }
                interface::CreateOrder::NthSibling (n) => {
                  let _ = self.view.make_nth_sibling (&new_id, n as usize)
                    .unwrap();
                }
                interface::CreateOrder::Append => {}
              }
              new_id
            };
            assert!(self.id_map.insert (child_id, new_id).is_none());
          }
          view::Display::Update (update) => {
            let id = self.id_map.get (&node_id).unwrap();
            match update {
              view::Update::View (view) =>
                *self.view.get_mut (id).unwrap().data_mut() = view,
              view::Update::FocusTop => {
                self.view.make_last_sibling (&id).unwrap();
              }
            }
          }
          view::Display::Destroy => {
            let id = self.id_map.remove (&node_id).unwrap();
            let _ = self.view
              .remove_node (id, tree::RemoveBehavior::DropChildren).unwrap();
          }
        }
      }
      self.display_counter += 1;
    }
  }
  pub fn update_and_do_frame (&mut self) {
    // update the renderer with the current view
    render::update (&mut self.inner, self.tileset_id, &self.view,
      self.draw_crosshair);
    // render the frame
    self.inner.do_frame (self.glium_frame.as_mut());
    self.frame += 1;
  }
  /// Pump display events and render a frame
  pub fn display (&mut self) {
    self.process_display_events();
    self.update_and_do_frame();
  }
}

/// Hack to allow passing a tree of views directly to the render::update
/// function
impl AsRef <View> for View {
  fn as_ref (&self) -> &View {
    self
  }
}
