use log;
use crate::Tree;
use crate::tree::NodeId;
use crate::interface::{view, View};
use super::{Graphics, Presentation};

/// A presentation implementation where display values are ignored.
///
/// Input is read in lines from stdin-- `control::Input::Text (line)` is the
/// only input event ever generated.
#[derive(Default)]
pub struct Headless { }
impl Graphics     for Headless { }
impl Presentation for Headless {
  fn with_root (_ : View, _ : NodeId) -> Self {
    Self::default()
  }
  /// Ignores display values and reads lines from stdin.
  ///
  /// Panics if input buffer is non-empty.
  fn get_input (&mut self,
    input_buffer : &mut Vec <view::Input>
  ) {
    log::trace!("update...");
    debug_assert!(input_buffer.is_empty());
    let mut line = String::new();
    let _ = std::io::stdin().read_line (&mut line).unwrap();
    input_buffer.push (view::input::Text::String (line).into());
    log::trace!("...update");
  }

  fn display_view <V : AsRef <View>> (&mut self,
    _view_tree      : &Tree <V>,
    _display_values : std::vec::Drain <(NodeId, view::Display)>
  ) { /* headless */ }
}
