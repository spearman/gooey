use std::cell::RefCell;
use serde_plain;
use smallvec::SmallVec;
use crate::interface::{view::input, widget};
use crate::tree::NodeId;

use super::{Id, Nil};

impl_control_builtin!{
  Button <Release> {
    ActivatePointer          => widget::frame::activate_pointer,
    ButtonPush               => widget::button::push,
    ButtonRelease            => widget::button::release,
    FieldBackspace           => widget::field::backspace,
    FormSubmitCallback       => widget::form::submit_callback,
    FrameClose               => widget::frame::close,
    FrameRefresh             => widget::frame::refresh,
    FrameToggleOrientation   => widget::frame::toggle_orientation,
    FrameFreeGrowWidth       => widget::frame::free::grow_width,
    FrameFreeGrowHeight      => widget::frame::free::grow_height,
    FrameFreeMoveDown        => widget::frame::free::move_down,
    FrameFreeMoveLeft        => widget::frame::free::move_left,
    FrameFreeMoveRight       => widget::frame::free::move_right,
    FrameFreeMoveUp          => widget::frame::free::move_up,
    FrameFreeShrinkHeight    => widget::frame::free::shrink_height,
    FrameFreeShrinkWidth     => widget::frame::free::shrink_width,
    FrameTiledDecreaseSize   => widget::frame::tiled::decrease_size,
    FrameTiledIncreaseSize   => widget::frame::tiled::increase_size,
    MenuNextItem             => widget::menu::next_item,
    MenuPreviousItem         => widget::menu::previous_item,
    MenuFirstItem            => widget::menu::first_item,
    MenuLastItem             => widget::menu::last_item,
    NumboxDecrease           => widget::numbox::decrease,
    NumboxIncrease           => widget::numbox::increase,
    NumboxDecrease10x        => widget::numbox::decrease_10x,
    NumboxIncrease10x        => widget::numbox::increase_10x,
    PlaybackLoop             => widget::playback::loop_,
    PlaybackPlay             => widget::playback::play,
    PlaybackReady            => widget::playback::ready,
    TextboxScrollRight       => widget::textbox::scroll_right,
    TextboxScrollLeft        => widget::textbox::scroll_left,
    TextboxScrollUp          => widget::textbox::scroll_up,
    TextboxScrollDown        => widget::textbox::scroll_down,
    TextboxPageRight         => widget::textbox::page_right,
    TextboxPageLeft          => widget::textbox::page_left,
    TextboxPageUp            => widget::textbox::page_up,
    TextboxPageDown          => widget::textbox::page_down
  }
}

impl_control_serdes!{ Button }

/// Argument type for button callbacks
pub type Release = Option <RefCell <SmallVec <[(Button, NodeId); 1]>>>;

/// Binds a button control to an input button
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Binding <E : Id <Button> = Nil> (pub Button, pub input::Button,
  pub std::marker::PhantomData <E>);

impl <E : Id <Button>> From <Binding <E>> for (input::Button, Button) {
  fn from (Binding (control, input, _) : Binding <E>) -> Self {
    (input, control)
  }
}

impl <E : Id <Button>> From <(input::Button, Button)> for Binding <E> {
  fn from ((input, control) : (input::Button, Button)) -> Self {
    Binding (control, input, Default::default())
  }
}

//
// private
//

struct BindingVisitor <E : Id <Button> = Nil> (
  std::marker::PhantomData <E>);

impl <E : Id <Button>> serde::Serialize for Binding <E> {
  fn serialize <S : serde::Serializer> (&self, serializer : S)
    -> std::result::Result <S::Ok, S::Error>
  {
    use std::convert::TryFrom;
    let control = if let Ok (builtin) = Builtin::try_from (self.0.clone()) {
      serde_plain::to_string (&builtin).unwrap()
    } else if let Ok (application) = E::try_from (self.0.clone()) {
      serde_plain::to_string (&application).unwrap()
    } else {
      unreachable!()
    };
    let s = format!("{}: {}{}",
      control, self.1.variant, self.1.modifiers.to_string());
    serializer.serialize_str(&s)
  }
}

impl <'de, E : Id <Button>> serde::de::Visitor <'de> for BindingVisitor <E> {
  type Value = Binding <E>;
  fn expecting (&self, formatter : &mut std::fmt::Formatter)
    -> std::fmt::Result
  {
    formatter.write_str ("A control: button pair")
  }
  fn visit_str <ERR : serde::de::Error> (self, value : &str)
    -> std::result::Result <Self::Value, ERR>
  {
    let control : Button = {
      let s = value.split (':').next().unwrap();
      if let Ok (builtin) = serde_plain::from_str::<Builtin> (s) {
        builtin.into()
      } else {
         serde_plain::from_str::<E> (s).unwrap().into()
      }
    };
    let button = {
      use std::str::FromStr;
      let mut iter = value.split (' ').skip (1).next().unwrap().split ('+');
      let s        = iter.next().unwrap();
      let variant  = input::button::Variant::from_str (&s).map_err (|err|{
        log::error!(err:?, string=s; "error parsing button");
        panic!("error parsing button string \"{s}\": {err}")
      }).unwrap();
      let mut modifiers = input::Modifiers::empty();
      for modifier in iter {
        match modifier.to_lowercase().as_str() {
          "shift" => modifiers |= input::Modifiers::SHIFT,
          "ctrl"  => modifiers |= input::Modifiers::CTRL,
          "alt"   => modifiers |= input::Modifiers::ALT,
          "super" => modifiers |= input::Modifiers::SUPER,
          "any"   => modifiers |= input::Modifiers::ANY,
          _ => return Err (
            ERR::custom (format!("Unrecognized modifier: {:?}", modifier)))
        }
      }
      input::Button { variant, modifiers }
    };
    Ok (Binding (control, button, Default::default()))
  }
}

impl <'de, E : Id <Button>> serde::Deserialize <'de> for Binding <E> {
  fn deserialize <D : serde::Deserializer <'de>> (deserializer : D)
    -> std::result::Result <Self, D::Error>
  {
    deserializer.deserialize_str (BindingVisitor (Default::default()))
  }
}
