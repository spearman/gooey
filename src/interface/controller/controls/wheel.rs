use crate::interface::view::input;
impl_control_builtin!{
  Wheel <input::Wheel> { }
}

impl_control_serdes!{ Wheel }
