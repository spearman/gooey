use crate::interface::widget;
use crate::interface::view::input;
impl_control_builtin!{
  System <input::System> {
    FrameScreenResize => widget::frame::screen::resize
  }
}

impl_control_serdes!{ System }
