use crate::interface::view::input;

impl_control_builtin!{
  Motion <input::Motion> { }
}

impl_control_serdes!{ Motion }
