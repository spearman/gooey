use crate::interface::widget;
use crate::interface::view::input;

impl_control_builtin!{
  Pointer <input::Pointer> {
    HotPointer => widget::frame::hot_pointer
  }
}

impl_control_serdes!{ Pointer }
