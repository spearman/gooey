use crate::interface::widget;
use crate::interface::view::input;

impl_control_builtin!{
  Text <input::Text> {
    FieldLineEntry => widget::field::line_entry
  }
}

impl_control_serdes!{ Text }
