use derive_more::{From, TryInto};

use crate::math::Normalized;
use crate::math::num_traits::One;

/// Absolute or relative width and height
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Size {
  pub width  : Unsigned,
  pub height : Unsigned
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, From, TryInto)]
pub enum Unsigned {
  Absolute (u32),
  Relative (Normalized <f32>)
}

impl Size {
  pub fn default_absolute() -> Self {
    Size {
      width:  0.into(),
      height: 0.into()
    }
  }
  pub fn default_relative() -> Self {
    Size {
      width:  Normalized::<f32>::zero().into(),
      height: Normalized::<f32>::zero().into()
    }
  }
  /// Relative size with 100% width and height
  pub fn fill() -> Self {
    Size {
      width:  Unsigned::fill(),
      height: Unsigned::fill()
    }
  }
}

impl Unsigned {
  pub fn fill() -> Self {
    Normalized::<f32>::one().into()
  }
  pub fn half (self) -> Self {
    match self {
      Unsigned::Absolute (x) => Unsigned::Absolute (x / 2),
      Unsigned::Relative (x) => Unsigned::Relative (x * Normalized::clamp (0.5))
    }
  }
}
