#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Alignment {
  pub horizontal : Horizontal,
  pub vertical   : Vertical
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Horizontal {
  Left,
  Center,
  Right
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Vertical {
  Top,
  Middle,
  Bottom
}

impl Alignment {
  /// Origin for row/column coordinates (default)
  #[inline]
  pub const fn tile() -> Self {
    Alignment {
      horizontal: Horizontal::Left,
      vertical:   Vertical::Top
    }
  }
  /// Origin for width/height coordinates
  #[inline]
  pub const fn pixel() -> Self {
    Alignment {
      horizontal: Horizontal::Left,
      vertical:   Vertical::Bottom
    }
  }
  /// Centered vertically and horizontally
  #[inline]
  pub const fn middle_center() -> Self {
    Alignment {
      horizontal: Horizontal::Center,
      vertical:   Vertical::Middle
    }
  }

  #[inline]
  pub const fn new (horizontal : Horizontal, vertical : Vertical) -> Self {
    Alignment { horizontal, vertical }
  }
}

impl Default for Alignment {
  fn default() -> Self {
    Self::tile()
  }
}

impl Horizontal {
  #[inline]
  pub fn right_sign (&self) -> i32 {
    match self {
      Horizontal::Left | Horizontal::Center =>  1,
      Horizontal::Right                     => -1
    }
  }
  #[inline]
  pub fn left_sign (&self) -> i32 {
    match self {
      Horizontal::Left | Horizontal::Center => -1,
      Horizontal::Right                     =>  1
    }
  }
}
impl Vertical {
  #[inline]
  pub fn up_sign (&self) -> i32 {
    match self {
      Vertical::Bottom | Vertical::Middle =>  1,
      Vertical::Top                       => -1
    }
  }
  #[inline]
  pub fn down_sign (&self) -> i32 {
    match self {
      Vertical::Bottom | Vertical::Middle => -1,
      Vertical::Top                       =>  1
    }
  }
}
