//! Interaction components

use std::cell::RefCell;
use std::convert::TryFrom;
use std::iter::FromIterator;
use log;
use bitflags::bitflags;
use key_vec::KeyVec;
use smallvec::SmallVec;
use strum::EnumCount;

use crate::{Application, Tree, TreeHelper};
use crate::tree::NodeId;
use crate::interface::{Action, Element, View, Widget, widget};
use crate::interface::view::{self, color, input, Input};

// control trait and types
pub mod controls;
pub use self::controls::Controls;
// serializable bindings and builder
pub mod bindings;
pub use self::bindings::Bindings;
// controller components
pub mod component;
pub use self::component::Component;
// data types
pub mod alignment;
pub mod offset;
pub mod size;
pub use self::alignment::Alignment;
pub use self::offset::Offset;
pub use self::size::Size;

/// A component that holds control bindings and interaction state.
///
/// Controllers handle the translation of `view::Input` events to interface
/// `Action`s via `Control` bindings.
// TODO: builder pattern? because input_map is private, we can't use .. syntax
#[derive(Clone, Debug, Default)]
pub struct Controller {
  pub component   : Component,
  /// Defines behavior and appearance
  pub state       : State,
  /// View appearance selection for each state
  pub appearances : Appearances,
  /// Controls whether the node will be moved to the last sibling position
  /// ("top") when processed by a `Focus` action (either as the target node or
  /// an ancestor of the target node).
  ///
  /// Defaults to `false`. Note that some widget builders may set this to true
  /// unless overridden (e.g. free frame widgets).
  pub focus_top   : bool,
  /// Controls whether unhandled input is bubbled up to parent: when true
  /// unhandled input will be trapped (not bubbled), when false unhandled input
  /// will be passed to the parent node.
  ///
  /// Defaults to `InputMask::empty()`
  pub bubble_trap : InputMask,
  /// Defines mappings from inputs to controls
  input_map       : InputMap
}

/// Determines appearance and behavior
#[derive(Clone, Debug, Eq, PartialEq, EnumCount)]
pub enum State {
  Enabled,
  Focused,
  Disabled
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Area {
  /// Specifies the area inside any border (default)
  Interior,
  /// Specifies the total area including any border
  Exterior
}

/// Flow of child content
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Orientation {
  Horizontal,
  Vertical
}

/// Selection of an appearance for each state
#[derive(Clone, Debug, Default, PartialEq)]
pub struct Appearances (pub [view::Appearance; State::COUNT]);
#[derive(Default)]
pub struct AppearancesBuilder ([view::Appearance; State::COUNT]);

bitflags! {
  #[derive(Clone, Copy, Debug, Default, Eq, Ord, PartialEq, PartialOrd)]
  pub struct InputMask : u8 {
    const AXIS    = 0b00000001;
    const BUTTON  = 0b00000010;
    const MOTION  = 0b00000100;
    const POINTER = 0b00001000;
    const SYSTEM  = 0b00010000;
    const TEXT    = 0b00100000;
    const WHEEL   = 0b01000000;
  }
}

/// Concrete control bindings for all input types.
///
/// Given an input event, if a corresponding control is not contained in this
/// struct, the input will either be trapped (discarded) or else passed up to
/// the parent node (depending on the state of the Controller `bubble_trap`
/// flag).
#[derive(Clone, Debug, Default, Eq, PartialEq)]
struct InputMap {
  pub button_any      : Option <controls::Button>,
  pub buttons         : KeyVec <input::Button, controls::Button>,
  pub release_buttons : KeyVec <input::Button, controls::Button>,
  pub axes            : KeyVec <u32, controls::Axis>,
  pub motion          : Option <controls::Motion>,
  pub pointer         : Option <controls::Pointer>,
  pub system          : Option <controls::System>,
  pub text            : Option <controls::Text>,
  pub wheel           : Option <controls::Wheel>
}

/// An input result for registering or removing button release controls.
///
/// This is optionally returned by `controller.handle_input()` when a button
/// release control needs to be registered or removed from the focused node.
#[derive(Debug)]
pub (crate) enum ButtonRelease {
  Insert (input::Button, SmallVec <[(controls::Button, NodeId); 1]>),
  Remove (input::Button, NodeId)
}

/// Allows focus directed to a parent frame to be redirected to a different
/// node:
///
/// - Fields
/// - Menus: if the first child of the focused Frame is a Menu (Selection)
///   node, focus will be redirected to the appropriate item node
pub (crate) fn refocus (node_id : &NodeId, elements : &Tree <Element>)
  -> Option <NodeId>
{
  let node    = elements.get (node_id).unwrap();
  let element = node.data();
  if element.controller.state != State::Focused {
    log::warn!("refocus state not focused: {:?}", element.controller.state);
    debug_assert!(false);
  }
  let mut refocus_id = None;
  // frame
  if let Ok (_) = widget::Frame::try_from (element) {
    let mut children_ids = node.children().iter();
    // first child
    if let Some (child_id) = children_ids.next() {
      let child = elements.get_element (child_id);
      if let Ok (_) = widget::Field::try_from (child) {
        refocus_id = Some (child_id.clone());
      } else if let Ok (_) = widget::Numbox::try_from (child) {
        refocus_id = Some (child_id.clone());
      } else if let Ok (Widget (selection, _, _)) =
        widget::Menu::try_from (child)
      {
        // refocus menu selection
        refocus_id = selection.current.clone()
          .or_else (|| widget::menu::find_first_item (elements, children_ids));
      }
    }
  }
  refocus_id
}

impl Controller {
  #[inline]
  pub fn with_bindings <A : Application> (bindings : &Bindings <A>) -> Self {
    Controller { input_map: bindings.into(), .. Controller::default() }
  }

  #[inline]
  pub fn get_appearance (&self) -> &view::Appearance {
    &self.appearances.get (self.state.clone())
  }

  #[inline]
  pub fn get_bindings <A : Application> (&self) -> Bindings <A> {
    self.input_map.to_bindings()
  }

  /// Replaces all existing bindings
  #[inline]
  pub fn set_bindings <A : Application> (&mut self, bindings : &Bindings <A>) {
    self.clear_bindings();
    self.add_bindings (bindings);
  }

  /// Must be new bindings or else panics
  #[inline]
  pub fn add_bindings <A : Application> (&mut self, bindings : &Bindings <A>) {
    self.input_map.add_bindings (bindings)
  }

  /// Replaces existing bindings; should not fail
  #[inline]
  pub fn insert_bindings <A : Application> (&mut self, bindings : &Bindings <A>) {
    self.input_map.insert_bindings (bindings)
  }

  /// Remove matching controls
  #[inline]
  pub fn remove_bindings (&mut self, controls : &Controls) {
    self.input_map.remove_bindings (controls)
  }

  #[inline]
  pub fn clear_buttons (&mut self) {
    self.input_map.buttons.clear()
  }

  #[inline]
  pub fn remove_any_button (&mut self) {
    self.input_map.button_any = None
  }

  #[inline]
  pub fn remove_system (&mut self) {
    self.input_map.system = None
  }

  #[inline]
  pub fn remove_text (&mut self) {
    self.input_map.text = None
  }

  #[inline]
  pub fn remove_motion (&mut self) {
    self.input_map.motion = None
  }

  #[inline]
  pub fn remove_pointer (&mut self) {
    self.input_map.pointer = None
  }

  #[inline]
  pub fn clear_bindings (&mut self) {
    self.input_map.clear()
  }

  pub (crate) fn handle_input <A : Application> (&self,
    input         : Input,
    elements      : &Tree <Element>,
    node_id       : &NodeId,
    action_buffer : &mut Vec <(NodeId, Action)>
  ) -> Result <Option <ButtonRelease>, Input> {
    use controls::Control;
    log::trace!("handle_input...");
    let mut button_release = None;
    match input {
      Input::Button (ref button, state) => {
        match self.component {
          Component::Cursor (ref cursor) => match button.variant {
            input::button::Variant::Keycode (keycode) => {
              if !button.modifiers.intersects (
                input::Modifiers::ALT | input::Modifiers::CTRL |
                input::Modifiers::SUPER
              ) {
                if cursor.ignore.contains (&keycode) {
                  // if the cursor is configured to ignore this keycode, allow
                  // the input to bubble
                  return Err (input)
                } else if keycode.is_printable() {
                  // if the keycode is printible, consume the input and the
                  // cursor will handle the text input
                  return Ok (None)
                }
              }
            }
            _ => {}
          }
          _ => {}
        }
        match state {
          input::button::State::Pressed => {
            // if the button matched was an any key or has Modifiers::ANY set,
            // we need to set the Modifier::ANY flag if a release control is
            // bound so that it will still be matched if modifiers change
            let mut any = false;
            // in backends that don't report key repeat events (Winit), if there
            // is a release event bound to the button then assumes the press was
            // a repeat and ignore
            if self.input_map.release_buttons
              .binary_search_by_key (&button, |(b, _)| b).is_ok()
            {
              return Ok (None)
            }
            let control = if let Some (control) = self.input_map.button_any
              .as_ref()
            {
              any = true;
              Some (control.clone())
            } else if let Ok (index) = self.input_map.buttons
              .binary_search_by_key (&button, |(b, _)| b)
            {
              let (b, control) = &self.input_map.buttons[index];
              if b.modifiers.contains (input::Modifiers::ANY) {
                any = true;
              }
              Some (control.clone())
            } else {
              None
            };
            if let Some (control) = control {
              let release = Some (RefCell::new (SmallVec::new()));
              control.fun::<A::ButtonControls>().0
                (&release, elements, node_id, action_buffer);
              let controls = release.unwrap().into_inner();
              if !controls.is_empty() {
                let mut button = button.clone();
                if any {
                  button.modifiers.set (input::Modifiers::ANY, true);
                }
                button_release =
                  Some (ButtonRelease::Insert (button, controls));
              }
            }
          }
          input::button::State::Released => {
            let control = if let Ok (index) = self.input_map.release_buttons
              .binary_search_by_key (&button, |(b, _)| b)
            {
              let (_, control) = &self.input_map.release_buttons[index];
              Some (control.clone())
            } else {
              None
            };
            if let Some (control) = control {
              control.fun::<A::ButtonControls>().0
                (&None, elements, node_id, action_buffer);
              button_release =
                Some (ButtonRelease::Remove (button.clone(), node_id.clone()));
            }
          }
        }
      }
      Input::Axis ( ref axis) => {
        if let Ok (index) = self.input_map.axes
          .binary_search_by_key (&axis.axis, |(a, _)| *a)
        {
          let (_, control) = &self.input_map.axes[index];
          control.clone().fun::<A::AxisControls>().0
            (&axis.value, elements, node_id, action_buffer)
        }
      }
      Input::Motion ( ref motion) => {
        if let Some (control) = self.input_map.motion.as_ref() {
          control.clone().fun::<A::MotionControls>().0
            (motion, elements, node_id, action_buffer)
        }
      }
      Input::Pointer (ref pointer) => {
        if let Some (control) = self.input_map.pointer.as_ref() {
          control.clone().fun::<A::PointerControls>().0
            (pointer, elements, node_id, action_buffer)
        }
      }
      Input::System (ref system) => {
        if let Some (control) = self.input_map.system.as_ref() {
          control.clone().fun::<A::SystemControls>().0
            (system, elements, node_id, action_buffer)
        }
      }
      Input::Text (ref text) => {
        if let Some (control) = self.input_map.text.as_ref() {
          control.clone().fun::<A::TextControls>().0
            (text, elements, node_id, action_buffer)
        }
      }
      Input::Wheel (ref wheel) => {
        if let Some (control) = self.input_map.wheel.as_ref() {
          control.clone().fun::<A::WheelControls>().0
            (wheel, elements, node_id, action_buffer)
        }
      }
    }
    log::trace!("...handle_input");
    if action_buffer.is_empty() && button_release.is_none() {
      Err (input)
    } else {
      Ok (button_release)
    }
  }

  pub (crate) fn release_buttons (&mut self) -> Vec <controls::Button> {
    self.input_map.release_buttons.drain (..).map (|(_, control)| control)
      .collect()
  }

  pub (crate) fn release_button_insert (&mut self,
    input : input::Button, control : controls::Button
  ) {
    if self.input_map.release_buttons.insert (input, control.clone()).is_some() {
      log::debug!("button release control already exists: {:?}",
        (input, control));
    }
  }

  pub (crate) fn release_button_remove (&mut self, input : input::Button) {
    match self.input_map.release_buttons
      .binary_search_by_key (&&input, |(b, _)| b)
    {
      Ok  (index) => {
        let _ = self.input_map.release_buttons.remove_index (index);
      }
      Err (_) => {
        log::warn!("remove release button not present: {:?}", input);
        debug_assert!(false);
      }
    }
  }

  pub (crate) fn update_view_focus (&self, view : &mut View) {
    view.appearance = self.get_appearance().clone();
    match &mut view.component {
      view::Component::Body (ref mut body) => match self.component {
        Component::Cursor (ref cursor) => match self.state {
          State::Focused  => {
            let caret = char::try_from (cursor.caret).unwrap();
            body.0.push (caret);
          }
          State::Enabled  => {
            let _ = body.0.pop().unwrap();
          }
          State::Disabled => {}
        }
        _ => {}
      }
      _ => {}
    }
  }
}

impl From <Component> for Controller {
  fn from (component : Component) -> Self {
    Controller { component, .. Controller::default() }
  }
}

impl From<&Input> for InputMask {
  fn from (input : &Input) -> Self {
    match input {
      Input::Axis   (_)    => InputMask::AXIS,
      Input::Button (_, _) => InputMask::BUTTON,
      Input::Motion (_)    => InputMask::MOTION,
      Input::Pointer(_)    => InputMask::POINTER,
      Input::System (_)    => InputMask::SYSTEM,
      Input::Text   (_)    => InputMask::TEXT,
      Input::Wheel  (_)    => InputMask::WHEEL
    }
  }
}

impl InputMap {
  pub fn to_bindings <A : Application> (&self) -> Bindings <A> {
    let buttons    = self.buttons.iter().cloned().map (Into::into).collect();
    let any_button = self.button_any.clone().map (Into::into);
    let system     = self.system.clone().map (Into::into);
    let text       = self.text.clone().map (Into::into);
    let motion     = self.motion.clone().map (Into::into);
    let pointer    = self.pointer.clone().map (Into::into);
    Bindings { buttons, any_button, system, text, motion, pointer }
  }

  /// Add new Bindings to the InputMap.
  ///
  /// Panics if there is a conflict with current:
  ///
  /// ```should_panic
  /// use gooey::application;
  /// use gooey::interface::controller::{bindings, controls, Controller};
  /// use gooey::interface::view::input;
  /// let bindings = bindings::Builder::<application::Default>::new()
  ///   .buttons (vec![
  ///     controls::button::Binding (
  ///       controls::button::Builtin::FormSubmitCallback.into(),
  ///       input::button::Keycode::Enter.into(),
  ///       Default::default()
  ///     )
  ///   ])
  ///   .build();
  /// let mut controller = Controller::with_bindings(&bindings);
  /// controller.add_bindings (&bindings);  // panic! duplicate bindings
  /// ```
  pub fn add_bindings <A : Application> (&mut self, bindings : &Bindings <A>) {
    let Bindings { buttons, any_button, system, text, motion, pointer } =
      bindings;
    // buttons
    let buttons_len  = self.buttons.len();
    let bindings_len = buttons.len();
    self.buttons.extend (buttons.iter().cloned().map (Into::into));
    assert_eq!(self.buttons.len(), buttons_len + bindings_len);
    // any button
    any_button.as_ref().cloned().map (|button| {
      assert!(self.button_any.is_none());
      self.button_any = Some (button.into());
    });
    // system
    system.as_ref().cloned().map (|system| {
      assert!(self.system.is_none());
      self.system = Some (system.into());
    });
    // text
    text.as_ref().cloned().map (|text| {
      assert!(self.text.is_none());
      self.text = Some (text.into());
    });
    // motion
    motion.as_ref().cloned().map (|motion| {
      assert!(self.motion.is_none());
      self.motion = Some (motion.into());
    });
    // pointer
    pointer.as_ref().cloned().map (|pointer| {
      assert!(self.pointer.is_none());
      self.pointer = Some (pointer.into());
    });
  }

  /// Insert Bindings to the InputMap, replacing any existing Bindings
  pub fn insert_bindings <A : Application> (&mut self, bindings : &Bindings <A>) {
    let Bindings { buttons, any_button, system, text, motion, pointer } =
      bindings;
    // buttons
    self.buttons.extend (buttons.iter().cloned().map (Into::into));
    // any button
    any_button.as_ref().cloned().map (|button| {
      self.button_any = Some (button.into());
    });
    // system
    system.as_ref().cloned().map (|system| {
      self.system = Some (system.into());
    });
    // text
    text.as_ref().cloned().map (|text| {
      self.text = Some (text.into());
    });
    // motion
    motion.as_ref().cloned().map (|motion| {
      self.motion = Some (motion.into());
    });
    // pointer
    pointer.as_ref().cloned().map (|pointer| {
      self.pointer = Some (pointer.into());
    });
  }

  /// Remove matching controls
  pub fn remove_bindings (&mut self, controls : &Controls) {
    let Controls { buttons, any_button, system, text, motion, pointer } =
      controls;
    // buttons
    self.buttons.retain (|(_, button)| !buttons.contains (button));
    // any button
    if &self.button_any == any_button {
      self.button_any = None;
    }
    // system
    if &self.system == system {
      self.system = None;
    }
    // text
    if &self.text == text {
      self.text = None;
    }
    // motion
    if &self.motion == motion {
      self.motion = None;
    }
    // pointer
    if &self.pointer == pointer {
      self.pointer = None;
    }
  }

  #[inline]
  pub fn clear (&mut self) {
    *self = InputMap::default()
  }
}

impl <A : Application> From <&Bindings <A>> for InputMap {
  /// Constructs an InputMap with the given Bindings
  fn from (bindings : &Bindings <A>) -> Self {
    let Bindings { buttons, any_button, system, text, motion, pointer } =
      bindings;
    let buttons    = KeyVec::from_iter (
      buttons.iter().cloned().map (Into::into));
    let button_any = any_button.clone().map (Into::into);
    let system     = system.clone().map (Into::into);
    let text       = text.clone().map (Into::into);
    let motion     = motion.clone().map (Into::into);
    let pointer    = pointer.clone().map (Into::into);
    InputMap {
      buttons, button_any, system, text, motion, pointer, .. InputMap::default()
    }
  }
}

impl State {
  /// Changes state to Focused and issues a warning if state was not Enabled
  #[inline]
  pub fn focus (&mut self) {
    if self != &State::Enabled {
      log::warn!("focus state not enabled: {:?}", self);
    }
    debug_assert_eq!(self, &State::Enabled);
    *self = State::Focused;
  }
  /// Changes state to Enabled and issues a warning if state was not Focused
  #[inline]
  pub fn defocus (&mut self) {
    if self != &State::Focused {
      log::warn!("defocus state not focused: {:?}", self);
    }
    debug_assert_eq!(self, &State::Focused);
    *self = State::Enabled;
  }
  /// Changes state to Enabled and issues a warning if state was not Disabled
  #[inline]
  pub fn enable (&mut self) {
    if self != &State::Disabled {
      log::warn!("enable state not disabled: {:?}", self);
    }
    debug_assert_eq!(self, &State::Disabled);
    *self = State::Enabled;
  }
  /// Changes state to Disabled and issues a warning if state was not Enabled
  #[inline]
  pub fn disable (&mut self) {
    if self != &State::Enabled {
      log::warn!("disable state not enabled: {:?}", self);
    }
    debug_assert_eq!(self, &State::Enabled);
    *self = State::Disabled;
  }
}

impl Default for State {
  fn default() -> Self {
    State::Enabled
  }
}

impl Appearances {
  #[inline]
  pub fn get (&self, state : State) -> &view::Appearance {
    &self.0[state.clone() as usize]
  }

  #[inline]
  pub fn get_mut (&mut self, state : State) -> &mut view::Appearance {
    &mut self.0[state.clone() as usize]
  }
}

impl AppearancesBuilder {
  pub fn transparent() -> Self {
    AppearancesBuilder::default()
      .style_fg (State::Focused,  color::TRANSPARENT.into())
      .style_bg (State::Focused,  color::TRANSPARENT.into())
      .style_fg (State::Enabled,  color::TRANSPARENT.into())
      .style_bg (State::Enabled,  color::TRANSPARENT.into())
      .style_fg (State::Disabled, color::TRANSPARENT.into())
      .style_bg (State::Disabled, color::TRANSPARENT.into())
  }

  #[inline]
  pub fn state (mut self, state : State, appearance : view::Appearance) -> Self {
    self.0[state as usize] = appearance;
    self
  }
  #[inline]
  pub fn style (mut self, state : State, style : view::Style) -> Self {
    self.0[state as usize].style = Some (style);
    self
  }
  #[inline]
  pub fn style_default (mut self, state : State) -> Self {
    self.0[state as usize].style = Some (view::Style::default());
    self
  }
  #[inline]
  pub fn sound (mut self, state : State, sound : view::Sound) -> Self {
    self.0[state as usize].sound = Some (sound);
    self
  }
  #[inline]
  pub fn pointer (mut self, state : State, pointer : view::Pointer) -> Self {
    self.0[state as usize].pointer = Some (pointer);
    self
  }
  #[inline]
  pub fn style_fg (mut self, state : State, color : view::Color) -> Self {
    let state = state as usize;
    let mut style = self.0[state].style.take().unwrap_or_default();
    style.fg = color;
    self.0[state].style = Some (style);
    self
  }
  #[inline]
  pub fn style_bg (mut self, state : State, color : view::Color) -> Self {
    let state = state as usize;
    let mut style = self.0[state].style.take().unwrap_or_default();
    style.bg = color;
    self.0[state].style = Some (style);
    self
  }
  #[inline]
  pub fn style_lo (mut self, state : State, color : view::Color) -> Self {
    let state = state as usize;
    let mut style = self.0[state].style.take().unwrap_or_default();
    style.lo = color;
    self.0[state].style = Some (style);
    self
  }
  #[inline]
  pub fn style_hi (mut self, state : State, color : view::Color) -> Self {
    let state = state as usize;
    let mut style = self.0[state].style.take().unwrap_or_default();
    style.hi = color;
    self.0[state].style = Some (style);
    self
  }
  #[inline]
  pub fn build (self) -> Appearances {
    Appearances (self.0)
  }
}

impl Default for Area {
  fn default() -> Self {
    Area::Interior
  }
}

impl Orientation {
  pub fn toggle (self) -> Self {
    match self {
      Orientation::Horizontal => Orientation::Vertical,
      Orientation::Vertical   => Orientation::Horizontal
    }
  }
}

impl Default for Orientation {
  fn default() -> Self {
    Orientation::Horizontal
  }
}
