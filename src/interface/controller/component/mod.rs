use std::convert::TryFrom;
use derive_more::{From, TryInto};

pub mod cursor;
pub mod layout;
pub mod numeral;
pub mod scroll;
pub mod selection;
pub mod switch;
pub use self::cursor::Cursor;
pub use self::layout::Layout;
pub use self::numeral::Numeral;
pub use self::scroll::Scroll;
pub use self::selection::Selection;
pub use self::switch::Switch;

#[derive(Clone, Debug, From, TryInto)]
pub enum Component {
  Empty,
  Cursor    (Cursor),
  Layout    (Layout),
  Numeral   (Numeral),
  Scroll    (Scroll),
  Selection (Selection),
  Switch    (Switch)
}

pub trait Kind : Into <Component> + TryFrom <Component> {
  fn try_ref     (component : &Component)     -> Option <&Self>;
  fn try_ref_mut (component : &mut Component) -> Option <&mut Self>;
}

impl Component {
  pub fn clear (&mut self) {
    *self = Component::Empty
  }
}

impl Default for Component {
  fn default() -> Self {
    Component::Empty
  }
}

impl Kind for Component {
  fn try_ref (component : &Component) -> Option <&Self> {
    Some (component)
  }
  fn try_ref_mut (component : &mut Component) -> Option <&mut Self> {
    Some (component)
  }
}

macro impl_kind {
  ($kind:ident) => {
    impl Kind for $kind {
      fn try_ref (component : &Component) -> Option <&Self> {
        match component {
          Component::$kind (t) => Some (t),
          _ => None
        }
      }
      fn try_ref_mut (component : &mut Component) -> Option <&mut Self> {
        match component {
          Component::$kind (t) => Some (t),
          _ => None
        }
      }
    }
  }
}
