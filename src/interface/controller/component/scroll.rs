//! 2D view and layout controller for Textbox and Picture widgets

use crate::interface::controller::{Alignment, Offset};
use super::impl_kind;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Scroll {
  pub offset    : Offset,
  pub alignment : Alignment
}
impl_kind!(Scroll);

impl Scroll {
  #[inline]
  pub fn default_tile_absolute() -> Self {
    Scroll {
      offset:    Offset::default_absolute(),
      alignment: Alignment::tile()
    }
  }
  #[inline]
  pub fn default_tile_relative() -> Self {
    Scroll {
      offset:    Offset::default_relative(),
      alignment: Alignment::tile()
    }
  }
  #[inline]
  pub fn default_pixel_absolute() -> Self {
    Scroll {
      offset:    Offset::default_absolute(),
      alignment: Alignment::pixel()
    }
  }
  #[inline]
  pub fn default_pixel_relative() -> Self {
    Scroll {
      offset:    Offset::default_relative(),
      alignment: Alignment::pixel()
    }
  }
  #[inline]
  pub fn offset_move_right (&mut self, count : u32) {
    self.offset = self.offset.move_right (self.alignment, count);
  }
  #[inline]
  pub fn offset_move_left (&mut self, count : u32) {
    self.offset = self.offset.move_left (self.alignment, count);
  }
  #[inline]
  pub fn offset_move_up (&mut self, count : u32) {
    self.offset = self.offset.move_up (self.alignment, count);
  }
  #[inline]
  pub fn offset_move_down (&mut self, count : u32) {
    self.offset = self.offset.move_down (self.alignment, count);
  }
}
