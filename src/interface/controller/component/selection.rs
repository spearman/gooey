use crate::tree::NodeId;
use super::impl_kind;

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct Selection {
  pub loop_   : bool,
  /// Saves the currently selected item in the menu. An "item" is a sibling node
  /// that is a Frame widget. If the menu parent is focused, this sibling should
  /// also be focused.
  pub current : Option <NodeId>
}
impl_kind!(Selection);
