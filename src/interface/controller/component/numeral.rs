//! Numeric value controller

use super::impl_kind;

#[derive(Clone, Debug, PartialEq)]
pub struct Numeral {
  pub min    : Option <f64>,
  pub max    : Option <f64>,
  pub step   : f64,
  pub format : Format
}
impl_kind!(Numeral);

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Format {
  Integer,
  Float (Option <u8>)
}

impl Numeral {
  /// Modify the given number by the given number of steps and clamp
  pub fn modify (&self, number : f64, steps : f64) -> f64 {
    let mut out = number + steps * self.step;
    self.min.map (|min| out = min.max (out));
    self.max.map (|max| out = max.min (out));
    out
  }
}

impl Default for Numeral {
  fn default() -> Self {
    Numeral {
      min:    None,
      max:    None,
      step:   1.0,
      format: Format::Integer
    }
  }
}

impl Format {
  pub fn format_number (&self, number : f64) -> String {
    let integer_out_of_range =
      || log::warn!("integer format number out of range: {}", number);
    match self {
      Format::Integer => {
        let integer = if number > f64::from (i32::max_value()) {
          integer_out_of_range();
          i32::max_value()
        } else if number < f64::from (i32::min_value()) {
          integer_out_of_range();
          i32::min_value()
        } else {
          number.trunc() as i32
        };
        integer.to_string()
      }
      Format::Float (None) => number.to_string(),
      Format::Float (Some (decimals)) =>
        format!("{:.1$}", number, *decimals as usize)
    }
  }
}
