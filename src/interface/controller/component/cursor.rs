//! Text entry controller

use smallvec::SmallVec;
use super::impl_kind;
use crate::interface::view::input;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Cursor {
  pub caret  : u32,
  pub length : Option <u32>,
  /// Can be used to ignore input of some keycodes, e.g. '`' (`Keycode::Grave`)
  /// for console controls
  pub ignore : SmallVec <[input::button::Keycode; 1]>
}
impl_kind!(Cursor);

impl Default for Cursor {
  fn default() -> Self {
    Cursor {
      caret:  '_' as u32,
      length: None,
      ignore: SmallVec::default()
    }
  }
}
