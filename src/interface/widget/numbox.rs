//! Number box.
//!
//! ```text
//! +--------+
//! | frame  |
//! +---+----+
//!     |
//! +---+----+
//! | numbox |
//! +--------+
//! ```

use lazy_static::lazy_static;

use crate::TreeHelper;
use crate::interface::{view, Action, Element, Model, View};
use crate::interface::controller::{self, controls, Controls};
use crate::tree::{Tree, NodeId};

use super::{Widget, Frame};

pub use self::builder::NumboxBuilder as Builder;

pub type Numbox <'element> = Widget <'element,
  controller::component::Numeral, f64, view::component::Body>;

//
//  controls
//

lazy_static!{
  pub static ref CONTROLS : Controls = controls::Builder::new()
    .buttons (vec![
      controls::button::Builtin::NumboxIncrease,
      controls::button::Builtin::NumboxDecrease,
      controls::button::Builtin::NumboxIncrease10x,
      controls::button::Builtin::NumboxDecrease10x
    ].into_iter().map (Into::into).collect::<Vec <_>>().into())
    .build();
}

/// Builtin button control ID 'NumboxDecrease'
pub fn decrease (
  _             : &controls::button::Release,
  elements      : &Tree <Element>,
  node_id       : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  log::trace!("decrease...");
  let Widget (numeral, value, _) = Numbox::try_get (elements, node_id).unwrap();
  let Widget (_, _, canvas) =
    Frame::try_get (elements, elements.get_parent_id (node_id)).unwrap();
  let new_value = numeral.modify (*value, -1.0);
  let new_body  = body (canvas, &numeral, new_value);
  { // update model
    let update_value =
      Box::new (move |model : &mut Model| model.component = new_value.into());
    action_buffer.push ((node_id.clone(), Action::ModifyModel (update_value)));
  }
  { // update view
    let update_body =
      Box::new (|view : &mut View| view.component = new_body.into());
    action_buffer.push ((node_id.clone(), Action::ModifyView (update_body)));
  }
  log::trace!("...decrease");
}

/// Builtin button control ID 'NumboxIncrease'
pub fn increase (
  _             : &controls::button::Release,
  elements      : &Tree <Element>,
  node_id       : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  log::trace!("increase...");
  let Widget (numeral, value, _) = Numbox::try_get (elements, node_id).unwrap();
  let Widget (_, _, canvas) =
    Frame::try_get (elements, elements.get_parent_id (node_id)).unwrap();
  let new_value = numeral.modify (*value, 1.0);
  let new_body  = body (canvas, &numeral, new_value);
  { // update model
    let update_value =
      Box::new (move |model : &mut Model| model.component = new_value.into());
    action_buffer.push ((node_id.clone(), Action::ModifyModel (update_value)));
  }
  { // update view
    let update_body =
      Box::new (|view : &mut View| view.component = new_body.into());
    action_buffer.push ((node_id.clone(), Action::ModifyView (update_body)));
  }
  log::trace!("...increase");
}

/// Builtin button control ID 'NumboxDecrease10x'
pub fn decrease_10x (
  _             : &controls::button::Release,
  elements      : &Tree <Element>,
  node_id       : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  log::trace!("decrease_10x...");
  let Widget (numeral, value, _) = Numbox::try_get (elements, node_id).unwrap();
  let Widget (_, _, canvas) =
    Frame::try_get (elements, elements.get_parent_id (node_id)).unwrap();
  let new_value = numeral.modify (*value, -10.0);
  let new_body  = body (canvas, &numeral, new_value);
  { // update model
    let update_value =
      Box::new (move |model : &mut Model| model.component = new_value.into());
    action_buffer.push ((node_id.clone(), Action::ModifyModel (update_value)));
  }
  { // update view
    let update_body =
      Box::new (|view : &mut View| view.component = new_body.into());
    action_buffer.push ((node_id.clone(), Action::ModifyView (update_body)));
  }
  log::trace!("...decrease_10x");
}

/// Builtin button control ID 'NumboxIncrease9x'
pub fn increase_10x (
  _             : &controls::button::Release,
  elements      : &Tree <Element>,
  node_id       : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  log::trace!("increase_10x...");
  let Widget (numeral, value, _) = Numbox::try_get (elements, node_id).unwrap();
  let Widget (_, _, canvas) =
    Frame::try_get (elements, elements.get_parent_id (node_id)).unwrap();
  let new_value = numeral.modify (*value, 10.0);
  let new_body  = body (canvas, &numeral, new_value);
  { // update model
    let update_value =
      Box::new (move |model : &mut Model| model.component = new_value.into());
    action_buffer.push ((node_id.clone(), Action::ModifyModel (update_value)));
  }
  { // update view
    let update_body =
      Box::new (|view : &mut View| view.component = new_body.into());
    action_buffer.push ((node_id.clone(), Action::ModifyView (update_body)));
  }
  log::trace!("...increase_10x");
}

//
//  utils
//

/// Utility function to set the umbox contents to the given value
pub fn set_value (
  elements      : &Tree <Element>,
  numbox_id     : &NodeId,
  value         : f64,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  let Widget (numeral, _, _) = Numbox::try_get (elements, numbox_id).unwrap();
  let Widget (_, _, canvas) =
    Frame::try_get (elements, elements.get_parent_id (numbox_id)).unwrap();
  let new_body  = body (canvas, &numeral, value);
  { // update model
    let update_value =
      Box::new (move |model : &mut Model| model.component = value.into());
    action_buffer.push ((numbox_id.clone(), Action::ModifyModel (update_value)));
  }
  { // update view
    let update_body =
      Box::new (|view : &mut View| view.component = new_body.into());
    action_buffer.push ((numbox_id.clone(), Action::ModifyView (update_body)));
  }
}

//
//  crate
//

pub (crate) fn body (
  canvas  : &view::component::Canvas,
  numeral : &controller::component::Numeral,
  value   : f64
) -> view::component::Body {
  log::trace!("body...");
  let body = {
    let body_width = canvas.body_wh().0 as usize;
    let mut string = numeral.format.format_number (value);
    let digits = string.split_off (string.len().saturating_sub (body_width));
    view::component::Body (format!("{:>1$}", digits, body_width))
  };
  log::trace!("...body");
  body
}

//
//  builder
//

mod builder {
  use derive_builder::Builder;
  use crate::Application;
  use crate::interface::{controller, view, Action, Controller, Element, Model};
  use crate::tree::{Tree, NodeId};
  use crate::interface::widget::{self, set_option, Widget, BuildActions};

  #[derive(Builder)]
  #[builder(pattern="owned", build_fn(private), setter(strip_option))]
  pub struct Numbox <'a, A : Application> {
    elements     : &'a Tree <Element>,
    parent_id    : &'a NodeId,
    #[builder(default)]
    appearances  : controller::Appearances,
    #[builder(default)]
    bindings     : Option <&'a controller::Bindings <A>>,
    #[builder(default)]
    frame_anchor : controller::Alignment,
    #[builder(default)]
    frame_appearances : controller::Appearances,
    #[builder(default)]
    frame_area   : controller::Area,
    #[builder(default)]
    frame_bindings : Option <&'a controller::Bindings <A>>,
    #[builder(default)]
    frame_border : Option <view::Border>,
    #[builder(default)]
    frame_offset : controller::Offset,
    #[builder(default = "12")]
    length       : u32,
    #[builder(default)]
    numeral      : controller::component::Numeral,
    #[builder(default)]
    value        : Option <f64>
  }

  impl <'a, A : Application> NumboxBuilder <'a, A> {
    pub fn new (elements : &'a Tree <Element>, parent_id : &'a NodeId) -> Self {
      NumboxBuilder {
        elements:          Some (elements),
        parent_id:         Some (parent_id),
        appearances:       None,
        bindings:          None,
        frame_anchor:      None,
        frame_appearances: None,
        frame_area:        None,
        frame_bindings:    None,
        frame_border:      None,
        frame_offset:      None,
        length:            None,
        numeral:           None,
        value:             None
      }
    }
  }

  impl <'a, A : Application> BuildActions for NumboxBuilder <'a, A> {
    fn build_actions (self) -> Vec<(NodeId, Action)> {
      use std::convert::TryInto;
      use crate::tree::{InsertBehavior, Node};
      use controller::component::layout;
      use view::coordinates;
      log::trace!("build actions...");
      let Numbox {
        elements, parent_id, appearances, bindings, frame_anchor,
        frame_appearances, frame_area, frame_bindings, frame_border,
        frame_offset, length, numeral, value
      } = self.build()
        .map_err(|err| log::error!("frame builder error: {:?}", err))
        .unwrap();
      let bindings_empty = controller::Bindings::empty();
      let bindings = bindings.unwrap_or (&bindings_empty)
        .get_bindings (&super::CONTROLS);
      let frame_bindings = frame_bindings.unwrap_or (&bindings_empty);
      if length == 0 {
        log::warn!("zero length number box field");
      }
      { // only allow tile coordinate parents
        let Widget (_, _, canvas) = widget::Frame::try_get (elements, parent_id)
          .unwrap();
        assert!(canvas.coordinates.kind() == coordinates::Kind::Tile);
      }
      let value   = value.unwrap_or (0.0);
      let mut out = vec![];
      let (mut subtree, order) = {
        let layout = {
          let anchor = frame_anchor;
          let offset = frame_offset;
          let size   = {
            let (border_w, border_h) = frame_border.as_ref()
              .map (view::Border::total_wh).unwrap_or ((0,0));
            let width   = (border_w as u32 + length).into();
            let height  = (border_h as u32 + 1).into();
            controller::Size { width, height }
          };
          layout::Variant::from (
            (layout::Free { anchor, offset, size }, frame_area)
          )
        };
        let mut actions = {
          let mut frame = widget::frame::Builder::new (elements, parent_id)
            .appearances (frame_appearances)
            .bindings (frame_bindings)
            .layout (layout.into());
          set_option!(frame, border, frame_border);
          frame.build_actions()
        };
        out.extend (actions.drain (1..));
        debug_assert_eq!(actions.len(), 1);
        actions.pop().unwrap().1.try_into().unwrap()
      };
      let frame_id = subtree.root_node_id().unwrap().clone();
      let numbox = {
        let controller = {
          let component          = numeral.clone().into();
          let mut controller     = Controller::with_bindings (&bindings);
          controller.component   = component;
          controller.appearances = appearances;
          controller
        };
        let model = Model { component: value.into(), .. Model::default() };
        let view  = {
          let Widget (_, _, canvas) =
            widget::Frame::try_get (&subtree, &frame_id).unwrap();
          view::Component::from (super::body (canvas, &numeral, value)).into()
        };
        Element { controller, model, view, name: "Numbox".to_string()  }
      };
      let _ = subtree
        .insert (Node::new (numbox), InsertBehavior::UnderNode (&frame_id))
        .unwrap();
      out.push ((parent_id.clone(), Action::Create (subtree, order)));
      log::trace!("...build actions");
      out
    }
  }
}
