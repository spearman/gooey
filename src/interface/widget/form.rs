//! Generic widget for submitting callbacks with current model state

use crate::{Tree, TreeHelper};
use crate::tree::NodeId;
use crate::interface::{model, view, Action, Element};
use crate::interface::controller::{self, controls};
use super::Widget;

/// Form is a generic widget (components can be of any type) that allows
/// generating a `SubmitCallback` action with the model component's current
/// callback ID
// TODO: currently the submit_callback control doesn't need to construct an
// instance of a form widget, since the model callback is not stored in the
// variant data; eventually there may be controls for operating on generic model
// data, although reflecting changes in controller or view state could be a
// challenge without specializing
pub type Form <'element> =
  Widget <'element, controller::Component, model::Component, view::Component>;

/// Builtin button control ID 'FormSubmitCallback': get the application callback
/// ID stored in the model component of the target node, and produce a submit
/// action.
///
/// /!\ Model must have a callback ID set.
pub fn submit_callback (
  _ : &controls::button::Release, elements : &Tree <Element>, node_id : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  log::trace!("submit_callback...");
  // no need to get create form widget
  let element     = elements.get_element (node_id);
  let callback_id = element.model.callback_id.as_ref().unwrap_or_else (||{
    log::error!("submit callback: no callback id in model");
    panic!("submit callback: no callback id in model")
  }).clone();
  action_buffer.push ((node_id.clone(), Action::SubmitCallback (callback_id)));
  log::trace!("...submit_callback");
}
