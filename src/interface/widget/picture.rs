//! Image display.
//!
//! ```text
//! +--------+
//! | frame  |
//! +---+----+
//!     |
//! +---+-----+
//! | picture |
//! +---------+
//! ```

use lazy_static::lazy_static;
use mat::Mat;

use crate::interface::{model, view, Action, Element, Widget};
use crate::interface::controller::{self, controls, Controls};
use crate::tree::{Tree, NodeId};

pub use self::builder::{PictureBuilder as Builder, FitBuilder, WithFrameBuilder};

pub type Picture <'element> = Widget <'element,
  controller::component::Scroll,
  model::component::Pixmap,
  view::component::Image>;

//
//  controls
//

lazy_static!{
  pub static ref CONTROLS : Controls = Controls::default();
    /* TODO scroll controls
    buttons: vec![
      controls::button::Builtin::PictureScrollDown,
      controls::button::Builtin::PictureScrollUp,
      controls::button::Builtin::PictureScrollRight,
      controls::button::Builtin::PictureScrollLeft,
      controls::button::Builtin::PicturePageDown,
      controls::button::Builtin::PicturePageUp,
      controls::button::Builtin::PicturePageRight,
      controls::button::Builtin::PicturePageLeft
    ].into_iter().map (Into::into).collect()
    */
}

/// Builtin picture control ID 'PictureScrollRight'
pub fn scroll_right (
  _              : &controls::button::Release,
  _elements      : &Tree <Element>,
  _node_id       : &NodeId,
  _action_buffer : &mut Vec <(NodeId, Action)>
) {
  log::trace!("scroll_right...");
  unimplemented!("TODO: picture scroll right")
  //log::trace!("...scroll_right");
}

//
//  crate
//

pub(crate) fn image (
  canvas : &view::component::Canvas,
  scroll : &controller::component::Scroll,
  pixmap : &model::component::Pixmap
) -> view::component::Image {
  use model::component::Pixmap;
  match &pixmap {
    Pixmap::Pixmap8  (mat) =>
      view::component::Image::Raw8  (make_image (canvas, scroll, &mat)),
    Pixmap::Pixmap16 (mat) =>
      view::component::Image::Raw16 (make_image (canvas, scroll, &mat)),
    Pixmap::Pixmap32 (mat) =>
      view::component::Image::Raw32 (make_image (canvas, scroll, &mat)),
    Pixmap::Pixmap64 (mat) =>
      view::component::Image::Raw64 (make_image (canvas, scroll, &mat)),
    Pixmap::Texture { resource_id, index, .. } =>
      view::component::Image::Texture (view::Texture {
        resource: *resource_id,
        index:    *index
      })
  }
}

//
//  private
//

fn make_image <T : Copy> (
  canvas : &view::component::Canvas,
  _scroll : &controller::component::Scroll,
  pixmap : &Mat <T>
) -> Mat <T> {
  let (image_width, image_height) = canvas.body_wh();
  let columns = pixmap.width().min (image_width as usize);
  let rows    = pixmap.height().min (image_height as usize);
  // TODO: scroll
  let v = if rows > 0 {
    pixmap.rows().take (rows).flat_map (|row| &row[0..columns])
      .cloned().collect::<Vec <T>>()
  } else {
    Vec::new()
  };
  Mat::from_vec ((rows, columns).into(), v).unwrap()
}

//
//  builder
//

mod builder {
  use derive_builder::Builder;

  use crate::Application;
  use crate::interface::{controller, model, view, Action, CreateOrder,
    Controller, Element, Model};
  use crate::interface::widget::{self, set_option, Widget, BuildActions,
    BuildElement};
  use crate::tree::{Tree, NodeId};

  #[derive(Builder)]
  #[builder(pattern="owned", build_fn(private), setter(strip_option))]
  pub struct Picture <'a, A : Application> {
    elements     : &'a Tree <Element>,
    parent_id    : &'a NodeId,
    #[builder(default)]
    bindings     : Option <&'a controller::Bindings <A>>,
    #[builder(default)]
    frame_appearances : controller::Appearances,
    #[builder(default)]
    frame_border : Option <view::Border>,
    #[builder(default)]
    frame_layout : controller::component::Layout,
    #[builder(default)]
    pixmap       : model::component::Pixmap
  }

  #[derive(Builder)]
  #[builder(pattern="owned", build_fn(private), setter(strip_option))]
  pub struct Fit <'a, A : Application> {
    elements     : &'a Tree <Element>,
    parent_id    : &'a NodeId,
    #[builder(default)]
    anchor       : controller::Alignment,
    #[builder(default)]
    appearances  : controller::Appearances,
    #[builder(default)]
    bindings     : Option <&'a controller::Bindings <A>>,
    #[builder(default)]
    border       : Option <view::Border>,
    #[builder(default)]
    clear_color  : Option <view::component::canvas::ClearColor>,
    #[builder(default)]
    coord_kind_override : Option <view::coordinates::Kind>,
    #[builder(default)]
    offset       : controller::Offset,
    #[builder(default)]
    pixmap       : model::component::Pixmap
  }

  #[derive(Builder)]
  #[builder(pattern="owned", build_fn(private), setter(strip_option))]
  pub struct WithFrame <'a, A : Application> {
    elements : &'a Tree <Element>,
    frame_id : &'a NodeId,
    #[builder(default)]
    bindings : Option <&'a controller::Bindings <A>>,
    #[builder(default)]
    pixmap   : model::component::Pixmap,
  }

  impl <'a, A : Application> PictureBuilder <'a, A> {
    pub fn new (elements : &'a Tree <Element>, parent_id : &'a NodeId) -> Self {
      PictureBuilder {
        elements:     Some (elements),
        parent_id:    Some (parent_id),
        bindings:     None,
        frame_appearances: None,
        frame_border: None,
        frame_layout: None,
        pixmap:       None
      }
    }
  }

  impl <'a, A : Application> BuildActions for PictureBuilder <'a, A> {
    /// Parent must have tile coordinates
    // TODO: image alignment
    fn build_actions (self) -> Vec<(NodeId, Action)> {
      use std::convert::TryInto;
      use crate::tree::{InsertBehavior, Node};
      use view::coordinates;
      log::trace!("build actions...");
      let Picture {
        elements, parent_id, bindings, frame_appearances, frame_border,
        frame_layout, pixmap
      } = self.build()
        .map_err(|err| log::error!("frame builder error: {:?}", err))
        .unwrap();
      let bindings_empty = controller::Bindings::empty();
      let bindings = bindings.unwrap_or (&bindings_empty);
      { // only allow tile coordinate parents
        let Widget (_, _, canvas) = widget::Frame::try_get (elements, parent_id)
          .unwrap();
        assert!(canvas.coordinates.kind() == coordinates::Kind::Tile);
      }
      let mut out = vec![];
      let (mut subtree, order) = {
        let mut actions = {
          let mut frame = widget::frame::Builder::new (elements, parent_id)
            .appearances (frame_appearances)
            .bindings (bindings)
            .layout (frame_layout);
          set_option!(frame, border, frame_border);
          frame.build_actions()
        };
        out.extend (actions.drain (1..));
        debug_assert_eq!(actions.len(), 1);
        actions.pop().unwrap().1.try_into().unwrap()
      };
      let frame_id = subtree.root_node_id().unwrap().clone();
      let picture = {
        let Widget (_, _, canvas) = widget::Frame::try_get (&subtree, &frame_id)
          .unwrap();
        let scroll = controller::component::Scroll::default_pixel_absolute()
          .into();
        let image  = super::image (canvas, &scroll, &pixmap);
        let controller = {
          let mut controller   = Controller::with_bindings (
            &bindings.get_bindings (&super::CONTROLS));
          controller.component = scroll.into();
          controller
        };
        let model = Model { component: pixmap.into(), .. Model::default() };
        let view  = view::Component::from (image).into();
        Element { controller, model, view, name: "Picture".to_string() }
      };
      let _ = subtree
        .insert (Node::new (picture), InsertBehavior::UnderNode (&frame_id))
        .unwrap();
      out.push ((parent_id.clone(), Action::Create (subtree, order)));
      log::trace!("...build actions");
      out
    }
  }

  impl <'a, A : Application> FitBuilder <'a, A> {
    pub fn new (elements : &'a Tree <Element>, parent_id : &'a NodeId) -> Self {
      FitBuilder {
        elements:             Some (elements),
        parent_id:            Some (parent_id),
        anchor:               None,
        appearances:          None,
        bindings:             None,
        border:               None,
        clear_color:          None,
        coord_kind_override:  None,
        offset:               None,
        pixmap:               None
      }
    }
  }

  impl <'a, A : Application> BuildActions for FitBuilder <'a, A> {
    /// Create a Picture with a Frame that fits the image contents
    // TODO: image alignment
    fn build_actions (self) -> Vec<(NodeId, Action)> {
      use crate::tree::{InsertBehavior, Node, TreeBuilder};
      use controller::component::{layout, Scroll};
      use view::coordinates;
      log::trace!("fit build actions...");
      let Fit {
        elements, parent_id, anchor, appearances, bindings, border, clear_color,
        coord_kind_override, offset, pixmap
      } = self.build()
        .map_err(|err| log::error!("frame builder error: {:?}", err))
        .unwrap();
      let bindings_empty = controller::Bindings::empty();
      let bindings = bindings.unwrap_or (&bindings_empty);
      let (body_width, body_height) = {
        let Widget (_, _, canvas) = widget::Frame::try_get (elements, parent_id)
          .unwrap();
        if let Some (coord_kind) = coord_kind_override.clone() {
          if canvas.coordinates.kind() == coord_kind {
            (pixmap.width() as u32, pixmap.height() as u32)
          } else {
            match canvas.coordinates.kind() {
              coordinates::Kind::Tile  => {
                // tile frame with pixel image
                let round_up = |dimension, tile|
                  dimension as u32 / tile +
                    if dimension as u32 % tile != 0 { 1 } else { 0 };
                let [tile_w, tile_h] = *coordinates::TILE_WH;
                ( round_up (pixmap.width(),  tile_w),
                  round_up (pixmap.height(), tile_h)
                )
              }
              coordinates::Kind::Pixel =>
                // pixel frame with tile image
                unimplemented!("TODO: pixel frame with tile image")
            }
          }
        } else {
          (pixmap.width() as u32, pixmap.height() as u32)
        }
      };
      let mut out = vec![];
      let frame = {
        let size   = {
          let (border_w, border_h) = border.as_ref().map (view::Border::total_wh)
            .unwrap_or ((0,0));
          let width   = (body_width  + border_w as u32).into();
          let height  = (body_height + border_h as u32).into();
          controller::Size { width, height }
        };
        let layout = layout::Free { anchor, offset, size };
        let mut free = widget::frame::free::Builder::new (elements, parent_id)
          .appearances (appearances)
          .bindings (bindings)
          .layout (layout);
        set_option!(free, border, border);
        set_option!(free, clear_color, clear_color);
        free.build_element()
      };
      let mut subtree = TreeBuilder::new().with_root (Node::new (frame))
        .build();
      let frame_id    = subtree.root_node_id().unwrap().clone();
      let picture     = {
        let Widget (_, _, canvas) = widget::Frame::try_get (&subtree, &frame_id)
          .unwrap();
        let scroll = Scroll::default_pixel_absolute().into();
        let image  = super::image (canvas, &scroll, &pixmap);
        let controller = {
          let mut controller = Controller::with_bindings (
            &bindings.get_bindings (&super::CONTROLS));
          controller.component = scroll.into();
          controller
        };
        let model = Model {
          component: pixmap.clone().into(), .. Model::default()
        };
        let view  = view::Component::from (image).into();
        Element { controller, model, view, name: "Picture".to_string() }
      };
      let _ = subtree
        .insert (Node::new (picture), InsertBehavior::UnderNode (&frame_id))
        .unwrap();
      out.push ((
        parent_id.clone(), Action::Create (subtree, CreateOrder::Append)
      ));
      log::trace!("...fit build actions");
      out
    }
  }

  impl <'a, A : Application> WithFrameBuilder <'a, A> {
    pub fn new (elements : &'a Tree <Element>, frame_id : &'a NodeId) -> Self {
      WithFrameBuilder {
        elements: Some (elements),
        frame_id: Some (frame_id),
        bindings: None,
        pixmap:   None
      }
    }
  }

  impl <'a, A : Application> BuildElement for WithFrameBuilder <'a, A> {
    /// Build with target Frame element
    fn build_element (self) -> Element {
      use view::coordinates;
      log::trace!("with frame build actions...");
      let WithFrame { elements, frame_id, bindings, pixmap } = self.build()
        .map_err(|err| log::error!("with frame builder error: {:?}", err))
        .unwrap();
      let bindings_empty = controller::Bindings::empty();
      let bindings = bindings.unwrap_or (&bindings_empty)
        .get_bindings (&super::CONTROLS);
      let Widget (_, _, canvas) = widget::Frame::try_get (elements, &frame_id)
        .unwrap();
      assert!(canvas.coordinates.kind() == coordinates::Kind::Tile);
      let scroll     = controller::component::Scroll::default_pixel_absolute()
        .into();
      let image      = super::image (canvas, &scroll, &pixmap);
      let controller = {
        let mut controller   = Controller::with_bindings (
          &bindings.get_bindings (&super::CONTROLS));
        controller.component = scroll.into();
        controller
      };
      let model = Model { component: pixmap.into(), .. Model::default() };
      let view  = view::Component::from (image).into();
      log::trace!("...with_frame");
      Element { controller, model, view, name: "Picture".to_string() }
    }
  }
}
