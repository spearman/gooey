//! Display text data.
//!
//! ```text
//! +--------+
//! | frame  |
//! +---+----+
//!     |
//! +---+-----+
//! | textbox |
//! +---------+
//! ```

use lazy_static::lazy_static;

use crate::{Application, Presentation, TreeHelper};
use crate::tree::{Tree, NodeId};
use crate::interface::{
  model, view, Action, Controller, Element, Interface, Model, View
};
use crate::interface::controller::{self, controls, Controls};
use super::{Widget, Frame, build_and_get_node_ids, frame, set_option};

mod make_table;

pub use self::builder::{TextboxBuilder as Builder, FitBuilder, WithFrameBuilder};
pub use self::make_table::{box_vec, make_table, mk_table};

pub type Textbox <'element> = Widget <'element,
  controller::component::Scroll, String, view::component::Body>;

//
//  controls
//

lazy_static!{
  pub static ref CONTROLS : Controls = controls::Builder::new()
    .buttons (vec![
      controls::button::Builtin::TextboxScrollDown,
      controls::button::Builtin::TextboxScrollUp,
      controls::button::Builtin::TextboxScrollRight,
      controls::button::Builtin::TextboxScrollLeft,
      controls::button::Builtin::TextboxPageDown,
      controls::button::Builtin::TextboxPageUp,
      controls::button::Builtin::TextboxPageRight,
      controls::button::Builtin::TextboxPageLeft
    ].into_iter().map (Into::into).collect::<Vec <_>>().into())
    .build();
}

/// Builtin textbox control ID 'TextboxScrollRight'
pub fn scroll_right (
  _             : &controls::button::Release,
  elements      : &Tree <Element>,
  textbox_id    : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  log::trace!("scroll_right...");
  scroll_helper (elements, textbox_id, action_buffer,
    controller::component::Scroll::offset_move_right, 1);
  log::trace!("...scroll_right");
}

/// Builtin textbox control ID 'TextboxScrollLeft'
pub fn scroll_left (
  _             : &controls::button::Release,
  elements      : &Tree <Element>,
  textbox_id    : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  log::trace!("scroll_left...");
  scroll_helper (elements, textbox_id, action_buffer,
    controller::component::Scroll::offset_move_left, 1);
  log::trace!("...scroll_left");
}

/// Builtin textbox control ID 'TextboxScrollUp'
pub fn scroll_up (
  _             : &controls::button::Release,
  elements      : &Tree <Element>,
  textbox_id    : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  log::trace!("scroll_up...");
  scroll_helper (elements, textbox_id, action_buffer,
    controller::component::Scroll::offset_move_up, 1);
  log::trace!("...scroll_up");
}

/// Builtin textbox control ID 'TextboxScrollDown'
pub fn scroll_down (
  _             : &controls::button::Release,
  elements      : &Tree <Element>,
  textbox_id    : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  log::trace!("scroll_down...");
  scroll_helper (elements, textbox_id, action_buffer,
    controller::component::Scroll::offset_move_down, 1);
  log::trace!("...scroll_down");
}

/// Builtin textbox control ID 'TextboxPageRight'
pub fn page_right (
  _             : &controls::button::Release,
  elements      : &Tree <Element>,
  textbox_id    : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  log::trace!("page_right...");
  // scroll by width - 1
  let (width, _) = {
    let Widget (_, _, canvas) =
      Frame::try_get (elements, elements.get_parent_id (textbox_id)).unwrap();
    canvas.body_wh()
  };
  scroll_helper (elements, textbox_id, action_buffer,
    controller::component::Scroll::offset_move_right, width-1);
  log::trace!("...page_right");
}

/// Builtin textbox control ID 'TextboxPageLeft'
pub fn page_left (
  _             : &controls::button::Release,
  elements      : &Tree <Element>,
  textbox_id    : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  log::trace!("page_left...");
  // scroll by width - 1
  let (width, _) = {
    let Widget (_, _, canvas) =
      Frame::try_get (elements, elements.get_parent_id (textbox_id)).unwrap();
    canvas.body_wh()
  };
  scroll_helper (elements, textbox_id, action_buffer,
    controller::component::Scroll::offset_move_left, width-1);
  log::trace!("...page_left");
}

/// Builtin textbox control ID 'TextboxPageUp'
pub fn page_up (
  _             : &controls::button::Release,
  elements      : &Tree <Element>,
  textbox_id    : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  log::trace!("page_up...");
  // scroll by height - 1
  let (_, height) = {
    let Widget (_, _, canvas) =
      Frame::try_get (elements, elements.get_parent_id (textbox_id)).unwrap();
    canvas.body_wh()
  };
  scroll_helper (elements, textbox_id, action_buffer,
    controller::component::Scroll::offset_move_up, height-1);
  log::trace!("...page_up");
}

/// Builtin textbox control ID 'TextboxPageDown'
pub fn page_down (
  _             : &controls::button::Release,
  elements      : &Tree <Element>,
  textbox_id    : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  log::trace!("page_down...");
  // scroll by height - 1
  let (_, height) = {
    let Widget (_, _, canvas) =
      Frame::try_get (elements, elements.get_parent_id (textbox_id)).unwrap();
    canvas.body_wh()
  };
  scroll_helper (elements, textbox_id, action_buffer,
    controller::component::Scroll::offset_move_down, height-1);
  log::trace!("...page_down");
}

//
//  utils
//

/// Create a new textbox in the middle of the parent element and focus
pub fn popup <A, P> (
  interface   : &mut Interface <A, P>,
  parent_id   : NodeId,
  text        : String,
  appearances : Option <controller::Appearances>,
  border      : Option <view::Border>
) -> [NodeId; 2] where
  A : Application,
  P : Presentation
{
  let mut builder = FitBuilder::<A>::new (interface.elements(), &parent_id)
    .frame_anchor (controller::Alignment::middle_center())
    .text (text);
  set_option!(builder, frame_appearances, appearances);
  set_option!(builder, frame_border, border);
  let [frame_id, textbox_id] =
    build_and_get_node_ids!(interface, builder, ["Frame", "Textbox"]);
  let _ = interface.action (&frame_id, Action::Focus);
  [frame_id, textbox_id]
}

/// Utility function to get the textbox contents
pub fn get_contents (elements : &Tree <Element>, textbox_id : &NodeId)
  -> String
{
  let Widget (_, contents, _) = Textbox::try_get (elements, textbox_id).unwrap();
  contents.clone()
}

/// Utility function to set the textbox contents to the given string
pub fn set_contents (
  elements      : &Tree <Element>,
  textbox_id    : &NodeId,
  contents      : String,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  let new_body = {
    // TODO: we may need to update scroll here
    let Widget (scroll, _, _) = Textbox::try_get (elements, textbox_id).unwrap();
    let Widget (_, _, canvas) =
      Frame::try_get (elements, elements.get_parent_id (textbox_id)).unwrap();
    body (canvas, &scroll, &contents)
  };
  { // update model
    let update_text =
      Box::new (|model : &mut Model| model.component = contents.into());
    action_buffer.push ((textbox_id.clone(), Action::ModifyModel (update_text)));
  }
  { // update view
    let update_body =
      Box::new (|view : &mut View| view.component = new_body.into());
    action_buffer.push ((textbox_id.clone(), Action::ModifyView (update_body)));
  }
}

/// Utility function to resize the textbox to the current contents
pub fn fit_to_contents (
  elements      : &Tree <Element>,
  textbox_id    : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  use controller::component::layout;
  let (width, height) = {
    let contents = get_contents (elements, textbox_id);
    let lines    = contents.lines().map (str::len).collect::<Vec <_>>();
    let height   = lines.len() as u32;
    let width    = (*lines.iter().max().unwrap_or (&0)) as u32;
    (width, height)
  };
  let frame_id = elements.get_parent_id (textbox_id);
  let mut layout = {
    let Widget (layout, _, _) = Frame::try_get (elements, frame_id).unwrap();
    layout.clone()
  };
  match layout.variant {
    layout::Variant::Free (ref mut free, _) => {
      let old_size = free.size;
      free.size.width  = width.into();
      free.size.height = height.into();
      if old_size != free.size {
        frame::set_layout (elements, frame_id, layout, None, action_buffer);
      }
    }
    layout::Variant::Tiled (_) =>
      log::warn!("fit to contents called on tiled textbox frame")
  }
}

/// Utility function to push a newline followed by the given string onto the
/// textbox contents
pub fn push_line (
  elements      : &Tree <Element>,
  textbox_id    : &NodeId,
  line          : String,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  let new_body = {
    // TODO: we may need to update scroll here
    let Widget (scroll, contents, _) = Textbox::try_get (elements, textbox_id)
      .unwrap();
    let mut contents = contents.clone();
    contents.push ('\n');
    contents.push_str (&line);
    let Widget (_, _, canvas) =
      Frame::try_get (elements, elements.get_parent_id (textbox_id)).unwrap();
    body (canvas, &scroll, &contents)
  };
  { // update model
    let update_text = Box::new (move |model : &mut Model| match model.component {
      model::Component::Text (ref mut text) => {
        text.push ('\n');
        text.push_str (&line);
      }
      _ => unreachable!()
    });
    action_buffer.push ((textbox_id.clone(), Action::ModifyModel (update_text)));
  }
  { // update view
    let update_body =
      Box::new (|view : &mut View| view.component = new_body.into());
    action_buffer.push ((textbox_id.clone(), Action::ModifyView (update_body)));
  }
}

//
//  crate
//

pub (crate) fn body (
  canvas : &view::component::Canvas,
  scroll : &controller::component::Scroll,
  text   : &String
) -> view::component::Body {
  use controller::{alignment, offset};
  log::trace!("body...");
  let (body_width, body_height) = canvas.body_wh();
  let lines   = text.lines().count() as u32;
  let longest = text.lines().map (str::len).max().unwrap_or_default() as u32;
  let scroll_horizontal = match scroll.offset.horizontal {
    offset::Signed::Absolute (horizontal) => horizontal,
    offset::Signed::Relative (horizontal) => {
      let horizontal_max = longest.saturating_sub (body_width) as f32;
      (*horizontal * horizontal_max).round() as i32
    }
  };
  let scroll_vertical = match scroll.offset.vertical {
    offset::Signed::Absolute (vertical) => vertical,
    offset::Signed::Relative (vertical) => {
      let vertical_max = lines.saturating_sub (body_height) as f32;
      (*vertical * vertical_max).round() as i32
    }
  };
  let mut body = String::new();
  let (skip, body_top) = match scroll.alignment.vertical {
    alignment::Vertical::Top    => (scroll_vertical as usize, 0),
    alignment::Vertical::Bottom => {
      for _ in 0..body_height.saturating_sub (lines) {
        body.push ('\n');
      }
      ( (lines.saturating_sub (body_height) as i32 - scroll_vertical).max (0)
          as usize,
        0
      )
    }
    alignment::Vertical::Middle => {
      let center   = lines as i32 / 2;
      let body_top = center - body_height as i32 / 2 - scroll_vertical;
      for _ in body_top..0 {
        body.push ('\n');
      }
      (body_top.max (0) as usize, body_top)
    }
  };
  // NOTE: take at most body_height lines, but stops at the last line if too
  // few; for middle scroll alignment subtract negative body_top from
  // body_height
  let take = (body_height as i32 + body_top.min (0)).max (0) as usize;
  let mut lines = text.lines().skip (skip).take (take);
  while let Some (line) = lines.next() {
    let len = line.len();
    let (space, start, end) = match scroll.alignment.horizontal {
      alignment::Horizontal::Left   => (
        0,
        scroll_horizontal  as usize,
        (scroll_horizontal as usize + body_width as usize).min (len)
      ),
      alignment::Horizontal::Center => {
        let center     = longest as i32 / 2;
        let body_left  = center - body_width as i32 / 2 + scroll_horizontal;
        let body_right = center + body_width as i32 / 2 + scroll_horizontal;
        let line_start = center - len as i32 / 2;
        let line_end   = center + len as i32 / 2 - (len as i32 + 1) % 2;
        let space      = (line_start - body_left).max (0)      as usize;
        let start      = (body_left - line_start).max (0)      as usize;
        let end        = len - (line_end - body_right).max (0) as usize;
        (space, start, end)
      }
      alignment::Horizontal::Right  =>
        ( (body_width as usize + scroll_horizontal as usize)
            .saturating_sub (len),
          len.saturating_sub (body_width as usize)
            .saturating_sub (scroll_horizontal as usize),
          len
        )
    };
    if start < end {
      for _ in 0..space {
        body.push ('\0');
      }
      // NOTE: previously we sliced here but if the string contains non-ascii
      // characters the byte boundary will not be aligned
      for ch in line.chars().skip (start).take (end - start) {
        body.push (ch);
      }
      body.push ('\n');
    } else if start == end {
      body.push ('\n');
    }
  }
  log::trace!("...body");
  view::component::Body (body)
}

//
//  private
//

fn scroll_helper (
  elements      : &Tree <Element>,
  textbox_id    : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>,
  scroll_fun    : fn (&mut controller::component::Scroll, u32),
  count         : u32
) {
  use controller::{alignment, offset};
  let Widget (scroll, text, _) = Textbox::try_get (elements, textbox_id)
    .unwrap();
  let Widget (_, _, canvas) =
    Frame::try_get (elements, elements.get_parent_id (textbox_id)).unwrap();
  let lines           = text.lines().count() as u32;
  let longest         = text.lines().map (str::len).max().unwrap() as u32;
  let (body_width, body_height) = canvas.body_wh();
  let horizontal_max  = longest.saturating_sub (body_width) as i32;
  let vertical_max    = lines.saturating_sub (body_height)  as i32;
  let new_scroll      = {
    let mut scroll = scroll.clone();
    scroll_fun (&mut scroll, count);
    match &mut scroll.offset.horizontal {
      offset::Signed::Absolute (ref mut horizontal) => {
        match scroll.alignment.horizontal {
          alignment::Horizontal::Left | alignment::Horizontal::Right => {
            *horizontal = (*horizontal).max (0);
            *horizontal = (*horizontal).min (horizontal_max);
          }
          alignment::Horizontal::Center => {
            *horizontal = (*horizontal)
              .min (horizontal_max / 2 + (body_width+1) as i32 % 2)
              .max (-horizontal_max / 2);
          }
        }
      }
      _ => {} // TODO: scroll relative ?
    }
    match &mut scroll.offset.vertical {
      offset::Signed::Absolute (ref mut vertical) => {
        match scroll.alignment.vertical {
          alignment::Vertical::Top | alignment::Vertical::Bottom => {
            *vertical   = (*vertical).max (0);
            *vertical   = (*vertical).min (vertical_max);
          }
          alignment::Vertical::Middle => {
            if vertical_max == 0 {
              *vertical = 0;
            } else {
              *vertical   = (*vertical)
                .min (vertical_max / 2 + body_height as i32 % 2)
                .max (-vertical_max / 2);
            }
          }
        }
      }
      _ => {} // TODO: scroll relative ?
    }
    scroll
  };
  if &new_scroll != scroll {
    let new_body = body (canvas, &new_scroll, text);
    { // update scroll
      let update_scroll = Box::new (move |controller : &mut Controller|
        controller.component = new_scroll.into());
      action_buffer.push (
        (textbox_id.clone(), Action::ModifyController (update_scroll)));
    }
    { // update body
      let update_body = Box::new (move |view : &mut View|
        view.component = new_body.into());
      action_buffer.push (
        (textbox_id.clone(), Action::ModifyView (update_body)));
    }
  }
}

//
//  builder
//

mod builder {
  use derive_builder::Builder;
  use crate::Application;
  use crate::interface::{controller, view, Action, Controller, Element, Model};
  use crate::interface::widget::{self, set_option, BuildActions, BuildElement,
    Widget};
  use crate::tree::{Tree, NodeId};

  #[derive(Builder)]
  #[builder(pattern="owned", build_fn(private), setter(strip_option))]
  pub struct Textbox <'a, A : Application> {
    elements       : &'a Tree <Element>,
    parent_id      : &'a NodeId,
    #[builder(default)]
    appearances    : controller::Appearances,
    #[builder(default)]
    bindings       : Option <&'a controller::Bindings <A>>,
    #[builder(default)]
    frame_appearances : controller::Appearances,
    #[builder(default)]
    frame_border   : Option <view::Border>,
    #[builder(default)]
    frame_disabled : bool,
    #[builder(default)]
    frame_layout   : controller::component::Layout,
    #[builder(default)]
    text           : String,
    #[builder(default)]
    scroll         : Option <controller::component::Scroll>
  }

  #[derive(Builder)]
  #[builder(pattern="owned", build_fn(private), setter(strip_option))]
  pub struct Fit <'a, A : Application> {
    elements       : &'a Tree <Element>,
    parent_id      : &'a NodeId,
    #[builder(default)]
    appearances    : controller::Appearances,
    #[builder(default)]
    bindings       : Option <&'a controller::Bindings <A>>,
    #[builder(default)]
    frame_anchor   : controller::Alignment,
    #[builder(default)]
    frame_appearances : controller::Appearances,
    #[builder(default)]
    frame_area     : controller::Area,
    #[builder(default)]
    frame_border   : Option <view::Border>,
    #[builder(default)]
    frame_disabled : bool,
    #[builder(default)]
    frame_offset   : controller::Offset,
    #[builder(default)]
    scroll         : Option <controller::component::Scroll>,
    #[builder(default)]
    text           : String
  }

  #[derive(Builder)]
  #[builder(pattern="owned", build_fn(private), setter(strip_option))]
  pub struct WithFrame <'a, A : Application> {
    elements     : &'a Tree <Element>,
    frame_id     : &'a NodeId,
    #[builder(default)]
    appearances  : controller::Appearances,
    #[builder(default)]
    bindings     : Option <&'a controller::Bindings <A>>,
    #[builder(default)]
    scroll       : Option <controller::component::Scroll>,
    #[builder(default)]
    text         : String
  }

  impl <'a, A : Application> TextboxBuilder <'a, A> {
    pub fn new (elements : &'a Tree <Element>, parent_id : &'a NodeId) -> Self {
      TextboxBuilder {
        elements:          Some (elements),
        parent_id:         Some (parent_id),
        appearances:       None,
        bindings:          None,
        frame_appearances: None,
        frame_border:      None,
        frame_disabled:    None,
        frame_layout:      None,
        scroll:            None,
        text:              None
      }
    }
  }

  impl <'a, A : Application> BuildActions for TextboxBuilder <'a, A> {
    fn build_actions (self) -> Vec<(NodeId, Action)> {
      use std::convert::TryInto;
      use crate::tree::{InsertBehavior, Node};
      use view::coordinates;
      log::trace!("build actions...");
      let Textbox {
        elements, parent_id, appearances, bindings, frame_appearances,
        frame_border, frame_disabled, frame_layout, text, scroll
      } = self.build()
        .map_err(|err| log::error!("frame builder error: {:?}", err))
        .unwrap();
      let bindings_empty = controller::Bindings::empty();
      let bindings = bindings.unwrap_or (&bindings_empty);
      { // only allow tile coordinate parents
        let Widget (_, _, canvas) = widget::Frame::try_get (elements, parent_id)
          .unwrap();
        assert!(canvas.coordinates.kind() == coordinates::Kind::Tile);
      }
      let mut out = vec![];
      let (mut subtree, order) = {
        let mut actions = {
          let mut frame = widget::frame::Builder::new (elements, parent_id)
            .appearances (frame_appearances)
            .bindings (&bindings)
            .disabled (frame_disabled)
            .layout (frame_layout);
          set_option!(frame, border, frame_border);
          frame.build_actions()
        };
        out.extend (actions.drain (1..));
        debug_assert_eq!(actions.len(), 1);
        actions.pop().unwrap().1.try_into().unwrap()
      };
      let frame_id = subtree.root_node_id().unwrap().clone();
      let textbox = {
        let Widget (_, _, canvas) = widget::Frame::try_get (&subtree, &frame_id)
          .unwrap();
        let scroll     = scroll
          .unwrap_or (controller::component::Scroll::default_tile_absolute());
        let body       = super::body (canvas, &scroll, &text);
        let controller = {
          let mut controller = Controller::with_bindings (
            &bindings.get_bindings (&super::CONTROLS));
          controller.appearances = appearances;
          controller.component   = scroll.into();
          controller
        };
        let model = Model { component: text.into(), .. Model::default() };
        let view  = view::Component::from (body).into();
        Element { controller, model, view, name: "Textbox".to_string() }
      };
      let _ = subtree
        .insert (Node::new (textbox), InsertBehavior::UnderNode (&frame_id))
        .unwrap();
      out.push ((parent_id.clone(), Action::Create (subtree, order)));
      log::trace!("...build actions");
      out
    }
  }

  impl <'a, A : Application> FitBuilder <'a, A> {
    pub fn new (elements : &'a Tree <Element>, parent_id : &'a NodeId) -> Self {
      FitBuilder {
        elements:          Some (elements),
        parent_id:         Some (parent_id),
        appearances:       None,
        bindings:          None,
        frame_anchor:      None,
        frame_appearances: None,
        frame_area:        None,
        frame_border:      None,
        frame_disabled:    None,
        frame_offset:      None,
        text:              None,
        scroll:            None
      }
    }
  }

  impl <'a, A : Application> BuildActions for FitBuilder <'a, A> {
    /// Create a parent frame with body size determined by the given text
    fn build_actions (self) -> Vec<(NodeId, Action)> {
      use std::convert::TryInto;
      use crate::tree::{InsertBehavior, Node};
      use controller::component::{layout, Scroll};
      use view::coordinates;
      log::trace!("fit build actions...");
      let Fit {
        elements, parent_id, appearances, bindings, frame_anchor,
        frame_appearances, frame_area, frame_border, frame_disabled,
        frame_offset, scroll, text
      } = self.build()
        .map_err(|err| log::error!("frame builder error: {:?}", err))
        .unwrap();
      let bindings_empty = controller::Bindings::empty();
      let bindings = bindings.unwrap_or (&bindings_empty);
      { // only allow tile coordinate parents
        let Widget (_, _, canvas) = widget::Frame::try_get (elements, parent_id)
          .unwrap();
        assert!(canvas.coordinates.kind() == coordinates::Kind::Tile);
      }
      let mut out = vec![];
      let (mut subtree, order) = {
        let size   = {
          let longest = text.lines().map (str::len).max().unwrap() as u32;
          let lines   = text.lines().count() as u32;
          let (border_w, border_h) = frame_border.as_ref()
            .map (view::Border::total_wh).unwrap_or ((0,0));
          let width   = (longest + border_w as u32).into();
          let height  = (lines   + border_h as u32).into();
          controller::Size { width, height }
        };
        let layout = layout::Variant::from ((
          layout::Free { anchor: frame_anchor, offset: frame_offset, size },
          frame_area
        ));
        let mut actions = {
          let mut frame = widget::frame::Builder::new (elements, parent_id)
            .appearances (frame_appearances)
            .bindings (bindings)
            .disabled (frame_disabled)
            .layout (layout.into());
          set_option!(frame, border, frame_border);
          frame.build_actions()
        };
        out.extend (actions.drain (1..));
        debug_assert_eq!(actions.len(), 1);
        actions.pop().unwrap().1.try_into().unwrap()
      };
      let frame_id = subtree.root_node_id().unwrap().clone();
      let textbox = {
        let Widget (_, _, canvas) = widget::Frame::try_get (&subtree, &frame_id)
          .unwrap();
        let scroll = scroll.unwrap_or (Scroll::default_tile_absolute());
        let body   = super::body (canvas, &scroll, &text);
        let controller = {
          let mut controller     = Controller::with_bindings (&bindings);
          controller.appearances = appearances;
          controller.component   = scroll.into();
          controller
        };
        let model  = Model {
          component: text.clone().into(), .. Model::default()
        };
        let view   = view::Component::from (body).into();
        Element { controller, model, view, name: "Textbox".to_string() }
      };
      let _ = subtree
        .insert (Node::new (textbox), InsertBehavior::UnderNode (&frame_id))
        .unwrap();
      out.push ((parent_id.clone(), Action::Create (subtree, order)));
      log::trace!("...fit build actions");
      out
    }
  }

  impl <'a, A : Application> WithFrameBuilder <'a, A> {
    pub fn new (elements : &'a Tree <Element>, frame_id : &'a NodeId) -> Self {
      WithFrameBuilder {
        elements:    Some (elements),
        frame_id:    Some (frame_id),
        bindings:    None,
        appearances: None,
        text:        None,
        scroll:      None
      }
    }
  }

  impl <'a, A : Application> BuildElement for WithFrameBuilder <'a, A> {
    /// Build Textbox element targeting an existing Frame node; Frame must have
    /// tile coordinates.
    fn build_element (self) -> Element {
      use view::coordinates;
      log::trace!("with frame build element...");
      let WithFrame {
        elements, frame_id, bindings, appearances, text, scroll
      } = self.build()
        .map_err(|err| log::error!("frame builder error: {:?}", err)).unwrap();
      let bindings_empty = controller::Bindings::empty();
      let bindings = bindings.unwrap_or (&bindings_empty)
        .get_bindings (&super::CONTROLS);
      let Widget (_, _, canvas) = widget::Frame::try_get (elements, &frame_id)
        .unwrap();
      assert!(canvas.coordinates.kind() == coordinates::Kind::Tile);
      let scroll = scroll
        .unwrap_or (controller::component::Scroll::default_tile_absolute());
      let body   = super::body (canvas, &scroll, &text);
      let controller = {
        let mut controller     = Controller::with_bindings (&bindings);
        controller.appearances = appearances;
        controller.component   = scroll.into();
        controller
      };
      let model = Model { component: text.into(), .. Model::default() };
      let view  = view::Component::from (body).into();
      log::trace!("...with frame build element");
      Element { controller, model, view, name: "Textbox".to_string() }
    }
  }
}
