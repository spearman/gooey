//! A widget for audio playback.
//!
//! Requires only an Sfx view and can be parented to any other widget.
//! Implements a state machine where control functions `play`, `loop_` (note:
//! `loop` is a keyword in Rust), and `ready` correspond to the possible states
//! and will set the state immediately in the Sfx component.

use crate::Tree;
use crate::tree::NodeId;
use crate::interface::{model, Action, Element, View};
use crate::interface::controller::{self, controls};
use crate::interface::view;
use crate::interface::view::component::Kind;
use super::Widget;

/// Widget for controlling playback of an Sfx component
// TODO: since this only relies on one component (view), the playback *widget*
// doesn't need to be downcast to; eventually these playback controls may need
// to be generalized to other kinds of view components or possibly controller
// and model components (e.g. a controller mixer component)
pub type Playback <'element> = Widget <'element,
  controller::Component, model::Component, view::component::Sfx>;

pub fn new (sound : Option <view::Sound>) -> Element {
  log::trace!("new...");
  let view = {
    use view::component::{sfx, Sfx};
    let playback = sound.map (|id| (id, sfx::State::Ready));
    view::Component::from (Sfx { playback }).into()
  };
  let out = Element {
    view, name: "Playback".to_string(), .. Element::default()
  };
  log::trace!("...new");
  out
}

//
//  controls
//

/// Builtin button control ID 'PlaybackPlay'
pub fn play (
  _ : &controls::button::Release, _elements : &Tree <Element>, node_id : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  log::trace!("play...");
  action_buffer.push ((node_id.clone(), Action::ModifyView (
    Box::new (|view : &mut View|{
      use view::component::{sfx, Sfx};
      let sfx = Sfx::try_ref_mut (&mut view.component).unwrap();
      let (_, state) = sfx.playback.as_mut().unwrap();
      *state = sfx::State::Play;
    })
  )));
  log::trace!("...play");
}

/// Builtin button control ID 'PlaybackLoop'.
///
/// `loop` is a keyword in Rust.
pub fn loop_ (
  _ : &controls::button::Release, _elements : &Tree <Element>, node_id : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  log::trace!("loop...");
  action_buffer.push ((node_id.clone(), Action::ModifyView (
    Box::new (|view : &mut View|{
      use view::component::{sfx, Sfx};
      let sfx = Sfx::try_ref_mut (&mut view.component).unwrap();
      let (_, state) = sfx.playback.as_mut().unwrap();
      *state = sfx::State::Loop;
    })
  )));
  log::trace!("...loop");
}

/// Builtin button control ID 'PlaybackReady'
pub fn ready (
  _ : &controls::button::Release, _elements : &Tree <Element>, node_id : &NodeId,
  action_buffer : &mut Vec <(NodeId, Action)>
) {
  log::trace!("ready...");
  action_buffer.push ((node_id.clone(), Action::ModifyView (
    Box::new (|view : &mut View|{
      use view::component::{sfx, Sfx};
      let sfx = Sfx::try_ref_mut (&mut view.component).unwrap();
      let (_, state) = sfx.playback.as_mut().unwrap();
      *state = sfx::State::Ready;
    })
  )));
  log::trace!("...ready");
}
