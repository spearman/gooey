#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Border {
  pub top              : u32,
  pub bottom           : u32,
  pub left             : u32,
  pub right            : u32,
  pub top_left         : u32,
  pub top_right        : u32,
  pub bottom_left      : u32,
  pub bottom_right     : u32,
  pub thickness_top    : u16,
  pub thickness_bottom : u16,
  pub thickness_left   : u16,
  pub thickness_right  : u16
}

impl Border {
  #[inline]
  pub fn total_width (&self) -> u16 {
    self.thickness_left + self.thickness_right
  }
  #[inline]
  pub fn total_height (&self) -> u16 {
    self.thickness_top + self.thickness_bottom
  }
  #[inline]
  pub fn total_wh (&self) -> (u16, u16) {
    (self.total_width(), self.total_height())
  }
  pub const fn eascii_lines() -> Self {
    Border {
      top:              0xA6,
      bottom:           0xA6,
      left:             0xA9,
      right:            0xA9,
      top_left:         0xA3,
      top_right:        0xA5,
      bottom_left:      0xAA,
      bottom_right:     0xAC,
      thickness_top:    1,
      thickness_bottom: 1,
      thickness_left:   1,
      thickness_right:  1
    }
  }
}

impl Default for Border {
  fn default() -> Border {
    Border {
      top:              b'-' as u32,
      bottom:           b'-' as u32,
      left:             b'|' as u32,
      right:            b'|' as u32,
      top_left:         b'+' as u32,
      top_right:        b'+' as u32,
      bottom_left:      b'+' as u32,
      bottom_right:     b'+' as u32,
      thickness_top:    1,
      thickness_bottom: 1,
      thickness_left:   1,
      thickness_right:  1
    }
  }
}
