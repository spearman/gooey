use derive_more::From;

#[derive(Clone, Debug, Eq, Ord, PartialEq, PartialOrd, From)]
pub struct Sound (pub IndexType);

pub type IndexType = u16;

impl Sound {
  pub fn index (&self) -> usize {
    self.0 as usize
  }
}
