use std;
use bitflags::bitflags;
use derive_more::{From, TryInto};
use serde::{Deserialize, Serialize};

pub mod button;
pub use self::button::Button;

type ModifiersPrimitiveType = u8;

/// A user or OS event from the presentation layer
#[derive(Clone, Debug, PartialEq, From, TryInto)]
pub enum Input {
  Axis    (Axis),
  Button  (Button, button::State),
  Motion  (Motion),
  Pointer (Pointer),
  System  (System),
  Text    (Text),
  Wheel   (Wheel)
}

/// These mostly correspond to non-user-input `glutin::WindowEvent`s
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum System {
  Close,
  CursorEntered,
  CursorLeft,
  Destroyed,
  Focused (bool),
  InputMethod (/*TODO*/),
  Occluded (bool),
  Redraw,
  Resized {
    width  : u32,
    height : u32
  },
  // NOTE: resume is spammed so we are not handling it for now
  //Resumed
}

/// Corresponds to glutin `Motion` device event.
///
/// Glutin mouse motion will produce motion events for axis 0 and 1.
///
/// Note that glutin also produces `AxisMotion` window events for mouse axis
/// motion, but the value is in window coordinates, not axis deltas.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Axis {
  pub axis  : u32,
  pub value : i32
}

/// Change in X and Y directions.
///
/// Corresponds to the glutin `MouseMotion` device event. Use for FPS controls.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Motion (pub i32, pub i32);

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Pointer {
  pub position_horizontal : f32,
  pub position_vertical   : f32,
  pub modifiers           : Modifiers
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Text {
  Char   (char),
  String (String)
}

/// Corresponds to glutin `MouseWheel` window event.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Wheel {
  pub delta     : (i32, i32),
  pub modifiers : Modifiers
  // TODO: touch phase ?
}

bitflags! {
  #[derive(Clone, Copy, Debug, Default, Eq, Ord, PartialEq, PartialOrd,
    Deserialize, Serialize)]
  pub struct Modifiers : ModifiersPrimitiveType {
    const SHIFT = 0b0_0001;
    const CTRL  = 0b0_0010;
    const ALT   = 0b0_0100;
    const SUPER = 0b0_1000;
    const ANY   = 0b1_0000;
  }
}

impl std::string::ToString for Modifiers {
  fn to_string (&self) -> String {
    let mut s = String::new();
    if self.contains (Modifiers::SHIFT) {
      s.push_str ("+Shift")
    }
    if self.contains (Modifiers::CTRL) {
      s.push_str ("+Ctrl")
    }
    if self.contains (Modifiers::ALT) {
      s.push_str ("+Alt")
    }
    if self.contains (Modifiers::SUPER) {
      s.push_str ("+Super")
    }
    if self.contains (Modifiers::ANY) {
      s.push_str ("+Any")
    }
    s
  }
}
