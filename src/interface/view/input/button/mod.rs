use std;
use derive_more::{From, TryInto};
use parse_display::{Display, FromStr};
use strum::{EnumCount, FromRepr};
use super::Modifiers;

pub mod keycode;
pub use self::keycode::Keycode;

#[derive(Clone, Copy, Debug, Eq)]
pub struct Button {
  pub variant   : Variant,
  pub modifiers : Modifiers
}

#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub enum State {
  Pressed,
  Released
}

/// Identifies which button was pressed.
///
/// NOTE: the types contained in this newtype wrapper should have unique
/// serializations, this is so we can omit the tag when giving bindings in
/// configuration files (see `control::button::Binding`).
///
/// `serde_plain` is unable to parse untagged enums, so we use the
/// `parse_display::FromStr` derive instead.
// TODO: joystick buttons ?
#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd,
  Display, FromStr, From, TryInto)]
#[display("{0:?}")]  // an "untagged" serialization format for FromStr
pub enum Variant {
  Keycode (Keycode),
  Mouse   (Mouse)
}

/// A mouse button
#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd, EnumCount,
  FromRepr, FromStr)]
pub enum Mouse {
  Mouse1, Mouse2, Mouse3, Mouse4, Mouse5
}

impl PartialEq for Button {
  fn eq (&self, other : &Self) -> bool {
    if self.variant != other.variant {
      false
    } else {
      if self.modifiers.contains (Modifiers::ANY) ||
        other.modifiers.contains (Modifiers::ANY)
      {
        true
      } else {
        self.modifiers == other.modifiers
      }
    }
  }
}

// NOTE: we need to define partial ord to use custom partial eq comparison
impl PartialOrd for Button {
  fn partial_cmp (&self, other : &Self) -> Option <std::cmp::Ordering> {
    let out = if self == other {
      std::cmp::Ordering::Equal
    } else {
      if self.variant > other.variant {
        std::cmp::Ordering::Greater
      } else if self.variant < other.variant {
        std::cmp::Ordering::Less
      } else if self.modifiers > other.modifiers {
        std::cmp::Ordering::Greater
      } else if self.modifiers < other.modifiers {
        std::cmp::Ordering::Less
      } else {
        unreachable!()
      }
    };
    Some (out)
  }
}

// NOTE: we need to define ord to use custom partial ord comparison
impl Ord for Button {
  fn cmp (&self, other : &Self) -> std::cmp::Ordering {
    self.partial_cmp (other).unwrap()
  }
}

impl <K : Into <Variant>> From <K> for Button {
  fn from (k : K) -> Self {
    Button {
      variant:   k.into(),
      modifiers: Modifiers::empty()
    }
  }
}

#[cfg(test)]
mod test {
  use strum;
  use crate::interface::controller::controls;

  use super::*;

  #[test]
  fn variant_unique_serialization() {
    use strum::EnumCount;
    for i in 0..Keycode::COUNT {
      use std::str::FromStr;

      let variant = Variant::from (Keycode::from_repr (i).unwrap());
      let s    = variant.to_string();
      assert_eq!(variant, Variant::from_str (&s).unwrap());
    }

    for i in 0..Mouse::COUNT {
      use std::str::FromStr;

      let variant = Variant::from (Mouse::from_repr (i).unwrap());
      let s    = variant.to_string();
      assert_eq!(variant, Variant::from_str (&s).unwrap());
    }
  }

  #[test]
  fn button_equality() {
    let a = Button {
      modifiers: Modifiers::ANY,
      .. Button::from (Keycode::A)
    };
    let b = Button {
      modifiers: Modifiers::SHIFT,
      .. Button::from (Keycode::A)
    };
    let c = Button {
      modifiers: Modifiers::ANY,
      .. Button::from (Keycode::C)
    };
    let d = Button::from (Keycode::C);
    assert_eq!(a, b);
    assert_eq!(&a, &b);
    assert_ne!(a, c);
    assert_eq!(c, d);
    assert_eq!(&c, &d);
    let buttons = vec![(a, controls::Button (0))];
    assert_eq!(a.cmp (&b), std::cmp::Ordering::Equal);
    assert!(buttons.binary_search_by_key (&b, |(button, _)| *button).is_ok());
  }
}
