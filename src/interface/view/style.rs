use crate::interface::view::Color;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Style {
  pub fg : Color,
  pub bg : Color,
  pub hi : Color,
  pub lo : Color,
}

impl Default for Style {
  fn default() -> Style {
    Style {
      fg: Color::white(),
      bg: Color::black(),
      hi: Color::red(),
      lo: Color::grey()
    }
  }
}
