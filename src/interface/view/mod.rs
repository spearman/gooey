//! Display and input data

use derive_more::{From, TryInto};
use crate::interface::CreateOrder;
use crate::tree::NodeId;

pub use nsys::color::{self, Color};

// input
pub mod input;
pub use self::input::Input;
// view components
pub mod component;
pub use self::component::Component;
// data types
pub mod coordinates;
pub use self::coordinates::{
  Coordinates,
  position,   Position,
  dimensions, Dimensions,
};
pub mod border;
pub mod pointer;
pub mod sound;
pub mod style;
pub mod texture;
pub use self::border::Border;
pub use self::pointer::Pointer;
pub use self::sound::Sound;
pub use self::style::Style;
pub use self::texture::Texture;

/// Describes the appearance of an element
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct View {
  pub component  : Component,
  pub appearance : Appearance,
  pub debug      : bool
}

/// An event for the presentation layer to process
#[derive(Clone, Debug)]
pub enum Display {
  /// Create a new child with the given node ID
  Create (View, NodeId, CreateOrder),
  Update (Update),
  Destroy
}

/// Either a view payload or focus re-order event
#[derive(Clone, Debug, PartialEq, From, TryInto)]
pub enum Update {
  View (View),
  FocusTop
}

/// Appearance is a combination of visual and audible parameters
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct Appearance {
  pub style   : Option <Style>,
  pub sound   : Option <Sound>,
  pub pointer : Option <Pointer>
}

impl From <Component> for View {
  fn from (component : Component) -> Self {
    View { component, .. View::default() }
  }
}
