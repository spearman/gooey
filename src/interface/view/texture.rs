#[derive(Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct Texture {
  pub resource : IndexType,
  pub index    : IndexType
}

pub type IndexType = u16;
