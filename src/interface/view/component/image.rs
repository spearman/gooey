use mat::Mat;
use crate::interface::view::Texture;
use super::impl_kind;

/// Visible image contents
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Image {
  Texture (Texture),
  Raw8    (Mat <u8>),
  Raw16   (Mat <u16>),
  Raw32   (Mat <u32>),
  Raw64   (Mat <u64>)
}
impl_kind!(Image);
