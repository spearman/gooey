use std::convert::TryFrom;
use derive_more::{From, TryInto};

pub mod body;
pub mod canvas;
pub mod image;
pub mod sfx;
pub use self::body::Body;
pub use self::canvas::Canvas;
pub use self::image::Image;
pub use self::sfx::Sfx;

#[derive(Clone, Debug, Eq, PartialEq, From, TryInto)]
pub enum Component {
  Empty,
  Body   (Body),
  Canvas (Canvas),
  Image  (Image),
  Sfx    (Sfx)
}

pub trait Kind : Into <Component> + TryFrom <Component> {
  fn try_ref     (component : &Component)     -> Option <&Self>;
  fn try_ref_mut (component : &mut Component) -> Option <&mut Self>;
}

impl Component {
  pub fn clear (&mut self) {
    *self = Component::Empty
  }
}

impl Default for Component {
  fn default() -> Self {
    Component::Empty
  }
}

impl Kind for Component {
  fn try_ref (component : &Component) -> Option <&Self> {
    Some (component)
  }
  fn try_ref_mut (component : &mut Component) -> Option <&mut Self> {
    Some (component)
  }
}

macro impl_kind {
  ($kind:ident) => {
    impl Kind for $kind {
      fn try_ref (component : &Component) -> Option <&Self> {
        match component {
          Component::$kind (t) => Some (t),
          _ => None
        }
      }
      fn try_ref_mut (component : &mut Component) -> Option <&mut Self> {
        match component {
          Component::$kind (t) => Some (t),
          _ => None
        }
      }
    }
  }
}
