use crate::interface::view::Sound;
use super::impl_kind;

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct Sfx {
  /// A generic ID for the backend to interpret as representing the sound being
  /// played together with a playback state
  pub playback : Option <(Sound, State)>
}
impl_kind!(Sfx);

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum State {
  Ready,
  Play,
  Loop,
  /// Play from position in sample (PCM) timeunits
  PlayFrom (u32),
  /// Loop from position in sample (PCM) timeunits
  LoopFrom (u32)
}

impl Default for State {
  fn default() -> Self {
    State::Ready
  }
}
