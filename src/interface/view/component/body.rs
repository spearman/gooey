use super::impl_kind;

/// Formatted text body
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct Body (pub String);
impl_kind!(Body);
