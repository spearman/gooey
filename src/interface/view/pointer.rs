use derive_more::From;

#[derive(Clone, Debug, Eq, Ord, PartialEq, PartialOrd, From)]
pub struct Pointer (pub IndexType);

pub type IndexType = u16;

impl Pointer {
  pub fn index (&self) -> usize {
    self.0 as usize
  }
}
