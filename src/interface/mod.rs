//! Interface layer.
//!
//! The `Interface` is the primary structure representing the user interface and
//! contains all the data for the interface including the `Presentation`
//! instance.
//!
//! The internal representation of the interface is a `Tree` of `Element`s.  An
//! `Element` is a combination of a `Model` component, a `View` component, and a
//! `Controller` component. The kinds of components in `Elements` and their
//! relations with parent/child nodes are contextualized in various `Widget`
//! definitions.
//!
//! `Action` events define the internal language of the `Interface` layer and
//! result from user `Input` events, or directly from application code.  Each
//! `Action` is directed at a target `Element` node.  `Action` events include
//! creating/destroying/modifying `Element` nodes and their components, changing
//! focus, or submitting callbacks.

use std::convert::TryFrom;
use std::sync::{Arc, RwLock};
use lazy_static::lazy_static;
use log;
use derive_more::{From, TryInto};
use crate::{application, presentation, Application, Presentation, TreeHelper};
use crate::tree::{self, Node, NodeId, Tree, TreeBuilder};

pub mod controller;
pub mod model;
pub mod view;
pub use self::controller::Controller;
pub use self::model::Model;
pub use self::view::View;

pub mod widget;
pub use self::widget::Widget;

lazy_static!{
  /// Pointer position
  pub static ref POINTER : Arc <RwLock <view::input::Pointer>> =
    Arc::new (RwLock::new (view::input::Pointer::default()));
}

/// Parameterized interface represented by a tree of `Element`s
#[derive(Debug)]
pub struct Interface <A=application::Default, P=presentation::Headless> where
  A : Application,
  P : Presentation
{
  pub presentation : P,
  elements         : Tree <Element>,
  focused_id       : NodeId,
  input_buffer     : Option <Vec <view::Input>>,
  action_buffer    : Option <Vec <(NodeId, Action)>>,
  display_buffer   : Vec <(NodeId, view::Display)>,
  event_buffer     : Vec <(NodeId, model::Event)>,
  _phantom         : std::marker::PhantomData <A>
}

/// An interface node consisting of one each of `Model`, `View`, and
/// `Controller` components.
///
/// See `Widget`s for construction of contextualzed combinations of different
/// components.
#[derive(Clone, Debug, Default)]
pub struct Element {
  pub name       : String,
  pub controller : Controller,
  pub model      : Model,
  pub view       : View
}

/// An interface-level event
#[derive(From, TryInto)]
pub enum Action {
  Create           (Tree <Element>, CreateOrder),
  ModifyController (Box <dyn FnOnce (&mut Controller)>),
  ModifyModel      (Box <dyn FnOnce (&mut Model)>),
  ModifyView       (Box <dyn FnOnce (&mut View)>),
  /// This will submit produce an event with the Model containing this callback
  /// ID, but will *not* modify the current Model callback ID.
  ///
  /// See the Form widget control `FormSubmitCallback` for a control function
  /// that submits a model with the currently set callback ID.
  SubmitCallback   (application::CallbackId),
  /// Change the view state from Enabled to Focused; it is a debug error if the
  /// view state is not Enabled.
  ///
  /// If `top` is true, node will be moved to the last sibling position
  /// ("top"), otherwise position relative to siblings will remain unchanged
  Focus,
  /// Change `view.state` from Disabled to Enabled; it is a debug error if the
  /// view state is not Disabled
  Enable,
  /// Change the `view.state` from Enabled to Disabled; it is a debug error if
  /// the view state is not Enabled
  Disable,
  /// Destroy the target element and children.
  ///
  /// If the node was focused, then focus is shifted to the parent. If it was
  /// the root node that was focused, then this is always an error since the
  /// root node is always focused as the common ancestor of all nodes.
  Destroy,
  /// Trigger any release buttons that exist in the current input map of the
  /// controller of the target node.
  ///
  /// This can be used when focus is changing and input state should not be
  /// orphaned.
  ReleaseButtons
}

/// Either append or prepend newly created element
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum CreateOrder {
  Append,
  Prepend,
  NthSibling (u32)
}

impl <A, P> Interface <A, P> where
  A : Application,
  P : Presentation
{
  pub fn with_root (mut root : Element) -> Self {
    log::trace!("with_root...");
    root.focus();
    let elements       = TreeBuilder::new().with_root (Node::new (root.clone()))
      .build();
    let focused_id     = elements.root_node_id().unwrap().clone();
    let presentation   = P::with_root (root.view, focused_id.clone());
    let input_buffer   = Some (vec![]);
    let action_buffer  = Some (vec![]);
    let display_buffer = vec![];
    let event_buffer   = vec![];
    let _phantom       = Default::default();
    log::trace!("...with_root");
    Interface {
      elements, focused_id, presentation,
      input_buffer, display_buffer, action_buffer, event_buffer,
      _phantom
    }
  }

  /// Handle an application request.
  ///
  /// Does not update presentation, must call `display()` to refresh display.
  #[inline]
  #[must_use]
  pub fn action (&mut self, node_id : &NodeId, action : Action)
    -> std::vec::Drain <(NodeId, model::Event)>
  {
    self.handle_action (action, node_id);
    self.event_buffer.drain(..)
  }

  /// Handles a batch of an application requests.
  ///
  /// Does not update presentation, must call `display()` to refresh display.
  #[inline]
  #[must_use]
  pub fn actions (&mut self, actions : Vec <(NodeId, Action)>)
    -> std::vec::Drain <(NodeId, model::Event)>
  {
    for (node_id, action) in actions.into_iter() {
      self.handle_action (action, &node_id);
    }
    self.event_buffer.drain(..)
  }

  #[must_use]
  /// Get and handle user input
  pub fn update (&mut self) -> std::vec::Drain <(NodeId, model::Event)> {
    log::trace!("update...");
    // handle input
    let mut action_buffer = self.action_buffer.take().unwrap();
    let mut input_buffer  = self.input_buffer.take().unwrap();
    debug_assert!(action_buffer.is_empty());
    debug_assert!(input_buffer.is_empty());
    self.presentation.get_input (&mut input_buffer);
    for input in input_buffer.drain(..) {
      log::trace!("input: {:?}", input);
      // update global POINTER
      match &input {
        view::Input::Pointer (pointer) =>
          *POINTER.write().unwrap() = pointer.clone(),
        _ => {}
      }
      let focused_id = self.focused_element_id().clone();
      let focused = self.focused_element();
      // TODO: possibly allow a node to bubble even if input was handled ?
      let button_release = match focused.controller.handle_input::<A> (
        input, &self.elements, &focused_id, &mut action_buffer
      ) {
        Ok  (result) => result,
        Err (mut input) => {
          // bubble up to ancestors
          debug_assert!(action_buffer.is_empty());
          let mut button_release = None;
          let input_mask = (&input).into();
          if !focused.controller.bubble_trap.intersects (input_mask) {
            for ancestor_id in self.elements.ancestor_ids (&focused_id)
              .unwrap()
            {
              let ancestor = self.get_element (&ancestor_id);
              input = match ancestor.controller.handle_input::<A> (
                input, &self.elements, &ancestor_id, &mut action_buffer
              ) {
                Err (input)  => if ancestor.controller.bubble_trap
                  .intersects (input_mask)
                {
                  break
                } else {
                  input
                }
                Ok  (result) => {
                  button_release = result.map (|button_release| button_release);
                  break
                }
              };
            }
          }
          button_release
        }
      };
      // handle actions
      for (node_id, action) in action_buffer.drain(..) {
        self.handle_action (action, &node_id);
      }
      // TODO: this was originally before handling the actions so that if focus
      // changes, the release button is inserted into the newly focused node;
      // however if the button release payload is a remove, we may want that to
      // be processed by the original node before focus changes
      if let Some (button_release) = button_release {
        match button_release {
          controller::ButtonRelease::Insert (input, controls) =>
            for (control, node_id) in controls {
              let element = self.get_element_mut (&node_id);
              element.controller.release_button_insert (input, control);
            }
          controller::ButtonRelease::Remove (input, node_id) => {
            let element = self.get_element_mut (&node_id);
            element.controller.release_button_remove (input);
          }
        }
      }
    }
    debug_assert!(action_buffer.is_empty());
    debug_assert!(input_buffer.is_empty());
    self.action_buffer = Some (action_buffer);
    self.input_buffer  = Some (input_buffer);

    log::trace!("...update");
    // return application events
    self.event_buffer.drain(..)
  }

  /// Presentation update
  #[inline]
  pub fn display (&mut self) {
    log::trace!("display...");
    self.presentation
      .display_view (&self.elements, self.display_buffer.drain(..));
    log::trace!("...display");
  }

  #[inline]
  pub fn elements (&self) -> &Tree <Element> {
    &self.elements
  }

  #[inline]
  pub fn root (&self) -> &Element {
    self.root_node().data()
  }

  pub fn root_node (&self) -> &Node <Element> {
    self.elements.get (self.root_id()).unwrap()
  }

  #[inline]
  pub fn root_id (&self) -> &NodeId {
    self.elements.root_node_id().unwrap()
  }

  #[inline]
  pub fn get_element (&self, node_id : &NodeId) -> &Element {
    self.elements.get_element (node_id)
  }

  #[inline]
  pub fn get_element_mut (&mut self, node_id : &NodeId) -> &mut Element {
    self.elements.get_element_mut (node_id)
  }

  #[inline]
  pub fn focused_element_id (&self) -> &NodeId {
    &self.focused_id
  }

  #[inline]
  pub fn focused_element (&self) -> &Element {
    self.get_element (&self.focused_id)
  }

  #[inline]
  pub fn focused_element_mut (&mut self) -> &mut Element {
    let focused_id = self.focused_id.clone();
    self.get_element_mut (&focused_id)
  }

  #[inline]
  pub fn print_elements_tree (&self) {
    let mut s = String::new();
    self.elements.write_formatted (&mut s).unwrap();
    println!("elements:\n{}", s);
  }

  #[inline]
  pub fn log_elements_tree (&self) {
    let mut s = String::new();
    self.elements.write_formatted (&mut s).unwrap();
    log::info!("elements:\n{}", s);
  }

  #[inline]
  pub fn print_elements_tree_names (&self) {
    let mut s = String::new();
    self.elements.write_formatted_names (&mut s).unwrap();
    println!("elements:\n{}", s);
  }

  #[inline]
  pub fn log_elements_tree_names (&self) {
    let mut s = String::new();
    self.elements.write_formatted_names (&mut s).unwrap();
    log::info!("elements:\n{}", s);
  }

  /// Create a new element and return the node ID
  pub fn create_singleton (&mut self,
    parent_id : &NodeId,
    element   : Element,
    order     : CreateOrder
  ) -> NodeId {
    match self.action (parent_id, Action::create_singleton (element, order))
      .next().unwrap()
    {
      (_, model::Event::Create (_, new_id, _)) => new_id.clone(),
      _ => unreachable!()
    }
  }

  fn handle_action (&mut self, action : Action, node_id : &NodeId) {
    use controller::component::{Kind, Selection};
    log::trace!("handle_action...");
    log::trace!("action: {:?}", action);
    match action {
      Action::Create (elements, order) => {
        self.splice_subtree (
          &elements, elements.root_node_id().unwrap(), node_id, order);
      }
      Action::ModifyController (f) => {
        let controller = &mut self.get_element_mut (&node_id).controller;
        f (controller);
      }
      Action::ModifyModel (f) => {
        let model = {
          let model = &mut self.get_element_mut (&node_id).model;
          f (model);
          model.clone()
        };
        self.event_buffer.push ((node_id.clone(), model::Event::Update (model)));
      }
      Action::ModifyView (f) => {
        let view = {
          let view = &mut self.get_element_mut (&node_id).view;
          f (view);
          // TODO: reset ephemeral fields (sound triggers)
          view.clone()
        };
        self.display_buffer.push (
          (node_id.clone(), view::Display::Update (view.into())));
      }
      Action::SubmitCallback (callback_id) => {
        let callback_id = Some (callback_id);
        let model = Model {
          callback_id, .. self.get_element (&node_id).model.clone()
        };
        self.event_buffer.push ((node_id.clone(), model::Event::Submit (model)));
      }
      Action::Focus => {
        let mut focus_id = Some (node_id.clone());
        while let Some (id) = focus_id {
          focus_id = self.change_focus (&id);
          // handle re-ordering
          if self.get_element (&id).controller.focus_top {
            self.elements.make_last_sibling (&id).unwrap();
            self.display_buffer.push (
              (id.clone(), view::Display::Update (view::Update::FocusTop)));
          }
          let ancestor_ids = self.elements.ancestor_ids (&id).unwrap().cloned()
            .collect::<Vec <_>>();
          for ancestor_id in ancestor_ids {
            if self.get_element (&ancestor_id).controller.focus_top {
              self.elements.make_last_sibling (&ancestor_id).unwrap();
              self.display_buffer.push (
                ( ancestor_id.clone(),
                  view::Display::Update (view::Update::FocusTop)
                )
              );
            }
          }
        }
      }
      Action::Enable => {
        let view = {
          let element = self.get_element_mut (&node_id);
          element.controller.state.enable();
          element.view.appearance = element.controller.get_appearance().clone();
          element.view.clone()
        };
        self.display_buffer.push (
          (node_id.clone(), view::Display::Update (view.into())));
      }
      Action::Disable => {
        let view = {
          let element = self.get_element_mut (&node_id);
          element.controller.state.disable();
          element.view.appearance = element.controller.get_appearance().clone();
          element.view.clone()
        };
        self.display_buffer.push (
          (node_id.clone(), view::Display::Update (view.into())));
      }
      Action::Destroy => {
        let parent_id = self.elements.get_parent_id (&node_id).clone();
        let focused = self.elements.get_element (&node_id).controller.state ==
          controller::State::Focused;
        { // widget-specific destroy logic
          let mut children_ids = self.elements.children_ids (&parent_id)
            .unwrap();
          // menu: we want to update the selection here so that the destroyed
          // node is not refocused below
          let first_sibling_id = children_ids.next().unwrap().clone();
          let first_sibling    = self.elements.get_element (&first_sibling_id);
          if let Ok (Widget (selection, _, _)) =
            widget::Menu::try_from (first_sibling)
          {
            if selection.current.as_ref() == Some (node_id) {
              let mut select = None;
              for sibling_id in children_ids {
                if sibling_id == node_id {
                  continue
                }
                let sibling = self.elements.get_element (&sibling_id);
                if sibling.controller.state == controller::State::Enabled &&
                  widget::Frame::try_from (sibling).is_ok()
                {
                  select = Some (sibling_id.clone());
                  break
                }
              }
              let deselect = Box::new (move |controller : &mut Controller|{
                let selection =
                  Selection::try_ref_mut (&mut controller.component).unwrap();
                selection.current = select;
              });
              self.handle_action (Action::ModifyController (deselect),
                &first_sibling_id);
            }
          }
        }
        if focused {
          let mut focus_id = Some (parent_id.clone());
          while let Some (id) = focus_id {
            focus_id = self.change_focus (&id);
          }
        }
        for node_id in self.elements.traverse_post_order_ids (&node_id)
          .unwrap()
        {
          self.event_buffer.push   ((node_id.clone(), model::Event::Destroy));
          self.display_buffer.push ((node_id,         view::Display::Destroy));
        }
        let _ = self.elements
          .remove_node (node_id.clone(), tree::RemoveBehavior::DropChildren)
          .unwrap();
      }
      Action::ReleaseButtons => {
        use controller::controls::Control;
        let release_buttons =
          self.elements.get_element_mut (node_id).controller.release_buttons();
        for control in release_buttons {
          control.fun::<A::ButtonControls>().0 (
            &None, &self.elements, node_id,
            self.action_buffer.as_mut().unwrap()
          );
        }
        let mut action_buffer = self.action_buffer.take().unwrap();
        for (node_id, action) in action_buffer.drain(..) {
          self.handle_action (action, &node_id);
        }
        self.action_buffer = Some (action_buffer);
      }
    }
    log::trace!("...handle_action");
  }

  /// Insert node as child of the given node ID
  fn insert_child (&mut self,
    parent_id : &NodeId,
    child     : Node <Element>,
    order     : CreateOrder
  ) -> NodeId {
    let child_model = child.data().model.clone();
    let child_view  = child.data().view.clone();
    let child_id    = self.elements.insert (
      child, tree::InsertBehavior::UnderNode (parent_id)).unwrap();
    match order {
      CreateOrder::Append  => {}
      CreateOrder::Prepend => {
        let _ = self.elements.make_first_sibling (&child_id).unwrap();
      }
      CreateOrder::NthSibling (n) => {
        let _ = self.elements.make_nth_sibling (&child_id, n as usize).unwrap();
      }
    }
    self.event_buffer.push ((parent_id.clone(),
      model::Event::Create (child_model, child_id.clone(), order)));
    self.display_buffer.push ((parent_id.clone(),
      view::Display::Create (child_view, child_id.clone(), order)));
    child_id
  }

  /// Defocus the currently focused node and focuses the target.
  ///
  /// Returns a new focus ID if focus should be redirected.
  fn change_focus (&mut self, target_id : &NodeId) -> Option <NodeId> {
    use controller::component::{Kind, Selection};
    log::trace!("change_focus...");
    let focused_id = self.focused_element_id().clone();
    let focused_ancestors = self.elements.ancestor_ids (&focused_id).unwrap()
      .cloned().collect::<Vec <NodeId>>();
    let target_ancestors = self.elements.ancestor_ids (target_id).unwrap()
      .cloned().collect::<Vec <NodeId>>();
    let maybe_common_ancestor_id = {
      focused_ancestors.iter().rev().zip (target_ancestors.iter().rev())
        .filter_map (|(a, b)|
          if a == b {
            Some (a)
          } else {
            None
          })
        .cloned().enumerate().last()
    };
    let focused_ancestor_of_target = target_ancestors.contains (&focused_id);
    let target_ancestor_of_focused = focused_ancestors.contains (&target_id);
    { // defocus currently focused node and ancestors up to common ancestor
      let mut defocus = |defocus_id| {
        let view = {
          // widget-specific defocus logic here
          let mut actions  = vec![];
          let focused_node = self.elements.get (&defocus_id).unwrap();
          // release button if the focused node is a button widget and it was
          // pressed ('on')
          if let Some (sub_child_id) = focused_node.children().first() {
            if let Ok (Widget (switch, _, _)) =
              widget::Button::try_get (&self.elements, sub_child_id)
            {
              if switch.state == controller::component::switch::State::On &&
                !switch.toggle
              {
                widget::button::release (&None, &self.elements, &defocus_id,
                  &mut actions);
              }
            }
          }
          for (node_id, action) in actions.into_iter() {
            self.handle_action (action, &node_id);
          }
          let focused = self.elements.get_mut (&defocus_id).unwrap().data_mut();
          focused.defocus();
          let view    = focused.view.clone();
          focused.view.appearance.sound = None;  // sounds are ephemeral events
          view
        };
        self.display_buffer
          .push ((defocus_id, view::Display::Update (view.into())));
      };
      // do not defocus if focused node is an ancestor of the target
      if !focused_ancestor_of_target {
        defocus (focused_id);
      }
      if let Some ((_, common_ancestor_id)) = maybe_common_ancestor_id.as_ref() {
        for ancestor_id in focused_ancestors.into_iter()
          .take_while (|id| id != common_ancestor_id && id != target_id)
        {
          defocus (ancestor_id);
        }
      }
    }
    let refocus_id = {
      // focus from common ancestor to target node
      let mut focus = |focus_id| {
        // widget-specific focus logic
        if widget::Frame::try_from (self.get_element (&focus_id)).is_ok() {
          // update selection if frame is a menu item
          let parent_id = self.elements.get_parent_id (&focus_id);
          let first_sibling_id = self.elements.children_ids (&parent_id).unwrap()
            .next().unwrap().clone();
          if first_sibling_id != focus_id {
            let first_sibling = self.elements.get_element (&first_sibling_id);
            if let Ok (Widget (selection, _, _)) =
              widget::Menu::try_from (first_sibling)
            {
              if selection.current.as_ref() != Some (&focus_id) {
                let select_id = focus_id.clone();
                let select = Box::new (move |controller : &mut Controller|{
                  let selection =
                    Selection::try_ref_mut (&mut controller.component).unwrap();
                  selection.current = Some (select_id.clone());
                });
                self.handle_action (Action::ModifyController (select),
                  &first_sibling_id);
              }
            }
          }
        }
        let focus = self.get_element_mut (&focus_id);
        focus.focus();
        let view = focus.view.clone();
        focus.view.appearance.sound = None;  // sounds are ephemeral events
        self.display_buffer.push (
          (focus_id.clone(), view::Display::Update (view.into())));
      };
      let depth = if let Some ((depth, _)) = maybe_common_ancestor_id {
        depth + 1 + if focused_ancestor_of_target { 1 } else { 0 }
      } else {
        1
      };
      for ancestor_id in target_ancestors.into_iter().rev().skip (depth) {
        focus (ancestor_id);
      }
      // do not focus if target is ancestor of the currently focused node
      // (the node should already be focused)
      if !target_ancestor_of_focused {
        focus (target_id.clone());
      }
      // only call refocus on the last node: intermediate nodes are not
      // redirected
      controller::refocus (target_id, self.elements())
    };
    self.focused_id = target_id.clone();
    log::trace!("...change_focus");
    refocus_id
  }

  /// See also public `crate::utils::splice_subtree` for operating on subtrees
  /// before insertion into the interface
  fn splice_subtree (&mut self,
    other     : &Tree <Element>,
    other_id  : &NodeId,
    parent_id : &NodeId,
    order     : CreateOrder
  ) -> NodeId {
    // TODO: avoid clone here?
    let subtree_root = Node::new (other.get (&other_id).unwrap().data().clone());
    let subtree_id   = self.insert_child (parent_id, subtree_root, order);
    for child_id in other.children_ids (other_id).unwrap() {
      self.splice_subtree (other, child_id, &subtree_id, CreateOrder::Append);
    }
    subtree_id
  }

  /// This is method is provided to allow initialization of composite
  /// presentations by first initializing an interface with
  /// `G::make_interface()`--the graphical backend creates the interface with a
  /// default initial interface, usually containing a root screen element--and
  /// then adding a default initialized audio backend.
  pub (crate) fn swap_presentation <P2 : Presentation> (self,
    f : impl FnOnce (P) -> P2
  ) -> Interface <A, P2> {
    let Interface {
      elements, focused_id, presentation,
      input_buffer, display_buffer, action_buffer, event_buffer,
      _phantom
    } = self;
    Interface {
      presentation: f (presentation),
      elements, focused_id, input_buffer, display_buffer, action_buffer,
      event_buffer, _phantom
    }
  }
}

impl Element {
  pub fn focus (&mut self) {
    self.controller.state.focus();
    self.controller.update_view_focus (&mut self.view);
  }
  pub fn defocus (&mut self) {
    self.controller.state.defocus();
    self.controller.update_view_focus (&mut self.view);
  }
  pub fn disable (&mut self) {
    self.controller.state.disable();
    self.controller.update_view_focus (&mut self.view);
  }
}

impl AsRef <Controller> for Element {
  fn as_ref (&self) -> &Controller {
    &self.controller
  }
}

impl AsRef <Model> for Element {
  fn as_ref (&self) -> &Model {
    &self.model
  }
}

impl AsRef <View> for Element {
  fn as_ref (&self) -> &View {
    &self.view
  }
}

impl Action {
  /// Produces a Create action that creates a singleton subtree containing the
  /// given Element
  #[inline]
  pub fn create_singleton (element : Element, order : CreateOrder) -> Self {
    let subtree = TreeBuilder::new().with_root (Node::new (element)).build();
    Action::Create (subtree, order)
  }

  /// Produces a ModifyView action that sets the given View data.
  #[inline]
  pub fn set_view (view : View) -> Self {
    Action::ModifyView (Box::new (|v| *v = view))
  }

  /// Produces a ModifyController action that sets the given Controller data.
  #[inline]
  pub fn set_controller (controller : Controller) -> Self {
    Action::ModifyController (Box::new (|c| *c = controller))
  }

  /// Produces a ModifyModel action that sets the given Model data.
  #[inline]
  pub fn set_model (model : Model) -> Self {
    Action::ModifyModel (Box::new (|m| *m = model))
  }

  /// Produces a ModifyView action that sets the given component Kind
  #[inline]
  pub fn set_view_component <V> (component : V) -> Self where
    V : view::component::Kind + 'static
  {
    Action::ModifyView (Box::new (|v| v.component = component.into()))
  }

  /// Produces a ModifyView action that updates the current component with the
  /// given value and panics if the current component is not the same Kind
  #[inline]
  pub fn update_view_component <V> (component : V) -> Self where
    V : view::component::Kind + 'static
  {
    Action::ModifyView (Box::new (|v| {
      let v = V::try_ref_mut (&mut v.component).unwrap();
      *v = component;
    }))
  }

  /// Produces a ModifyController action that sets the given component Kind
  #[inline]
  pub fn set_controller_component <V> (component : V) -> Self where
    V : controller::component::Kind + 'static
  {
    Action::ModifyController (Box::new (|c| c.component = component.into()))
  }

  /// Produces a ModifyController action that updates the current component with the
  /// given value and panics if the current component is not the same Kind
  #[inline]
  pub fn update_controller_component <V> (component : V) -> Self where
    V : controller::component::Kind + 'static
  {
    Action::ModifyController (Box::new (|c| {
      let c = V::try_ref_mut (&mut c.component).unwrap();
      *c = component;
    }))
  }

  /// Produces a ModifyModel action that sets the given component Kind
  #[inline]
  pub fn set_model_component <V> (component : V) -> Self where
    V : model::component::Kind + 'static
  {
    Action::ModifyModel (Box::new (|m| m.component = component.into()))
  }

  /// Produces a ModifyModel action that updates the current component with the
  /// given value and panics if the current component is not the same Kind
  #[inline]
  pub fn update_model_component <V> (component : V) -> Self where
    V : model::component::Kind + 'static
  {
    Action::ModifyModel (Box::new (|m| {
      let m = V::try_ref_mut (&mut m.component).unwrap();
      *m = component;
    }))
  }
}

impl std::fmt::Debug for Action {
  fn fmt (&self, f : &mut std::fmt::Formatter) -> Result <(), std::fmt::Error> {
    match self {
      Action::Create (ref subtree, ref order)    =>
        write!(f, "Create({:?}, {:?})", subtree, order),
      Action::ModifyController (ref closure)     =>
        write!(f, "ModifyController({:p})", &closure),
      Action::ModifyModel      (ref closure)     =>
        write!(f, "ModifyModel({:p})", &closure),
      Action::ModifyView       (ref closure)     =>
        write!(f, "ModifyView({:p})", &closure),
      Action::SubmitCallback   (ref callback_id) =>
        write!(f, "SubmitCallback({:?})", callback_id),
      Action::Focus   => write!(f, "Focus"),
      Action::Enable  => write!(f, "Enable"),
      Action::Disable => write!(f, "Disable"),
      Action::Destroy => write!(f, "Destroy"),
      Action::ReleaseButtons => write!(f, "ReleaseButtons")
    }
  }
}

impl Default for CreateOrder {
  fn default() -> Self {
    CreateOrder::Append
  }
}
