//! Domain data bindings

use crate::application;
use crate::interface::CreateOrder;
use crate::tree::NodeId;

pub mod component;
pub use self::component::Component;

/// A data component associated with application events and callbacks
#[derive(Clone, Debug, Default)]
pub struct Model {
  pub component   : Component,
  pub callback_id : Option <application::CallbackId>
}

/// A change in domain data to be processed by the `Application`.
#[derive(Debug)]
pub enum Event {
  Create (Model, NodeId, CreateOrder),
  Submit (Model),
  Update (Model),
  Destroy
}

impl From <Component> for Model {
  fn from (component : Component) -> Self {
    Model { component, .. Model::default() }
  }
}
