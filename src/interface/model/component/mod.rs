use std::convert::TryFrom;
use derive_more::{From, TryInto};

pub mod pixmap;
pub use self::pixmap::Pixmap;

#[derive(Clone, Debug, PartialEq, From, TryInto)]
pub enum Component {
  Empty,
  Flag     (bool),
  Unsigned (u64),
  Signed   (i64),
  Float    (f64),
  IntVec   (Vec <i64>),
  FloatVec (Vec <f64>),
  Text     (String),
  Pixmap   (Pixmap)
}

pub trait Kind : Into <Component> + TryFrom <Component> {
  fn try_ref     (component : &Component)     -> Option <&Self>;
  fn try_ref_mut (component : &mut Component) -> Option <&mut Self>;
}

impl Component {
  pub fn clear (&mut self) {
    *self = Component::Empty
  }
}

impl Default for Component {
  fn default() -> Self {
    Component::Empty
  }
}

impl Kind for Component {
  fn try_ref (component : &Component) -> Option <&Self> {
    Some (component)
  }
  fn try_ref_mut (component : &mut Component) -> Option <&mut Self> {
    Some (component)
  }
}

macro impl_kind {
  ($component:ident, $kind:ty) => {
    impl Kind for $kind {
      fn try_ref (component : &Component) -> Option <&Self> {
        match component {
          Component::$component (t) => Some (t),
          _ => None
        }
      }
      fn try_ref_mut (component : &mut Component) -> Option <&mut Self> {
        match component {
          Component::$component (t) => Some (t),
          _ => None
        }
      }
    }
  }
}

impl_kind!(Flag,     bool);
impl_kind!(Unsigned, u64);
impl_kind!(Signed,   i64);
impl_kind!(Float,    f64);
impl_kind!(IntVec,   Vec <i64>);
impl_kind!(FloatVec, Vec <f64>);
impl_kind!(Text,     String);
