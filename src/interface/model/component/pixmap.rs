use derive_more::{From, TryInto};
use mat::Mat;
use super::impl_kind;

#[derive(Clone, Debug, PartialEq, From, TryInto)]
pub enum Pixmap {
  // TODO: bitmap variant
  Pixmap8  (Mat  <u8>),
  Pixmap16 (Mat <u16>),
  Pixmap32 (Mat <u32>),
  Pixmap64 (Mat <u64>),
  Texture {
    resource_id : u16,
    index       : u16,
    width       : u32,
    height      : u32
  }
}

impl_kind!(Pixmap, Pixmap);

impl Default for Pixmap {
  fn default() -> Self {
    Pixmap::Pixmap8 (Mat::default())
  }
}

impl Pixmap {
  pub fn width (&self) -> usize {
    match self {
      Pixmap::Pixmap8  (mat) => mat.width(),
      Pixmap::Pixmap16 (mat) => mat.width(),
      Pixmap::Pixmap32 (mat) => mat.width(),
      Pixmap::Pixmap64 (mat) => mat.width(),
      Pixmap::Texture { width, .. } => *width as usize
    }
  }
  pub fn height (&self) -> usize {
    match self {
      Pixmap::Pixmap8  (mat) => mat.height(),
      Pixmap::Pixmap16 (mat) => mat.height(),
      Pixmap::Pixmap32 (mat) => mat.height(),
      Pixmap::Pixmap64 (mat) => mat.height(),
      Pixmap::Texture { height, .. } => *height as usize
    }
  }
}
