//! # gooey <IMG STYLE="vertical-align: middle" SRC="../../../splatl.png">
//!
//! > UI library
//!
//! A gooey interface is defined as a layer between an application and a
//! presentation backend. An application layer can handle callbacks on model
//! data bound to the interface, and a presentation layer can receive input from
//! the user or OS and display a view of the interface.
//!
//! ```
//! use gooey::{application, presentation, Presentation};
//! use gooey::interface::{self, Interface, view, widget};
//! // create a default interface with a headless presentation layer that reads
//! // from stdin and ignores all display values
//! let mut interface =
//!   presentation::Headless::make_interface::<application::Default>();
//! // attach a widget to the root node
//! let root_id = interface.root_id().clone();
//! let action  = interface::Action::create_singleton (
//!   widget::playback::new (None), interface::CreateOrder::Append);
//! for event in interface.action (&root_id, action) {
//!   // ... model events
//! }
//! // get stdin and update the interface
//! for event in interface.update() {
//!   // ... model events
//! }
//! interface.display();  // no-op
//! ```
//!
//! See `./examples/` for complete examples.

#![warn(unused_extern_crates)]
#![feature(associated_type_defaults)]
#![feature(decl_macro)]

#[doc(hidden)] pub use serde;
#[doc(hidden)] pub use strum;

pub use id_tree as tree;
pub use tree::Tree;
pub use nsys::{geometry, math};

pub mod application;
pub mod interface;
pub mod presentation;
pub mod utils;
pub mod prelude;

pub use self::application::Application;
pub use self::interface::Interface;
pub use self::presentation::Presentation;
pub use self::utils::TreeHelper;

#[doc(hidden)]
#[allow(unused_macros)]
pub (crate) macro debug {
  ($e:expr) => {
    log::debug!("{}: {:?}", stringify!($e), $e);
  }
}

#[doc(hidden)]
#[allow(unused_macros)]
pub (crate) macro pretty {
  ($e:expr) => {
    log::debug!("{}: {:#?}", stringify!($e), $e);
  }
}

#[doc(hidden)]
macro_rules! show {
  ($e:expr) => { println!("{}: {:?}", stringify!($e), $e); }
}

/// Diagnostic print sizes of various types
pub fn report_sizes() {
  use std::mem::size_of;
  println!("gooey: report sizes...");

  show!(size_of::<Interface>());
  show!(size_of::<interface::Element>());
  show!(size_of::<interface::Model>());
  show!(size_of::<interface::Controller>());
  show!(size_of::<interface::controller::Appearances>());
  show!(size_of::<interface::controller::component::Layout>());
  show!(size_of::<interface::View>());
  show!(size_of::<interface::view::component::Canvas>());
  show!(size_of::<interface::Action>());
  show!(size_of::<interface::model::Event>());
  show!(size_of::<interface::view::Input>());

  #[cfg(feature="curses")]
  show!(size_of::<Interface <application::Default, presentation::Curses>>());
  #[cfg(feature="fmod-audio")]
  show!(size_of::<Interface <application::Default, presentation::Fmod>>());

  println!("gooey: ...report sizes");
}
