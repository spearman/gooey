#!/usr/bin/env bash

set -x

RUST_BACKTRACE=1 cargo run --example curses-defaults --features="curses" $@
tail curses-defaults.log
set +x
warn_count=$(rg -c WARN curses-defaults.log)
error_count=$(rg -c ERROR curses-defaults.log)
if [ -z "$warn_count" ]; then
  warn_count=0
fi
if [ -z "$error_count" ]; then
  error_count=0
fi
echo "$warn_count warnings"
echo "$error_count errors"

exit
