with import <nixpkgs> {};
mkShell {
  buildInputs = [
    gdb       # required for rust-gdb
    graphviz
    llvm
    pkg-config # seems to be needed for opengl to find libX11 ?
    rustup
    rust-analyzer
  ];
  # needed for fmod-sys crate so bindgen can find libclang.so
  LIBCLANG_PATH="${llvmPackages.libclang.lib}/lib";
  # needed for libfmod.so to dynamically link libstdc++.so.6 and libalsa.so
  LD_LIBRARY_PATH = "${stdenv.cc.cc.lib}/lib:${alsaLib}/lib:" +
    lib.makeLibraryPath [
      # required for opengl
      libglvnd
      xorg.libX11
      xorg.libXcursor
      xorg.libXi
      xorg.libXrandr
      libxkbcommon
      # required for curses
      ncurses
    ];
}
