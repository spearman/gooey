#!/usr/bin/env bash

set -x

RUST_BACKTRACE=1 cargo run --example remote --features="opengl-x11" $@ &&
  tail -n 20 remote.log

exit
