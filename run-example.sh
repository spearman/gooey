#!/usr/bin/env bash

set -x

RUST_BACKTRACE=1 cargo run --example example $@

exit
