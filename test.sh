#!/usr/bin/env bash

set -x

cargo test --all-features $@

exit
