#!/usr/bin/env bash

set -x

export RUSTFLAGS="-Z linker-features=-lld"
RUST_BACKTRACE=1 cargo run --example composite \
  --features="curses fmod-audio" $@ && tail composite.log

exit
