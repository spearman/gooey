#!/usr/bin/env bash

set -x

cargo modules generate graph --layout=dot $@ > modules.dot && \
  dot -Tpng modules.dot > modules.png && feh modules.png

exit
