#!/usr/bin/env bash

set -x

cargo clean
rm -rf html
rm -f *.log
rm -f *.dot

exit
